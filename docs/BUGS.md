# Reporting bugs
There are two places where you're able to report issues with Safe Surfer's desktop app:
- Help Desk Support: info@safesurfer.co.nz
- [GitLab](https://gitlab.com/safesurfer/SafeSurfer-Android/issues/new)

We request that you please report your issue to Help Desk Support, unless you know it's a specific technical issue.

# Reporting bugs on GitLab
As a prerequisite, you'll need a GitLab account (if you don't have one already) -- If you don't, [click here](https://gitlab.com/users/sign_in#register-pane).  

Head over to the [GitLab issues](https://gitlab.com/safesurfer/SafeSurfer-Android/issues/new) page, choose the `bugs.md` template (if there is no pre-written bug template given).  

Steps to report bugs:  
1. Find a bug (you will to be able to replicated it)
2. Go to 'About us' (via Hamburger menu), take note of Version, Build, and Git Hash
3. Choose the bug template
4. Paste version information below the `App Information` header
5. Fill in the rest of the fields in the template
