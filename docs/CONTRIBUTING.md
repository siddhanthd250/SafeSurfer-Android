# Contributing to this project
Thank you for wanting to contribute to Safe Surfer Android! Let's get you up to date on contributing to this project.  
We're excited to see this app getting improved and used worldwide, improving the lives of so many people through keeping them safe online.  

## Ground rules
#### Be respectful to others
We can achieve great things when we work collaboratively and respectfully.  
Remember that all source code must respect the freedom and privacy of others.  

#### We're all here to collaborate, so let's enjoy it
Through our collaboration in the building of this community, we can achieve awesome things.  
By collaborating with one another, we can build an excellent project, which will positively impact other people’s lives.   

#### Stay awesome
We appreciate all the help which will come from this project, so we greatly thank our contributors.  

## Community Agreements

To contribute to the Android app project, please read [the community agreements docs](https://gitlab.com/safesurfer/community/-/blob/master/docs/CONTRIBUTING.md).

## Your first commit
Looking for places to start? A good place is the [issues](https://gitlab.com/safesurfer/SafeSurfer-Android/issues) page.  
There will likely be a few issues for you to check out and to see if you can resolve.  

## How to make changes
Please read our guide on [pull requests](PULLREQUESTCHECKLIST.md) if you wish to make one.  
1. Fork this project
2. Make changes to the fork
3. Submit a pull request

## Report bugs
Please refer to `BUGS.md`.  

## Which js documents do what?
`app/src/main/java/com/safesurfer` is the folder where most of the code lives.  
`app/src/main/res` is the folder where resources for the project are, such as images and translations.  

## Request features
Again, using our [issues](https://gitlab.com/safesurfer/SafeSurfer-Android/issues/new) page, you can request features by using the `features.md` template.  
Simply write in the given fields, a description of the feature that is of request.  

## Translating
For information regarding translating Safe Surfer's Android app, please read [TRANSLATING.md](TRANSLATING.md).  
