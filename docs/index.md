# Safe Surfer Android

> Simplest way to protect your phone for on-the-go mobile protection

## Index
- [Building](./BUILDING.md)
- [Contributing](./CONTRIBUTING.md)
- [Pull request checklist](./PULLREQUESTCHECKLIST.md)
- [Translating](./TRANSLATING.md)
- [Bugs](./BUGS.md)
