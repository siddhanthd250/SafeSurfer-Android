#!/bin/bash

while read r
do
    case "$(echo "$r" | cut -d '/' -f1)" in
        app|.gitlab-ci.yml|build.gradle|gradle.properties|settings.gradle|support)
            echo "Will build APK."
            exit 0
            ;;
    esac
done < <(git diff-tree --no-commit-id --name-only -r $(git rev-parse --short HEAD))

echo "Will not build APK."