#!/bin/bash

inFile="$1"
appendText="$2"

if [ -z "$inFile" ]
then
	echo "Please provide a filename."
	exit 1
fi

if [ ! -f "$inFile" ]
then
	echo "File doesn't exist."
	exit 1
fi

currentVersion="$(grep versionCode app/build.gradle | awk '{print $2}')"
currentBuild="$(grep versionName app/build.gradle | awk '{print $2}' | cut -d '"' -f2)"

inFileBaseName=$(basename "$inFile")
inFileDirName=$(dirname "$inFile")

newFileBaseName="SafeSurfer-Android-${currentVersion}-${currentBuild}"
if [ ! -z "$CI_COMMIT_REF_NAME" ] && ! ( [ "$CI_COMMIT_REF_NAME" = "master" ] || [ "$CI_COMMIT_REF_NAME" = "tag" ] )
then
	newFileBaseName="$newFileBaseName-$CI_COMMIT_SHORT_SHA"
fi

if [ ! -z "$appendText" ]
then
    newFileBaseName="$newFileBaseName-$appendText"
fi

case "$inFileBaseName" in
	*.apk)
		mv "$inFile" "$inFileDirName/$newFileBaseName.apk" || echo "Unable to rename APK."
	;;

	*)
		echo "File given isn't applicable. Ignoring..."
	;;
esac
