package com.safesurfer.screens.reset_pin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.safesurfer.R;

public class ResetPinAuthActivity extends AppCompatActivity {

    ResetPinAuthActivityViewController viewController;
    ResetPinAuthActivityDataProvider dataProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_reset_pin_auth);

        viewController = new ResetPinAuthActivityViewController(this);
        dataProvider = new ResetPinAuthActivityDataProvider();

        viewController.setListener((email, password) -> dataProvider.userAuthenticate(email, password));

        dataProvider.setListener(new ResetPinAuthActivityDataProvider.ResetPinAuthActivityDataProviderListener() {
            @Override
            public void onAuthenticateSuccess() {
                viewController.onSuccessAuthenticate();
            }

            @Override
            public void onAuthenticateFail() {
                viewController.onFailAuthenticate();
            }
        });

        viewController.setUpView();

    }
}
