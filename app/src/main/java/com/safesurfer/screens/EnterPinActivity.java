package com.safesurfer.screens;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.Vibrator;
import android.text.InputType;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.frostnerd.general.Utils;
import com.frostnerd.preferences.util.VariableChecker;
import com.google.android.material.textfield.TextInputLayout;
import com.safesurfer.BuildConfig;
import com.safesurfer.LogFactory;
import com.safesurfer.R;
import com.safesurfer.screens.reset_pin.ResetPinAuthActivity;
import com.safesurfer.services.VirtualTunnelService;
import com.safesurfer.util.Constants;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.Tools;
import com.safesurfer.util.Util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.safesurfer.util.Tools.getMacAddr;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 * <p>
 * This file is part of SafeSurfer-Android.
 * <p>
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class EnterPinActivity extends AppCompatActivity {
    private static final String LOG_TAG = "[PinActivity]";

    private TextInputLayout met;
    private EditText pinInput;
    private Button startProtection;
    private Button startScreenBlackout;
    private String pin;

    private Vibrator vibrator;
    private Handler handler;
    private BroadcastReceiver importFinishedReceiver;
    SharedPreferences sharedPreferences;
    private CancellationSignal fingerprintCancellationSignal;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        final String altCode = BuildConfig.VERSION_NAME + getMacAddr() + new SimpleDateFormat("yyyyMMdd").format(new Date());
        final String altCode_hash = hashMD5(altCode);

        sharedPreferences = getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);

        if (sharedPreferences.getBoolean(Constants.REGISTRATION_PREFERENCES_KEY_ISFIRSTTIME, true)) {
            sharedPreferences.edit().putBoolean(Constants.REGISTRATION_PREFERENCES_KEY_ISFIRSTTIME, false).apply();
        }
        LogFactory.writeMessage(this, LOG_TAG, "Created Activity", getIntent());
        final boolean main = getIntent() != null && !getIntent().hasExtra("redirectToService");
        LogFactory.writeMessage(this, LOG_TAG, "Returning to main after pin: " + main);

        try {
            if (!PreferencesAccessor.isPinProtectionEnabled(this)) {
                SharedPreferences _sp = getSharedPreferences("lock", 0);
                String data = _sp.getString("pin", "No Value");

                if (data != "No Value") {
                    PreferencesAccessor.setPinEnabled(this, true);
                    PreferencesAccessor.setPinOnAppStart(this, true);
                    PreferencesAccessor.setPin(this, data);
                    PreferencesAccessor.setPinOnNotify(this, true);
                }
            }
        } catch (Exception ex) {
            LogFactory.writeMessage(EnterPinActivity.this, LOG_TAG, "Getting old Pin");
        }

        if (!PreferencesAccessor.isPinProtectionEnabled(this)) {
            LogFactory.writeMessage(this, LOG_TAG, "Pin is disabled");
            continueToFollowing();
            return;
        }
        if ((main && !PreferencesAccessor.isPinProtected(this, PreferencesAccessor.PinProtectable.APP)) ||
                (!main && (!PreferencesAccessor.isPinProtected(this, PreferencesAccessor.PinProtectable.NOTIFICATION) &&
                        !PreferencesAccessor.isPinProtected(this, PreferencesAccessor.PinProtectable.TILE) &&
                        !PreferencesAccessor.isPinProtected(this, PreferencesAccessor.PinProtectable.APP_SHORTCUT)))) {
            if (main && !PreferencesAccessor.isPinProtected(this, PreferencesAccessor.PinProtectable.APP)) {
                LogFactory.writeMessage(this, LOG_TAG, "We are going to main and pin for the app is not enabled. Not asking for pin");
            } else if (!main && !PreferencesAccessor.isPinProtected(this, PreferencesAccessor.PinProtectable.NOTIFICATION)) {
                LogFactory.writeMessage(this, LOG_TAG, "We are doing something in the notification and pin for it is not enabled. Not asking for pin");
            } else if (!main && !PreferencesAccessor.isPinProtected(this, PreferencesAccessor.PinProtectable.TILE)) {
                LogFactory.writeMessage(this, LOG_TAG, "We are doing something in the tiles and pin for it is not enabled. Not asking for pin");
            } else if (!main && !PreferencesAccessor.isPinProtected(this, PreferencesAccessor.PinProtectable.APP_SHORTCUT)) {
                LogFactory.writeMessage(this, LOG_TAG, "We are doing something in an app shortcut and pin for it is not enabled. Not asking for pin");
            }
            continueToFollowing();
        }
        LogFactory.writeMessage(this, LOG_TAG, "Have to ask for pin.");
        setContentView(R.layout.dialog_pin);
        LogFactory.writeMessage(this, LOG_TAG, "Content set");
        // Function to run when ok is pressed
        Runnable onDone = () -> {
            if (isFinishing()) return;
            if (pinInput.getText().toString().equals(pin) || pinInput.getText().toString().equals(altCode_hash) || pinInput.getText().toString().equals(altCode_hash.replaceAll("[^\\d.]", ""))) {
                LogFactory.writeMessage(EnterPinActivity.this, LOG_TAG, "Correct pin entered");
                met.setErrorEnabled(false);
                if (fingerprintCancellationSignal != null) {
                    fingerprintCancellationSignal.cancel();
                }
                continueToFollowing();
            } else {
                LogFactory.writeMessage(EnterPinActivity.this, LOG_TAG, "Incorrect pin entered");
                met.setErrorEnabled(true);
                met.setError(getResources().getString(R.string.pin_incorrect));
                vibrator.vibrate(200);
            }
        };
        pin = PreferencesAccessor.getPinCode(this);
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        met = findViewById(R.id.pin_dialog_met);
        pinInput = findViewById(R.id.pin_dialog_pin);
        startProtection = findViewById(R.id.pin_start_protection);
        startScreenBlackout = findViewById(R.id.pin_start_blackout);
        this.updateStartButtonState();
        pinInput.setOnKeyListener((view1, keyCode, keyEvent) -> {
            if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                onDone.run();
                return true;
            }
            return false;
        });
        if (!VariableChecker.isInteger(pin))
            pinInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        findViewById(R.id.pin_dialog_ok).setOnClickListener(v -> {
            onDone.run();
        });
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && PreferencesAccessor.canUseFingerprintForPin(this)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) == PackageManager.PERMISSION_GRANTED) {
                final FingerprintManager fingerprintManager = Utils.requireNonNull((FingerprintManager) getSystemService(FINGERPRINT_SERVICE));
                KeyguardManager keyguardManager = Utils.requireNonNull(getSystemService(KeyguardManager.class));
                if (fingerprintManager.isHardwareDetected() && fingerprintManager.hasEnrolledFingerprints() && keyguardManager.isKeyguardSecure()) {
                    handler = new Handler();
                    fingerprintCancellationSignal = new CancellationSignal();
                    findViewById(R.id.pin_fingerprint_icon).setVisibility(View.VISIBLE);
                    fingerprintManager.authenticate(null, fingerprintCancellationSignal, 0, new FingerprintManager.AuthenticationCallback() {
                        @Override
                        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
                            if (isFinishing()) return;
                            met.setErrorEnabled(false);
                            continueToFollowing();
                        }

                        @Override
                        public void onAuthenticationFailed() {
                            if (isFinishing()) return;
                            met.setErrorEnabled(true);
                            met.setError(getResources().getString(R.string.pin_incorrect));
                            vibrator.vibrate(200);
                            handler.postDelayed(() -> met.setErrorEnabled(false), 3500);
                        }
                    }, null);
                }
            }
        }
        LogFactory.writeMessage(this, LOG_TAG, "Activity fully created.");
        context = this;
        if (findViewById(R.id.button_reset_pin) != null) {

            if (Tools.getInstance().getDeviceAuthToken() != null) {
                findViewById(R.id.button_reset_pin).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.button_reset_pin).setVisibility(View.GONE);
            }

            findViewById(R.id.button_reset_pin).setOnClickListener(view -> {
                if (fingerprintCancellationSignal != null) {
                    fingerprintCancellationSignal.cancel();
                }
                Intent intent = new Intent(context, ResetPinAuthActivity.class);
                startActivity(intent);
                finish();
            });
        }
    }

    Context context;

    @NonNull
    private String hashMD5(@NonNull String s) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(s.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            System.out.println(bigInt.toString(16));
            return bigInt.toString(16);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return "";
    }

    private void continueToFollowing() {
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    protected void onStop() {
        if (!isFinishing()) finish();
        super.onStop();
    }

    /**
     * We allow protection to be started from the PIN screen, set whether this is currently available.
     */
    private void updateStartButtonState() {
        // If protection isn't running and private DNS isn't running, show enable
        Pair<Boolean, Boolean> privateDNSStatus = Tools.getInstance().getPrivateDNSStatus();
        if (!Util.isServiceRunning() && !privateDNSStatus.first) {
            startProtection.setVisibility(View.VISIBLE);
            startProtection.setOnClickListener(view -> {
                startService(new Intent(this, VirtualTunnelService.class));
                new Thread(() -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(() -> {
                        updateStartButtonState();
                    });
                }).start();
            });
        } else {
            startProtection.setVisibility(View.GONE);
        }
        try {
            if (!Tools.getInstance().needScreencastPermission() && !Tools.getInstance().isScreencastDetectionActive()) {
                startScreenBlackout.setVisibility(View.VISIBLE);
                startScreenBlackout.setOnClickListener(view -> {
                    Tools.getInstance().startScreencast(this, () -> {
                        new Thread(() -> {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            runOnUiThread(() -> {
                                updateStartButtonState();
                            });
                        }).start();
                    });
                });
            } else {
                startScreenBlackout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            startScreenBlackout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        LogFactory.writeMessage(this, LOG_TAG, "Destroying activity");
        if (importFinishedReceiver != null) unregisterReceiver(importFinishedReceiver);
        if (handler != null) handler.removeCallbacksAndMessages(null);
        importFinishedReceiver = null;
        met = null;
        pinInput = null;
        vibrator = null;
        handler = null;
        super.onDestroy();
    }
}
