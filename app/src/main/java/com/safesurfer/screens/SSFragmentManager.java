package com.safesurfer.screens;

import android.os.Bundle;

import java9.lang.FunctionalInterface;

@FunctionalInterface
public interface SSFragmentManager {
    void setFragment(MainActivity.SSFragment fragment, boolean doBackStack, boolean ignoreUnsavedChanges, Bundle args);
}
