package com.safesurfer.screens.subscription;

import android.content.Context;
import android.content.res.Resources;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.safesurfer.R;

import java.util.List;

public class TierAdapter extends RecyclerView.Adapter<TierAdapter.ViewHolder> {

    private List<Tier> data;

    public TierAdapter(List<Tier> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_pro_surfer_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Context context = holder.title.getContext();
        holder.title.setText(data.get(position).getTitle());
        holder.price.setText(data.get(position).getPrice());
        holder.freeTrialPeriod.setText(data.get(position).getFreeTrialPeriod());
        Resources resources = context.getResources();
        // Add each benefit
        for (Pair<Integer, Integer> benefit : data.get(position).getBenefits()) {
            LinearLayout.LayoutParams rowParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            rowParams.setMargins(10,10,10,10);
            LinearLayout.LayoutParams iconParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            iconParams.setMargins(30,10,10,10);
            LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            textParams.setMargins(10,15,10,10);
            LinearLayout row = new LinearLayout(context);
            row.setLayoutParams(rowParams);
            row.setOrientation(LinearLayout.HORIZONTAL);
            ImageView icon = new ImageView(context);
            icon.setImageResource(benefit.first);
            icon.setLayoutParams(iconParams);
            row.addView(icon);
            TextView textView = new TextView(context);
            textView.setText(benefit.second);
            textView.setLayoutParams(textParams);
            textView.setTextColor(resources.getColor(R.color.black));
            row.addView(textView);
            holder.benefitsContainer.addView(row);
        }
        // Add badge for current subscription
        if (data.get(position).isPurchased()) {
            holder.mySubscriptionBadge.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private TextView price;
        private TextView freeTrialPeriod;
        private LinearLayout benefitsContainer;
        private CardView mySubscriptionBadge;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            price = itemView.findViewById(R.id.price);
            benefitsContainer = itemView.findViewById(R.id.benefits_container);
            mySubscriptionBadge = itemView.findViewById(R.id.my_subscription_badge);
            freeTrialPeriod = itemView.findViewById(R.id.free_trial_period);
        }
    }
}
