package com.safesurfer.screens.subscription;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.billingclient.api.SkuDetails;
import com.safesurfer.R;
import com.safesurfer.screens.MainActivity;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.util.SubscriptionUtils;
import com.yarolegovich.discretescrollview.DSVOrientation;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubscriptionActivity extends PINProtectedActivity implements DiscreteScrollView.OnItemChangedListener<TierAdapter.ViewHolder> {

    public static final String INTENT_EXTRA_SKIP_ON_ALREADY_HAS_PRO = "com.safesurfer.skip_on_pro";
    public static final String INTENT_EXTRA_SHOW_CLOSE = "com.safesurfer.show_close";

    /**
     * For removing the app title from subscription text.
     */
    private static final String SKU_TITLE_APP_NAME_REGEX = "(?> \\(.+?\\))$";
    private static final Pattern SKU_TITLE_APP_NAME_PATTERN = Pattern.compile(SKU_TITLE_APP_NAME_REGEX, Pattern.CASE_INSENSITIVE);

    /**
     * Declare benefits for each subscription.
     */
    private static final Map<String, List<Pair<Integer, Integer>>> ID_TO_BENEFITS = new HashMap<String, List<Pair<Integer, Integer>>>(){
        {
            put("pro_surfer_0_1", Arrays.asList(
                    new Pair(
                            R.drawable.ic_baseline_history_24,
                            R.string.pro_surfer_0_1_benefit_history
                    ),
                    new Pair(
                            R.drawable.ic_baseline_sync_disabled_24,
                            R.string.pro_surfer_0_1_protection_disabled
                    ),
                    new Pair(
                            R.drawable.ic_baseline_mail_outline_24,
                            R.string.pro_surfer_0_1_benefit_mail
                    ),
                    new Pair(
                            R.drawable.ic_baseline_cast_24,
                            R.string.pro_surfer_0_1_benefit_cast
                    ),
                    new Pair(
                            R.drawable.ic_baseline_notifications_24,
                            R.string.menu_chat_guard
                    ),
                    new Pair(
                            R.drawable.ic_baseline_app_blocking_24,
                            R.string.pro_surfer_0_1_benefit_block
                    ),
                    new Pair(
                            R.drawable.ic_baseline_web_24,
                            R.string.pro_surfer_0_1_benefit_web
                    )
            ));
            put("pro_surfer_1_2", Arrays.asList(
                    new Pair(
                            R.drawable.ic_baseline_filter_2_24,
                            R.string.pro_surfer_1_2_benefit
                    )
            ));
            put("pro_surfer_3_4", Arrays.asList(
                    new Pair(
                            R.drawable.ic_baseline_filter_4_24,
                            R.string.pro_surfer_3_4_benefit
                    )
            ));
            put("pro_surfer_5_50", Arrays.asList(
                    new Pair(
                            R.drawable.ic_baseline_filter_5_24,
                            R.string.pro_surfer_5__benefit
                    )
            ));
        }
    };

    private List<Tier> data = new ArrayList<>();

    private LinearLayout loadingLayout;
    private LinearLayout textLayout;
    private TextView textView;
    private LinearLayout loadedLayout;
    private Button skipButton;
    private Button getButton;
    private DiscreteScrollView itemPicker;
    private int adapterPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        // Get views
        loadingLayout = findViewById(R.id.loading_layout);
        textLayout = findViewById(R.id.text_layout);
        textView = findViewById(R.id.text_layout_text);
        loadedLayout = findViewById(R.id.loaded_layout);
        skipButton = findViewById(R.id.pro_surfer_button_skip);
        getButton = findViewById(R.id.pro_surfer_button_get);
        // Allow showing "close" instead
        if (getIntent().getBooleanExtra(INTENT_EXTRA_SHOW_CLOSE, false)) {
            skipButton.setText(R.string.pro_surfer_button_close);
        }
        // Make OK buttons go to the main activity
        View.OnClickListener toMain = view -> {
            if (getIntent().getBooleanExtra(INTENT_EXTRA_SHOW_CLOSE, false)) {
                finish();
            } else {
                Intent intent = new Intent(SubscriptionActivity.this, MainActivity.class).putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
                startActivity(intent);
                finish();
            }
        };
        findViewById(R.id.text_view_ok).setOnClickListener(toMain);
        findViewById(R.id.pro_surfer_button_skip).setOnClickListener(toMain);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        // Get button listener
        getButton.setOnClickListener(view -> {
            this.onGet();
        });
        new Thread(() -> {
            SubscriptionUtils.init(this);
            SubscriptionUtils.onInit(result -> {
                this.onSubscriptionsLoaded(result);
            });
        }).start();
    }

    /**
     * Once subscription data is here, take away the loading
     * view and show the data.
     */
    private void onSubscriptionsLoaded(SubscriptionUtils.Result result) {
        switch (result) {
            case INIT_FAILED:
            case PURCHASE_FAILED_UNKNOWN:
            case PRO_WAITING_FOR_SIGN_IN: // We should never get to this activity if we aren't signed in already
                runOnUiThread(() -> {
                    this.showTextView(R.string.subscription_activity_failed);
                });
                return;
            case PURCHASE_FAILED_BILLING:
                runOnUiThread(() -> {
                    this.showTextView(R.string.subscription_activity_billing_failed);
                });
                return;
            case PRO_NOT_AVAILABLE:
                runOnUiThread(() -> {
                    this.showTextView(R.string.subscription_activity_not_available);
                });
                return;
            case PURCHASE_FAILED_CONFLICT:
                runOnUiThread(() -> {
                    this.showTextView(R.string.pro_signup_error_conflict_body);
                });
                return;
            case INIT_SUCCESS:
                SubscriptionUtils.JWTClaims claims = SubscriptionUtils.parseDeviceJWTClaims();
                if (SubscriptionUtils.shouldOfferSignUp(claims)) {
                    // Show available plans
                    this.initItemPicker();
                } else {
                    // This one is slightly more complex. In some cases, we may want
                    // to show the plans anyway so the user can switch. In others,
                    // we want to just continue.
                    boolean skipOnAlreadyHasPro = getIntent().getBooleanExtra(INTENT_EXTRA_SKIP_ON_ALREADY_HAS_PRO, false);
                    if (skipOnAlreadyHasPro) {
                        Intent intent = new Intent(SubscriptionActivity.this, MainActivity.class).putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
                        startActivity(intent);
                        finish();
                    } else {
                        this.initItemPicker();
                    }
                }
        }
    }

    /**
     * Show the item picker and fill it with data.
     */
    private void initItemPicker() {
        List<SkuDetails> skus = SubscriptionUtils.getSkuDetailsList();
        SubscriptionUtils.JWTClaims claims = SubscriptionUtils.parseDeviceJWTClaims();
        String relevantPurchaseSKU = claims.getPlanID();
        boolean subscriptionNotCancelled = !claims.getSubStatus().equals(SubscriptionUtils.SUBSCRIPTION_STATUS_CANCELLED) &&
                !claims.getSubStatus().equals(SubscriptionUtils.SUBSCRIPTION_STATUS_NON_RENEWING);
        int relevantPurchaseSkuIndex = -1;
        data.clear();
        for (int i = 0; i < skus.size(); i++) {
            SkuDetails sku = skus.get(i);
            if (sku.getSku().equals(relevantPurchaseSKU)) {
                relevantPurchaseSkuIndex = i;
            }
            if (ID_TO_BENEFITS.containsKey(sku.getSku())) {
                Matcher skuTitleAppNameMatcher = SKU_TITLE_APP_NAME_PATTERN.matcher(sku.getTitle());
                data.add(new Tier(
                        sku.getSku(),
                        skuTitleAppNameMatcher.replaceAll(""),
                        sku.getPrice() + " " + sku.getPriceCurrencyCode() + " " + getResources().getString(R.string.per_month),
                        getResources().getString(R.string.free_trial),
                        relevantPurchaseSkuIndex == i && subscriptionNotCancelled,
                        ID_TO_BENEFITS.get(sku.getSku())
                ));
            }
        }
        final int skuIdx = relevantPurchaseSkuIndex;
        runOnUiThread(() -> {
            loadingLayout.setVisibility(View.GONE);
            loadedLayout.setVisibility(View.VISIBLE);
            itemPicker = findViewById(R.id.item_picker);
            itemPicker.setOrientation(DSVOrientation.HORIZONTAL);
            itemPicker.addOnItemChangedListener(this);
            itemPicker.setAdapter(new TierAdapter(data));
            itemPicker.setItemTransitionTimeMillis(200);
            itemPicker.setItemTransformer(new ScaleTransformer.Builder()
                    .setMinScale(0.8f)
                    .build());
            if (skuIdx != -1) {
                itemPicker.scrollToPosition(skuIdx);
            }
        });
    }

    /**
     * Show a text view, e.g. if something failed.
     * @param text The ID of text to show in the view.
     */
    private void showTextView(int text) {
        this.loadingLayout.setVisibility(View.GONE);
        this.loadedLayout.setVisibility(View.GONE);
        this.textLayout.setVisibility(View.VISIBLE);
        this.textView.setText(text);
    }

    /**
     * Called when the get/manage button is pressed.
     */
    private void onGet() {
        Tier tier = this.data.get(this.adapterPosition);
        if (tier.isPurchased()) {
            // User already has this subscription, link to play store
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(
                    String.format(
                            "https://play.google.com/store/account/subscriptions?sku=%s&package=%s",
                            tier.getId(),
                            "com.safesurfer"
                            )
            ));
            startActivity(intent);
            return;
        }
        getButton.setEnabled(false);
        // User doesn't have this subscription, get it
        runOnUiThread(() -> {
            this.loadingLayout.setVisibility(View.VISIBLE);
            this.loadedLayout.setVisibility(View.GONE);
            SubscriptionUtils.launchBilling(this, tier.getId());
            SubscriptionUtils.onInit(result -> {
                getButton.setEnabled(true);
                this.onSubscriptionsLoaded(result);
            });
        });
    }

    /**
     * Update the get button text depending on if the user already has the subscription.
     */
    private void updateGetButtonText() {
        if (this.data.get(this.adapterPosition).isPurchased()) {
            getButton.setText(R.string.pro_surfer_button_manage);
        } else {
            getButton.setText(R.string.pro_surfer_button_get);
        }
    }

    @Override
    public void onCurrentItemChanged(@Nullable TierAdapter.ViewHolder viewHolder, int adapterPosition) {
        this.adapterPosition = adapterPosition;
        this.updateGetButtonText();
    }
}
