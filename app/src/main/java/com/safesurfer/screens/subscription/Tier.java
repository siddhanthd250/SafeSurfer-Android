package com.safesurfer.screens.subscription;

import android.util.Pair;

import java.util.List;

public class Tier {
    private final List<Pair<Integer, Integer>> benefits;
    private final String price;
    private final String title;
    private final boolean isPurchased;
    private final String id;
    private final String freeTrialPeriod;

    public Tier(String id, String title, String price, String freeTrialPeriod, boolean isPurchased, List<Pair<Integer, Integer>> benefits) {
        this.benefits = benefits;
        this.price = price;
        this.title = title;
        this.id = id;
        this.isPurchased = isPurchased;
        this.freeTrialPeriod = freeTrialPeriod;
    }

    public List<Pair<Integer, Integer>> getBenefits() {
        return benefits;
    }

    public String getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public boolean isPurchased() {
        return isPurchased;
    }

    public String getFreeTrialPeriod() {
        return freeTrialPeriod;
    }
}
