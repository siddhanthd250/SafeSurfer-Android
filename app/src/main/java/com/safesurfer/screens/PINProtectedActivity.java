package com.safesurfer.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.safesurfer.util.PreferencesAccessor;

public class PINProtectedActivity extends AppCompatActivity {
    public static final String INTENT_EXTRA_SKIP_PIN = "com.safesurfer.skip_pin";

    private boolean pinOnResume = false;
    private boolean startedActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.pinOnResume = !getIntent().getBooleanExtra(INTENT_EXTRA_SKIP_PIN, false) &&
                pinProtected();
    }

    private boolean pinProtected () {
        return PreferencesAccessor.isPinProtected(this, PreferencesAccessor.PinProtectable.APP);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.startedActivity = false;
        if (pinOnResume) {
            Intent i = new Intent(this, EnterPinActivity.class);
            startActivityForResult(i, 1111);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1111) {
            pinOnResume = resultCode != Activity.RESULT_OK;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.pinOnResume = pinProtected() && !startedActivity;
    }

    @Override
    public void startActivity(Intent intent) {
        this.startedActivity = true;
        super.startActivity(intent);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        this.startedActivity = true;
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    public void startActivityFromFragment(@NonNull android.app.Fragment fragment, Intent intent, int requestCode) {
        this.startedActivity = true;
        super.startActivityFromFragment(fragment, intent, requestCode);
    }

    @Override
    public void startActivityFromFragment(@NonNull android.app.Fragment fragment, Intent intent, int requestCode, @Nullable Bundle options) {
        this.startedActivity = true;
        super.startActivityFromFragment(fragment, intent, requestCode, options);
    }

    @Override
    public void startActivityFromFragment(Fragment fragment, Intent intent, int requestCode) {
        this.startedActivity = true;
        super.startActivityFromFragment(fragment, intent, requestCode);
    }

    @Override
    public void startActivityFromFragment(Fragment fragment, Intent intent, int requestCode, @Nullable Bundle options) {
        this.startedActivity = true;
        super.startActivityFromFragment(fragment, intent, requestCode, options);
    }
}
