package com.safesurfer.screens;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.collection.ArraySet;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;
import com.safesurfer.BuildConfig;
import com.safesurfer.fragments.PWAPageFragment;
import com.safesurfer.fragments.alerts.AlertsPage;
import com.safesurfer.fragments.social_media_monitoring.SocialMediaMonitoringFragment;
import com.safesurfer.fragments.social_media_monitoring.SocialMediaMonitoringPage;
import com.safesurfer.fragments.my_devices.MyDevicesPage;
import com.safesurfer.fragments.screen_blackout.ScreenBlackoutFragment;
import com.safesurfer.fragments.blocked_sites.BlockedSitesFragment;
import com.safesurfer.fragments.screen_blackout.ScreenBlackoutHistoryPage;
import com.safesurfer.fragments.usage.UsagePage;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.screens.registration.RegisterActivity;

import com.safesurfer.fragments.MainFragment;
import com.safesurfer.fragments.SettingsFragment;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.safesurfer.LogFactory;
import com.safesurfer.R;

import com.safesurfer.util.SubscriptionUtils;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerActivity;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerService;
import com.safesurfer.util.Constants;
import com.safesurfer.util.PreferencesAccessor;

import com.safesurfer.util.Tools;
import com.safesurfer.util.Util;
import com.safesurfer.util.Preferences;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import java9.util.Sets;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 * <p>
 * This file is part of SafeSurfer-Android.
 * <p>
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class MainActivity extends PINProtectedActivity {
    private static final String BACK_STACK_ROOT_TAG = "root_fragment";
    private static final String LOG_TAG = "[MainActivity]";

    private static final ScreenBlackoutHistoryPage SCREEN_BLACKOUT_HISTORY_PAGE = new ScreenBlackoutHistoryPage();
    private static final UsagePage USAGE_PAGE = new UsagePage();
    private static final MyDevicesPage MY_DEVICES_PAGE = new MyDevicesPage();
    private static final AlertsPage ALERTS_PAGE = new AlertsPage();
    private static final SocialMediaMonitoringPage CHAT_GUARD_PAGE = new SocialMediaMonitoringPage();

    private boolean showFragmentOnDrawerClose = false;

    // ID for each fragment
    public enum SSFragment {
        MAIN,
        BLOCKED_SITES,
        SCREEN_BLACKOUT,
        SCREEN_BLACKOUT_HISTORY,
        SETTINGS,
        ALERTS,
        DEVICES,
        USAGE,
        CHAT_GUARD
    }
    // Map from menu item ID to the fragment it needs
    private static final Map<Integer, SSFragment> MENU_ID_TO_FRAGMENT = new HashMap<Integer, SSFragment>(){{
        put(R.id.menu_home, SSFragment.MAIN);
        put(R.id.menu_blocked_sites, SSFragment.BLOCKED_SITES);
        put(R.id.menu_screencast_detection, SSFragment.SCREEN_BLACKOUT);
        put(R.id.menu_settings, SSFragment.SETTINGS);
        put(R.id.menu_alerts, SSFragment.ALERTS);
        put(R.id.menu_devices, SSFragment.DEVICES);
        put(R.id.menu_usage, SSFragment.USAGE);
        put(R.id.menu_chat_guard, SSFragment.CHAT_GUARD);
    }};

    private FragmentManager fragmentManager;
    private BlockedSitesFragment blockedSitesFragment;
    private PWAPageFragment pwaPageFragment;

    private DrawerLayout drawer;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navView;
    private View navHeader;
    private LinearLayout navHeaderSignedInView;
    private LinearLayout navHeaderNotSignedInView;
    private Toolbar toolbar;
    private MaterialButton signedInButton;
    private int lastSelectedMenuItem = 0;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            setContext(this);
            super.onCreate(savedInstanceState);
            boolean userAgreeTerms = Tools.getInstance().getUserAgreeTerms();
            if (!userAgreeTerms) {
                Intent intent = new Intent(this, RegisterActivity.class)
                        .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
                startActivity(intent);
                finish();
                return;
            }
            setContentView(R.layout.activity_main);
            toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            drawer = findViewById(R.id.drawer);
            drawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.open, R.string.close);
            drawer.addDrawerListener(drawerToggle);
            drawerToggle.syncState();
            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.transparent));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            pwaPageFragment = new PWAPageFragment(getSupportActionBar(), this::setMenuItem);
            pwaPageFragment.setAllPages(Sets.of(
                    SCREEN_BLACKOUT_HISTORY_PAGE,
                    USAGE_PAGE,
                    MY_DEVICES_PAGE,
                    ALERTS_PAGE,
                    CHAT_GUARD_PAGE
            ));
            toolbar.setNavigationIcon(R.drawable.ic_baseline_menu_24);
            fragmentManager = getSupportFragmentManager();
            if (savedInstanceState == null) {
                this.setFragment(MENU_ID_TO_FRAGMENT.get(R.id.menu_home), false, false, null);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            }
            this.navView = findViewById(R.id.navigation_view);
            this.navView.setNavigationItemSelectedListener(item -> {
                if (checkNavViewMoreItem(item)) {
                    drawer.closeDrawer(GravityCompat.START);
                    return true;
                }
                this.navView.setCheckedItem(item);
                drawer.closeDrawer(GravityCompat.START);
                this.lastSelectedMenuItem = item.getItemId();
                this.showFragmentOnDrawerClose = true;
                return true;
            });
            this.drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {}
                @Override
                public void onDrawerOpened(@NonNull View drawerView) {}
                @Override
                public void onDrawerStateChanged(int newState) {}

                @Override
                public void onDrawerClosed(@NonNull View drawerView) {
                    if (MainActivity.this.showFragmentOnDrawerClose) {
                        MainActivity.this.setFragment(MENU_ID_TO_FRAGMENT.get(MainActivity.this.lastSelectedMenuItem), true, false, null);
                        MainActivity.this.showFragmentOnDrawerClose = false;
                    }
                }
            });
            this.navHeader = this.navView.getHeaderView(0);
            this.navHeaderSignedInView = this.navHeader.findViewById(R.id.signed_in_layout);
            this.navHeaderNotSignedInView = this.navHeader.findViewById(R.id.not_signed_in_layout);
            // If not signed in, add action to sign in (nav header)
            this.navHeader.findViewById(R.id.not_signed_in_button).setOnClickListener(view -> {
                Intent intent = new Intent(this, RegisterActivity.class)
                        .putExtra(RegisterActivity.INTENT_EXTRA_FINISH_SETUP, false)
                        .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
                startActivity(intent);
            });
            // If signed in, add action to manage subscription (nav header)
            this.signedInButton = this.navHeader.findViewById(R.id.signed_in_button);
            this.signedInButton.setOnClickListener(view -> {
                startActivity(Util.getProIntent(this));
            });
            this.updateNavContents();
            Util.runBackgroundConnectivityCheck(this, true);
            final Preferences preferences = Preferences.getInstance(this);

            if (preferences.getBoolean("first_run", true))
                preferences.put("setting_show_notification", false);
            if (preferences.getBoolean("first_run", true))
                preferences.put("setting_start_boot", true);
            if (preferences.getBoolean("first_run", true))
                preferences.put("setting_auto_wifi", true);
            if (preferences.getBoolean("first_run", true))
                preferences.put("excluded_apps", new ArraySet<>(Arrays.asList(getResources().getStringArray(R.array.default_blacklist))));
            if (preferences.getBoolean("first_run",
                    true))
                preferences.put("setting_auto_mobile", true);
            if (preferences.getBoolean("first_run", true))
                preferences.put("check_connectivity", true);
            int random = new Random().nextInt(100), launches = preferences.getInteger("launches", 0);
            preferences.put("launches", launches + 1);
            if (launches >= 5 && !preferences.getBoolean("first_run", true) &&
                    !preferences.getBoolean("rated", false) && random <= 16) {
                LogFactory.writeMessage(this, LOG_TAG, "Showing dialog requesting rating");
                new AlertDialog.Builder(this).setPositiveButton(R.string.ok, (dialog, which) -> rateApp()).setNegativeButton(R.string.dont_ask_again, (dialog, which) -> {
                    preferences.put("rated", true);
                    dialog.cancel();
                }).setNeutralButton(R.string.not_now, (dialog, which) -> dialog.cancel()).setMessage(R.string.rate_request_text).setTitle(R.string.rate).show();
                LogFactory.writeMessage(this, LOG_TAG, "Dialog is now being shown");
            }
            preferences.putBoolean("first_run", false);
            // Clear the webview cache each time we run the app
            new WebView(this).clearCache(true);
            // Schedule the protection polling
            // Disabled for now, doesn't seem to work & need to account for private DNS
//            PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(
//                    ProtectionStatusWorker.class,
//                    15,
//                    TimeUnit.MINUTES
//            ).build();
//            WorkManager
//                    .getInstance(getContext())
//                    .enqueueUniquePeriodicWork(
//                            "protection_status",
//                            ExistingPeriodicWorkPolicy.REPLACE,
//                            periodicWorkRequest
//                    );
        } catch (Exception ex) {
            LogFactory.writeMessage(this, LOG_TAG, ex.toString());
        }

        if (Tools.getInstance().isMyServiceRunning(ScreenCapturerService.class)) {
            Log.d(getClass().getName(), "ScreenCapturerService is running");
        } else {
            Log.d(getClass().getName(), "ScreenCapturerService is not running");

            if (Tools.getInstance().isScreencastDetectionActive()) {
                Log.d(getClass().getName(), "run ScreenCapturerService");
                Intent intent = new Intent(Tools.getInstance().getContext(), ScreenCapturerActivity.class);
                startActivity(intent);
            }
        }

        checkReAuth();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    /**
     * Set the fragment to display.
     * @param fragment The ID constant for the fragment we want to display.
     * @param doBackStack Whether to pop/push to the back stack.
     * @param ignoreUnsavedChanges Whether to change the fragment regardless of if it would
     *                             lose changes.
     */
    public void setFragment(SSFragment fragment, boolean doBackStack, boolean ignoreUnsavedChanges, Bundle args) {
        if (!ignoreUnsavedChanges &&
                fragment != SSFragment.BLOCKED_SITES &&
                blockedSitesFragment != null &&
                blockedSitesFragment.isVisible() &&
                blockedSitesFragment.getHasChanges()) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.unsaved_changes)
                    .setNegativeButton(R.string.cancel, ((dialogInterface, i) -> {
                        navView.setCheckedItem(R.id.menu_blocked_sites);
                        dialogInterface.dismiss();
                    }))
                    .setPositiveButton(R.string.abandon, (dialogInterface, i) -> {
                        setFragment(fragment, doBackStack, true, args);
                    })
                    .show();
            return;
        }
        FragmentTransaction tx = fragmentManager
                .beginTransaction();
        Fragment fragmentImpl = null;
        switch (fragment) {
            case MAIN:
                fragmentImpl = new MainFragment(getSupportActionBar(), this::setMenuItem);
                break;
            case SETTINGS:
                fragmentImpl = new SettingsFragment(getSupportActionBar(), this::setMenuItem);
                break;
            case BLOCKED_SITES:
                blockedSitesFragment = new BlockedSitesFragment(getSupportActionBar(), this::setMenuItem);
                fragmentImpl = blockedSitesFragment;
                break;
            case SCREEN_BLACKOUT:
                fragmentImpl = new ScreenBlackoutFragment(getSupportActionBar(), this::setMenuItem, this::setFragment);
                break;
            case SCREEN_BLACKOUT_HISTORY:
                fragmentImpl = pwaPageFragment;
                pwaPageFragment.setCurrPage(SCREEN_BLACKOUT_HISTORY_PAGE);
                break;
            case ALERTS:
                fragmentImpl = pwaPageFragment;
                pwaPageFragment.setCurrPage(ALERTS_PAGE);
                break;
            case DEVICES:
                fragmentImpl = pwaPageFragment;
                pwaPageFragment.setCurrPage(MY_DEVICES_PAGE);
                break;
            case USAGE:
                fragmentImpl = pwaPageFragment;
                pwaPageFragment.setCurrPage(USAGE_PAGE);
                break;
            case CHAT_GUARD:
                // If we are signed out, basic, or haven't given permissions, direct to a placeholder
                // fragment that will show what to do.
                if (!Tools.getInstance().isNotificationServiceEnabled() ||
                        PreferencesAccessor.getAccountAccessRevoked(this) ||
                        Tools.getInstance().getDeviceAuthToken() == null ||
                    SubscriptionUtils.getSubscriptionBadgeMode(SubscriptionUtils.parseDeviceJWTClaims()) == SubscriptionUtils.SubscriptionBadgeMode.BASIC) {
                    fragmentImpl = new SocialMediaMonitoringFragment(getSupportActionBar(), this::setMenuItem, this::setFragment);
                } else {
                    fragmentImpl = pwaPageFragment;
                    pwaPageFragment.setCurrPage(CHAT_GUARD_PAGE);
                }
                break;
        }
        if (fragmentImpl != null) {
            fragmentImpl.setArguments(args);
            tx.replace(R.id.fragment_container, fragmentImpl);
        }
        if (doBackStack) {
            tx.addToBackStack(BACK_STACK_ROOT_TAG);
        }
        tx.commit();
    }

    /**
     * On the navigation bar, set the item as being selected.
     * @param menuItemID The ID of the item to select.
     */
    private void setMenuItem(int menuItemID) {
        if (navView != null) {
            navView.setCheckedItem(menuItemID);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            drawer.openDrawer(GravityCompat.START);
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (blockedSitesFragment != null &&
                blockedSitesFragment.isVisible() &&
                blockedSitesFragment.getHasChanges()) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.unsaved_changes)
                    .setNegativeButton(R.string.cancel, null)
                    .setPositiveButton(R.string.abandon, (dialogInterface, i) -> {
                        super.onBackPressed();
                    })
                    .show();
        } else if (pwaPageFragment != null && pwaPageFragment.isVisible()) {
            if (!pwaPageFragment.goBack()) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Initialize billing if it isn't already.
     */
    private void initBilling() {
        // Check billing
        if (SubscriptionUtils.getState() != SubscriptionUtils.BillingState.INITIALIZING) {
            new Thread(() -> {
                SubscriptionUtils.init(this);
                SubscriptionUtils.onInit(result -> {
                    runOnUiThread(() -> {
                        this.updateNavContents();
                    });
                });
            }).start();
        }
    }

    /**
     * Update the navigation header with who is signed in (if any),
     * what the subscription status is, etc.
     */
    private void updateNavContents() {
        SubscriptionUtils.JWTClaims claims = SubscriptionUtils.parseDeviceJWTClaims();
        this.navHeaderSignedInView.setVisibility(View.GONE);
        this.navHeaderNotSignedInView.setVisibility(View.GONE);
        this.navView.getMenu().clear();
        if (claims == null || PreferencesAccessor.getAccountAccessRevoked(this)) {
            // Not signed in
            this.navHeaderNotSignedInView.setVisibility(View.VISIBLE);
            this.navView.inflateMenu(R.menu.navigation_drawer_not_signed_in);
            if (PreferencesAccessor.getAccountAccessRevoked(this)) {
                ((TextView) this.navHeaderNotSignedInView.findViewById(R.id.not_signed_in_text)).setText(R.string.not_signed_in_text_revoked);
            } else {
                ((TextView) this.navHeaderNotSignedInView.findViewById(R.id.not_signed_in_text)).setText(R.string.not_signed_in_text);
            }
        } else if (SubscriptionUtils.getState() != SubscriptionUtils.BillingState.INITIALIZED) {
            navView.inflateMenu(R.menu.navigation_drawer_not_pro);
        } else {
            // Set subscription badge
            this.navHeaderSignedInView.setVisibility(View.VISIBLE);
            ((TextView) this.navHeaderSignedInView.findViewById(R.id.signed_in_text)).setText(
                    getSharedPreferences(Constants.REGISTRATION_PREFERENCES, Context.MODE_PRIVATE)
                        .getString(Constants.REGISTRATION_PREFERENCES_KEY_EMAIL, "")
            );
            LinearLayout badgeBackground = this.navHeaderSignedInView.findViewById(R.id.signed_in_badge_background);
            TextView badgeText = this.navHeaderSignedInView.findViewById(R.id.signed_in_badge_text);
            this.signedInButton.setText(R.string.signed_in_button);
            SubscriptionUtils.SubscriptionPlatform platform = SubscriptionUtils.getSubscriptionPlatform(claims);
            switch (SubscriptionUtils.getSubscriptionBadgeMode(claims)) {
                case PRO:
                    badgeBackground.setBackgroundResource(R.drawable.primary_round_background);
                    navView.inflateMenu(R.menu.navigation_drawer);
                    if (platform == SubscriptionUtils.SubscriptionPlatform.WEB) {
                        this.signedInButton.setVisibility(View.GONE);
                        this.findViewById(R.id.signed_in_text_margin).setVisibility(View.VISIBLE);
                    }
                    badgeText.setText(R.string.subscription_badge_pro);
                    break;
                case TRIAL:
                    badgeBackground.setBackgroundResource(R.drawable.secondary_round_background);
                    navView.inflateMenu(R.menu.navigation_drawer);
                    if (platform == SubscriptionUtils.SubscriptionPlatform.WEB) {
                        this.signedInButton.setVisibility(View.GONE);
                        this.findViewById(R.id.signed_in_text_margin).setVisibility(View.VISIBLE);
                    }
                    badgeText.setText(R.string.subscription_badge_trial);
                    break;
                default:
                    badgeBackground.setBackgroundResource(R.drawable.secondary_round_background);
                    navView.inflateMenu(R.menu.navigation_drawer_not_pro);
                    if (platform == SubscriptionUtils.SubscriptionPlatform.WEB) {
                        this.signedInButton.setVisibility(View.GONE);
                        this.findViewById(R.id.signed_in_text_margin).setVisibility(View.VISIBLE);
                    }
                    this.signedInButton.setText(R.string.get_pro_surfer_short);
                    badgeText.setText(R.string.subscription_badge_basic);
            }
        }
    }

    /**
     * Check whether we need to do an action for a nav item in the "more" section,
     * and if so, do it.
     * @param item The clicked item.
     * @return True if we have done an action.
     */
    private boolean checkNavViewMoreItem(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_about:
                showAbout();
                return true;
            case R.id.menu_help:
                showHelp();
                return true;
            case R.id.menu_open_source_licenses:
                showOpenSourceLicenses();
                return true;
            default:
                return false;
        }
    }

    /**
     * Show information about this app.
     */
    private void showAbout() {
        String text = getString(R.string.about_text)
                .replace("[[version]]", BuildConfig.VERSION_NAME)
                .replace("[[build]]", BuildConfig.VERSION_CODE + "")
                .replace("[[githash]]", BuildConfig.GitHash + "");
        ;
        new AlertDialog.Builder(MainActivity.this).setTitle(R.string.title_about).setMessage(text)
                .setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    /**
     * Open the helpdesk.
     */
    private void showHelp() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://helpdesk.safesurfer.io"));
        startActivity(browserIntent);
    }

    /**
     * Show the open source licenses.
     */
    private void showOpenSourceLicenses() {
        String licenseText = "\n\n" + getString(R.string.dialog_libraries_text) + "";
        licenseText += "\n\n- - - - - - - - - - - -\nSafeSurfer v6 Copyright Daniel Wolf 2017\n\nGNU General Public License v3.0 with exceptions granted.";
        licenseText += "\n\n- - - - - - - - - - - -\ndnsjava by Brian Wellington - http://www.xbill.org/dnsjava/\n\n" + getString(R.string.license_bsd_2).replace("[yearrange]", "1998-2011").replace("[author]", "Brian Wellington");
        licenseText += "\n\n- - - - - - - - - - - -\nfirebase-jobdispatcher-android by Google\n\nAvailable under the [1]Apache License 2.0[2]";
        licenseText += "\n\n- - - - - - - - - - - -\nPrivacyGuard Copyright (C) 2014  Yihang Song\n\n[3]GNU General Public License v2.0[4]";
        licenseText += "\n\n- - - - - - - - - - - -\nMiniDNS by Measite\n\nAvailable under the [5]Apache License 2.0[6]";
        licenseText += "\n\n- - - - - - - - - - - -\nMaterial icon pack by Google\n\nAvailable under the [7]Apache License 2.0[8]";
        licenseText += "\n\n- - - - - - - - - - - -\nGson by google\n\nAvailable under the [9]Apache License 2.0[a]";
        licenseText += "\n\n- - - - - - - - - - - -\nGuava by google\n\nAvailable under the [b]Apache License 2.0[c]";
        licenseText += "\n\n- - - - - - - - - - - -\nMaterial Components for Android\n\nAvailable under the [d]Apache License 2.0[e]";
        licenseText += "\n\n- - - - - - - - - - - -\nDiscrete Scrollview by Yaroslav Shevchuk\n\nAvailable under the [f]Apache License 2.0[g]";
        ClickableSpan span = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
            }
        }, span3 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
            }
        }, span4 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
            }
        }, span5 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
            }
        }, span6 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
            }
        }, span7 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
            }
        }, span8 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
            }
        }, span9 = new ClickableSpan(){
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(MainActivity.this).setTitle("GNU General Public License v2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_gpl_2).show();
                }
        };
        SpannableString spannable = new SpannableString(licenseText.replaceAll("\\[.]", ""));
        spannable.setSpan(span3, licenseText.indexOf("[1]"), licenseText.indexOf("[2]") - 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(span9, licenseText.indexOf("[3]") - 6, licenseText.indexOf("[4]") - 9, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(span, licenseText.indexOf("[5]") - 12, licenseText.indexOf("[6]") - 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(span4, licenseText.indexOf("[7]") - 18, licenseText.indexOf("[8]") - 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(span5, licenseText.indexOf("[9]") - 24, licenseText.indexOf("[a]") - 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(span6, licenseText.indexOf("[b]") - 30, licenseText.indexOf("[c]") - 33, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(span7, licenseText.indexOf("[d]") - 36, licenseText.indexOf("[e]") - 39, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(span8, licenseText.indexOf("[f]") - 42, licenseText.indexOf("[g]") - 45, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        AlertDialog dialog1 = new AlertDialog.Builder(MainActivity.this).setTitle(R.string.nav_title_libraries).setNegativeButton(R.string.close, null)
                .setMessage(spannable).show();
        ((TextView) dialog1.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public SharedPreferences getSharedPreferences(String name, int mode) {
        if (name == "com.safesurfer.registration") {
            return super.getSharedPreferences(name, mode);
        }
        return Preferences.getInstance(this);
    }

    public void rateApp() {
        final String appPackageName = this.getPackageName();
        LogFactory.writeMessage(this, LOG_TAG, "Opening site to rate app");
        try {
            LogFactory.writeMessage(this, LOG_TAG, "Trying to open market");
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            LogFactory.writeMessage(this, LOG_TAG, "Market was opened");
        } catch (android.content.ActivityNotFoundException e) {
            LogFactory.writeMessage(this, LOG_TAG, "Market not present. Opening with general ACTION_VIEW");
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
        Preferences.getInstance(this).put("rated", true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.setMenuItem(this.lastSelectedMenuItem);
        this.initBilling();
    }

    /**
     * Check whether we need to reauthenticate, e.g. if our access token has been revoked.
     * This may also trigger a re-signup if we're upgrading to version 73 or higher.
     */
    private void checkReAuth(){
        String JWTToken = Tools.getInstance().getDeviceAuthToken();
        String deviceId = Tools.getInstance().getDeviceId();
        if(JWTToken == null){
            return;
        }
        Runnable showReAuth = () -> {
            Log.d(getClass().getName(), "auth error. Do register again");
            Toast.makeText(context, R.string.auth_has_expired, Toast.LENGTH_SHORT ).show();
            Intent intent = new Intent(this, RegisterActivity.class)
                    .putExtra(RegisterActivity.INTENT_EXTRA_FINISH_SETUP, false)
                    .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
            startActivity(intent);
        };
        if (!PreferencesAccessor.getPost73ReAuthDone(this)) {
            // Do a re-setup process
            // Invalidate the current auth token. But first make sure we have a DNS token, or
            // signing out may mess up usage data.
            SafeSurferNetworkApi.getInstance().getSRUseCase().getDNSToken(JWTToken, deviceId)
                    .subscribe(result -> {
                        if (result.code() == 200) {
                            PreferencesAccessor.setDNSUUID(this, result.body().string().replaceAll("^\"|\"$", ""));
                        }
                        if (result.code() == 200 || result.code() == 401) {
                            SafeSurferNetworkApi.getInstance().getSRUseCase().postDeviceSignout(JWTToken, deviceId)
                                    .subscribe(result2 -> {
                                        switch (result2.code()) {
                                            case 201:
                                            case 401:
                                                // Either invalid now, or was already
                                                showReAuth.run();
                                        }
                                    });
                        }
                    });
            return;
        }
        SafeSurferNetworkApi.getInstance().getSRUseCase().getScreencasts(
                JWTToken,
                "1",
                "100",
                deviceId
        ).subscribe(success->{
            if(success == null)return;
            switch (success.code()){
                case 401:
                    if (!PreferencesAccessor.getAccountAccessRevoked(this)) {
                        showReAuth.run();
                    }
                    break;
            }
        }, fail->{
            fail.printStackTrace();
        });
    }
}
