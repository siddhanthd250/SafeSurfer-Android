package com.safesurfer.screens;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Pair;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.safesurfer.R;
import com.safesurfer.services.VirtualTunnelService;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.Tools;

public class EnablePrivateDNSActivity extends PINProtectedActivity {

    private Button copyLink;
    private Button openSettings;
    ServiceConnection mSc;
    VirtualTunnelService mVPN;
    private boolean bounded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.checkRedirect();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        mSc = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mVPN = ((VirtualTunnelService.VirtualTunnelServiceBinder) service).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
            }
        };
        setContentView(R.layout.activity_enable_private_dns);
        copyLink = findViewById(R.id.copy_link);
        copyLink.setOnClickListener(v -> {
            String dnsUUID = PreferencesAccessor.getDNSUUID(this);
            if (dnsUUID == null) {
                dnsUUID = "";
            } else {
                dnsUUID += ".";
            }
            dnsUUID += "dns.safesurfer.io";
            setClipboard(this, dnsUUID);
            Toast.makeText(this, R.string.copied,
                    Toast.LENGTH_SHORT).show();
        });
        openSettings = findViewById(R.id.open_settings);
        openSettings.setOnClickListener(v -> {
            // Stop protection
            if (bounded) {
                this.unbindService(mSc);
                bounded = false;
            }
            mVPN.stopVPN();
            final Intent intent = new Intent(Settings.ACTION_SETTINGS);
            if (intent.resolveActivity(this.getPackageManager()) != null) {
                try {
                    Toast.makeText(this, R.string.enable_private_dns_toast,
                            Toast.LENGTH_LONG).show();
                    startActivity(intent);
                } catch (Exception e) {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.no_fix_private_dns_title)
                            .setMessage(R.string.no_fix_private_dns_message)
                            .setPositiveButton(R.string.no_battery_optimization_ok, null)
                            .show();
                }
            }
        });
        if (!bounded) {
            Intent service = new Intent(this, VirtualTunnelService.class);
            bindService(service, mSc, Context.BIND_AUTO_CREATE);
            bounded = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.checkRedirect();
    }

    /**
     * If private DNS is already enabled, redirect to the main activity.
     */
    private void checkRedirect() {
        Pair<Boolean, Boolean> privateDNSStatus = Tools.getInstance().getPrivateDNSStatus();
        if (privateDNSStatus.first) {
            Toast.makeText(this, R.string.private_dns_success,
                    Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private void setClipboard(Context context, String text) {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
        }
    }
}
