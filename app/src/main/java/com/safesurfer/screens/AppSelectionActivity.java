package com.safesurfer.screens;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.safesurfer.R;
import com.frostnerd.general.Utils;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class AppSelectionActivity extends PINProtectedActivity implements SearchView.OnQueryTextListener{
    private long lastBackPress;
    private ArrayList<String> currentSelected;
    private RecyclerView appList;
    private AppListAdapter listAdapter;
    private boolean changed;
    private String infoTextWhitelist, infoTextBlacklist;
    private boolean whiteList, onlyInternet, showSystemApps = true;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_select);
        appList = findViewById(R.id.app_list);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        onlyInternet = getIntent() != null && getIntent().getBooleanExtra("onlyInternet",false);
        currentSelected = getIntent() != null && getIntent().hasExtra("apps") ? getIntent().getStringArrayListExtra("apps") : new ArrayList<String>();
        infoTextWhitelist = getIntent() != null && getIntent().hasExtra("infoTextWhitelist") ? getIntent().getStringExtra("infoTextWhitelist") : null;
        infoTextBlacklist = getIntent() != null && getIntent().hasExtra("infoTextBlacklist") ? getIntent().getStringExtra("infoTextBlacklist") : null;
        whiteList = getIntent() != null && getIntent().getBooleanExtra("whitelist", false);
        appList.setLayoutManager(new LinearLayoutManager(this));
        appList.setHasFixedSize(true);
        ((SimpleItemAnimator) appList.getItemAnimator()).setSupportsChangeAnimations(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                listAdapter = new AppListAdapter();
                if(appList != null)runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        appList.setAdapter(listAdapter);
                    }
                });
                else {
                    listAdapter.update = false;
                    listAdapter.apps.clear();
                    listAdapter.searchedApps.clear();
                    listAdapter = null;
                }
            }
        }).start();
        getSupportActionBar().setSubtitle(getString(R.string.x_apps_selected).replace("[[x]]", currentSelected.size() + ""));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_autopause_appselect, menu);
        ((SearchView) menu.findItem(R.id.action_search).getActionView()).setOnQueryTextListener(this);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() - lastBackPress <= 1500 || !changed) {
            setResult(RESULT_CANCELED);
            super.onBackPressed();
        } else {
            lastBackPress = System.currentTimeMillis();
            Toast.makeText(this, R.string.press_back_again, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_done){
            setResult(RESULT_OK, new Intent().putExtra("apps", currentSelected).putExtra("whitelist", whiteList));
            finish();
        }else if(item.getItemId() == android.R.id.home) {
            if (System.currentTimeMillis() - lastBackPress <= 1500 || !changed) {
                setResult(RESULT_CANCELED);
                finish();
            }else {
                lastBackPress = System.currentTimeMillis();
                Toast.makeText(this, R.string.press_back_again, Toast.LENGTH_LONG).show();
            }
        }else return super.onOptionsItemSelected(item);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(listAdapter != null && findViewById(R.id.progress).getVisibility() != View.VISIBLE)listAdapter.filter(newText);
        return true;
    }

    private final class AppListAdapter extends RecyclerView.Adapter<ViewHolder> {
        private final TreeSet<AppEntry> apps = new TreeSet<>();
        private final List<AppEntry> searchedApps = new ArrayList<>();
        private boolean update = true;
        private String currentSearch = "";

        AppListAdapter() {
            reload();
        }

        void reload(){
            runOnUiThread(() -> findViewById(R.id.progress).setVisibility(View.VISIBLE));
            apps.clear();
            List<ApplicationInfo> packages = getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
            AppEntry entry;
            for (ApplicationInfo packageInfo : packages) {
                entry = new AppEntry(AppSelectionActivity.this, packageInfo);
                if(!onlyInternet || entry.hasPermission(Manifest.permission.INTERNET)){
                    if(showSystemApps || !entry.isSystemApp())apps.add(entry);
                }
            }
            filter(currentSearch);
        }

        public void filter(String search){
            this.currentSearch = search;
            searchedApps.clear();
            if(search.equals("")){
                searchedApps.addAll(apps);
            }else{
                for(AppEntry entry: apps){
                    if(entry.getTitle().toLowerCase().contains(search.toLowerCase()))searchedApps.add(entry);
                }
            }
            runOnUiThread(() -> {
                notifyDataSetChanged();
                findViewById(R.id.progress).setVisibility(View.GONE);
            });
        }

        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(viewType == 0 ?
                    R.layout.row_appselect_info : R.layout.row_app_entry, parent, false);
            return new ViewHolder((RelativeLayout) view, viewType);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            if(!update)return;
            if (holder.type == 0){
                ((TextView)holder.itemView.findViewById(R.id.text)).setText(whiteList ? infoTextWhitelist : infoTextBlacklist);
            }else{
                int offSet = 1;
                AppEntry entry = searchedApps.get(position - offSet);
                CheckBox checkBox = holder.itemView.findViewById(R.id.app_selected_indicator);
                ((ImageView) holder.itemView.findViewById(R.id.app_image)).setImageDrawable(entry.getIcon());
                ((TextView) holder.itemView.findViewById(R.id.app_title)).setText(entry.getTitle());
                holder.itemView.setClickable(true);
                checkBox.setOnCheckedChangeListener(null);
                checkBox.setChecked(currentSelected.contains(entry.getPackageName()));
                holder.itemView.setOnClickListener(v -> ((CheckBox) v.findViewById(R.id.app_selected_indicator)).toggle());
                checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if(!update)return;
                    if (isChecked) currentSelected.add(holder.appEntry.get().getPackageName());
                    else currentSelected.remove(holder.appEntry.get().getPackageName());
                    listAdapter.notifyItemChanged(0);
                    Utils.requireNonNull(getSupportActionBar()).setSubtitle(getString(R.string.x_apps_selected).replace("[[x]]", currentSelected.size() + ""));
                    changed = true;
                });
                holder.appEntry = new SoftReference<>(entry);
            }
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? 0 : 1;
        }

        @Override
        public int getItemCount() {
            return searchedApps.size() + 1;
        }
    }

    private static final class ViewHolder extends RecyclerView.ViewHolder {
        private SoftReference<AppEntry> appEntry;
        private final int type;

        ViewHolder(RelativeLayout layout, int type) {
            super(layout);
            this.type = type;
        }

        @Override
        protected void finalize() throws Throwable {
            super.finalize();
            if (appEntry != null) {
                appEntry.clear();
            }
            appEntry = null;
        }
    }

    private static class AppEntry implements Comparable<AppEntry> {
        private final ApplicationInfo info;
        private SoftReference<Context> context;

        AppEntry(Context context, ApplicationInfo info) {
            this.info = info;
            this.context = new SoftReference<>(context);
        }

        public ApplicationInfo getRawInfo() {
            return info;
        }

        public String getTitle() {
            if(context.isEnqueued())
                throw new IllegalStateException("The Context supplied to the AppEntry doesn't exist anymore");
            return context.get().getPackageManager().getApplicationLabel(info).toString();
        }

        public String getPackageName(){
            return info.packageName;
        }

        public boolean isSystemApp() {
            return (info.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
        }

        private Drawable getIcon() {
            if(context.isEnqueued())
                throw new IllegalStateException("The Context supplied to the AppEntry doesn't exist anymore");
            return info.loadIcon(context.get().getPackageManager());
        }

        @Override
        public int compareTo(@NonNull AppEntry o) {
            return getTitle().compareTo(o.getTitle());
        }

        public boolean hasPermission(String s){
            try {
                PackageInfo info = context.get().getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_PERMISSIONS);
                String[] permissions = info.requestedPermissions;
                if(permissions == null)return false;
                for(int i = 0; i < permissions.length; i++){
                    if(info.requestedPermissions[i].equals(s) && isPermissionGranted(info, i))return true;
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            return false;
        }

        private boolean isPermissionGranted(PackageInfo info, int pos){
            return Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN || (info.requestedPermissionsFlags[pos] & PackageInfo.REQUESTED_PERMISSION_GRANTED) != 0;
        }
    }
}
