package com.safesurfer.screens.chat_guard;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.safesurfer.R;
import com.safesurfer.util.KeywordsManager;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.util.PreferencesAccessor;

public class KeywordsActivity extends PINProtectedActivity {

    private Toolbar toolbar;

    private LinearLayout categorySwear;
    private LinearLayout categoryDrugs;
    private LinearLayout categorySexting;
    private LinearLayout categorySelfHarm;
    private MaterialCardView customKeywords;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        setContentView(R.layout.activity_chat_guard_keywords);
        KeywordsManager.getInstance().init(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        categorySwear = findViewById(R.id.chat_guard_category_swear);
        categorySwear.findViewById(R.id.root).setOnClickListener(view -> {
            boolean newVal = !PreferencesAccessor.isChatGuardSwearWordsEnabled(this);
            PreferencesAccessor.setChatGuardSwearWordsEnabled(this, newVal);
            ((SwitchMaterial) categorySwear.findViewById(R.id.switch_material)).setChecked(newVal);
        });
        categoryDrugs = findViewById(R.id.chat_guard_category_drugs);
        categoryDrugs.findViewById(R.id.root).setOnClickListener(view -> {
            boolean newVal = !PreferencesAccessor.isChatGuardDrugsEnabled(this);
            PreferencesAccessor.setChatGuardDrugsEnabled(this, newVal);
            ((SwitchMaterial) categoryDrugs.findViewById(R.id.switch_material)).setChecked(newVal);
        });
        categorySexting = findViewById(R.id.chat_guard_category_sexting);
        categorySexting.findViewById(R.id.root).setOnClickListener(view -> {
            boolean newVal = !PreferencesAccessor.isChatGuardSextingEnabled(this);
            PreferencesAccessor.setChatGuardSextingEnabled(this, newVal);
            ((SwitchMaterial) categorySexting.findViewById(R.id.switch_material)).setChecked(newVal);
        });
        categorySelfHarm = findViewById(R.id.chat_guard_category_self_harm);
        categorySelfHarm.findViewById(R.id.root).setOnClickListener(view -> {
            boolean newVal = !PreferencesAccessor.isChatGuardSelfHarmEnabled(this);
            PreferencesAccessor.setChatGuardSelfHarmEnabled(this, newVal);
            ((SwitchMaterial) categorySelfHarm.findViewById(R.id.switch_material)).setChecked(newVal);
        });
        ((TextView) categorySwear.findViewById(R.id.name)).setText(R.string.chat_guard_category_swear);
        ((TextView) categorySwear.findViewById(R.id.bottom_text)).setText(R.string.chat_guard_category_description_swear);
        categorySwear.findViewById(R.id.switch_material).setOnClickListener(view -> {
            boolean newVal = !PreferencesAccessor.isChatGuardSwearWordsEnabled(this);
            PreferencesAccessor.setChatGuardSwearWordsEnabled(this, newVal);
        });
        ((SwitchMaterial) categorySwear.findViewById(R.id.switch_material)).setChecked(
                PreferencesAccessor.isChatGuardSwearWordsEnabled(this)
        );
        categorySwear.findViewById(R.id.bottom_text).setVisibility(View.VISIBLE);
        ((TextView) categoryDrugs.findViewById(R.id.name)).setText(R.string.chat_guard_category_drugs);
        ((TextView) categoryDrugs.findViewById(R.id.bottom_text)).setText(R.string.chat_guard_category_description_drugs);
        categoryDrugs.findViewById(R.id.switch_material).setOnClickListener(view -> {
            boolean newVal = !PreferencesAccessor.isChatGuardDrugsEnabled(this);
            PreferencesAccessor.setChatGuardDrugsEnabled(this, newVal);
        });
        ((SwitchMaterial) categoryDrugs.findViewById(R.id.switch_material)).setChecked(
                PreferencesAccessor.isChatGuardDrugsEnabled(this)
        );
        categoryDrugs.findViewById(R.id.bottom_text).setVisibility(View.VISIBLE);
        ((TextView) categorySexting.findViewById(R.id.name)).setText(R.string.chat_guard_category_sexting);
        ((TextView) categorySexting.findViewById(R.id.bottom_text)).setText(R.string.chat_guard_category_description_sexting);
        categorySexting.findViewById(R.id.switch_material).setOnClickListener(view -> {
            boolean newVal = !PreferencesAccessor.isChatGuardSextingEnabled(this);
            PreferencesAccessor.setChatGuardSextingEnabled(this, newVal);
        });
        ((SwitchMaterial) categorySexting.findViewById(R.id.switch_material)).setChecked(
                PreferencesAccessor.isChatGuardSextingEnabled(this)
        );
        categorySexting.findViewById(R.id.bottom_text).setVisibility(View.VISIBLE);
        ((TextView) categorySelfHarm.findViewById(R.id.name)).setText(R.string.chat_guard_category_self_harm);
        ((TextView) categorySelfHarm.findViewById(R.id.bottom_text)).setText(R.string.chat_guard_category_description_self_harm);
        categorySelfHarm.findViewById(R.id.switch_material).setOnClickListener(view -> {
            boolean newVal = !PreferencesAccessor.isChatGuardSelfHarmEnabled(this);
            PreferencesAccessor.setChatGuardSelfHarmEnabled(this, newVal);
        });
        ((SwitchMaterial) categorySelfHarm.findViewById(R.id.switch_material)).setChecked(
                PreferencesAccessor.isChatGuardSelfHarmEnabled(this)
        );
        categorySelfHarm.findViewById(R.id.bottom_text).setVisibility(View.VISIBLE);
        customKeywords = findViewById(R.id.chat_guard_custom_keywords);
        customKeywords.setOnClickListener(view -> {
            startActivity(new Intent(this, CustomKeywordsActivity.class)
                .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true));
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
