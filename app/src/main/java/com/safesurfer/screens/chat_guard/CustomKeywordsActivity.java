package com.safesurfer.screens.chat_guard;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.safesurfer.R;
import com.safesurfer.util.KeywordsManager;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.util.Tools;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import java9.util.stream.Collectors;
import java9.util.stream.StreamSupport;

public class CustomKeywordsActivity extends PINProtectedActivity {

    private RecyclerView recyclerView = null;
    private Adapter adapter;

    private AlertDialog dialogDelete;
    private AlertDialog dialogKeyword;

    private Set<Integer> selectedIndices = new HashSet<>();

    private Toolbar toolbar;

    private ActionMode actionMode;
    // Action mode for toolbar if selecting items
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_menu_delete, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            if (item.getItemId() == R.id.action_delete) {
                new AlertDialog.Builder(CustomKeywordsActivity.this)
                        .setMessage(R.string.delete_keyword_confirm)
                        .setNegativeButton(R.string.cancel, null)
                        .setPositiveButton(R.string.delete, (dialogInterface, j) -> {
                            List<Integer> toRemove = StreamSupport.stream(selectedIndices)
                                    .sorted()
                                    .collect(Collectors.toList());
                            for (int k = 0; k < toRemove.size(); k++) {
                                KeywordsManager.getInstance().removeKeyword(toRemove.get(k) - k);
                            }
                            adapter.notifyDataSetChanged();
                            stopSelection();
                        })
                        .show();
                return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            selectedIndices.clear();
            adapter.notifyDataSetChanged();
            setSupportABVisible(true);
        }
    };

    @SuppressLint("CheckResult")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        setContentView(R.layout.activity_chat_guard_custom_keywords);
        KeywordsManager.getInstance().init(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        adapter = new Adapter();
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.getItemAnimator().setChangeDuration(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat_guard_keywords, menu);
        return true;
    }

    /**
     * Set whether the support action bar is visible.
     */
    private void setSupportABVisible(boolean visible) {
        if (visible) {
            getSupportActionBar().show();
        } else {
            getSupportActionBar().hide();
        }
    }

    /**
     * Deselect all items and hide the action mode bar.
     */
    private void stopSelection() {
        selectedIndices.clear();
        if (actionMode != null) {
            actionMode.finish();
        }
        setSupportABVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        if (item.getItemId() == R.id.chat_guard_add) {
            showKeywordDialog(-1);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showKeywordDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.keyword));
        View inputView = getLayoutInflater().inflate(R.layout.dialog_enter_site, null);
        EditText edittext = inputView.findViewById(R.id.edittext);
        if (position != -1) {
            edittext.setText(KeywordsManager.getInstance().getCustomKeywords().get(position));
        }
        builder.setView(inputView);
        builder.setPositiveButton(getString(R.string.ok), (dialogInterface, i) -> {
            String w = edittext.getText().toString().trim();
            if (!Tools.isStringEmpty(w) && !KeywordsManager.getInstance().keyWordExists(w)) {
                if (position == -1) {
                    KeywordsManager.getInstance().addKeyWord(w);
                } else {
                    KeywordsManager.getInstance().changeKeyWord(position, w);
                }
                adapter.notifyDataSetChanged();
            }
            dialogKeyword.dismiss();
        });

        builder.setNegativeButton(getString(R.string.cancel), (dialogInterface, i) -> dialogKeyword.dismiss());

        dialogKeyword = builder.create();
        dialogKeyword.show();

        Tools.getInstance().showKeyboard(edittext);
        edittext.requestFocus();
    }

    /**
     * Show the context menu to delete items.
     */
    private void showContextMenu() {
        actionMode = startActionMode(mActionModeCallback);
        setSupportABVisible(false);
    }

    private class Adapter extends RecyclerView.Adapter<ViewHolder> {

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_checkable_text_list_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public int getItemCount() {
            return KeywordsManager.getInstance().getCustomKeywords().size();
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.tvWord.setText(KeywordsManager.getInstance().getCustomKeywords().get(position));
            holder.root.setOnClickListener(view -> {
                if (selectedIndices.contains(position)) {
                    selectedIndices.remove(position);
                    holder.root.setChecked(false);
                    if (selectedIndices.size() == 0) {
                        stopSelection();
                    }
                } else if (selectedIndices.size() > 0) {
                    selectedIndices.add(position);
                    holder.root.setChecked(true);
                } else {
                    showKeywordDialog(position);
                }
            });
            holder.root.setChecked(selectedIndices.contains(position));
            holder.root.setOnLongClickListener(view -> {
                holder.root.setChecked(true);
                selectedIndices.add(position);
                showContextMenu();
                return true;
            });
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private MaterialCardView root;
        private TextView tvWord;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvWord = itemView.findViewById(R.id.list_text);
            root = itemView.findViewById(R.id.root);
        }
    }
}
