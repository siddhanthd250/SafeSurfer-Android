package com.safesurfer.screens;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.safesurfer.R;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.network_api.entities.MetaResp;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.Tools;

public class RevokeAccessActivity extends PINProtectedActivity {
    // If true, revoke immediately rather than later and return to the main screen after.
    public static final String INTENT_EXTRA_REVOKE_IMMEDIATE = "com.safesurfer.revoke_immediate";

    private Button revokeButton;
    private Button cancelButton;
    private LinearLayout loadedLayout;
    private LinearLayout loadingLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (PreferencesAccessor.getAccountAccessRevoked(this)) {
            // Already revoked, nothing to do
            finish();
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        setContentView(R.layout.activity_revoke_access);
        this.revokeButton = findViewById(R.id.revoke_access_revoke);
        this.cancelButton = findViewById(R.id.revoke_access_cancel);
        this.loadedLayout = findViewById(R.id.loaded_layout);
        this.loadingLayout = findViewById(R.id.loading_layout);
        this.revokeButton.setOnClickListener(this::revoke);
        this.cancelButton.setOnClickListener(view -> {
            finish();
        });
        if (getIntent().getBooleanExtra(INTENT_EXTRA_REVOKE_IMMEDIATE, false)) {
            revoke(null);
            startActivity(new Intent(this, MainActivity.class).putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true));
            finish();
            return;
        }
    }

    @SuppressLint("CheckResult")
    private void revoke(View view) {
        this.loadedLayout.setVisibility(View.GONE);
        this.loadingLayout.setVisibility(View.VISIBLE);
        Runnable onFail = () -> {
            Toast.makeText(this, R.string.revoke_access_fail, Toast.LENGTH_LONG).show();
            this.loadedLayout.setVisibility(View.VISIBLE);
            this.loadingLayout.setVisibility(View.GONE);
        };
        try {
            SafeSurferNetworkApi.getInstance().getSRUseCase()
                    .getTokenWithRoles(Tools.getInstance().getDeviceAuthToken(), "android")
                    .subscribe(result -> {
                        if (result.code() == 200) {
                            Gson gson = new Gson();
                            MetaResp<String> tokenResp = gson.fromJson(result.body().string(), new TypeToken<MetaResp<String>>() {
                            }.getType());
                            Tools.getInstance().setDeviceAuthToken(tokenResp.spec);
                            PreferencesAccessor.setAccountAccessRevoked(this, true);
                            finish();
                        } else {
                            onFail.run();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            onFail.run();
        }
    }
}
