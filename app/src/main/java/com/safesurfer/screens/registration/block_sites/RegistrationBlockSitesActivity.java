package com.safesurfer.screens.registration.block_sites;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.safesurfer.R;
import com.safesurfer.fragments.blocked_sites.BlockedSitesFragment;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.screens.RevokeAccessActivity;
import com.safesurfer.screens.registration.setup_pin.RegistrationSetupPIN;

public class RegistrationBlockSitesActivity extends AppCompatActivity {
    public static final String INTENT_EXTRA_PIN_NEXT = "com.safesurfer.pin_next";

    private Toolbar toolbar;
    private FragmentManager fragmentManager;
    private BlockedSitesFragment blockedSitesFragment;
    private Button skipButton;
    private Button nextButton;
    private boolean saving = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_blocked_sites);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        fragmentManager = getSupportFragmentManager();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        this.blockedSitesFragment = new BlockedSitesFragment(getSupportActionBar(), menuItem -> {}, true);
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, this.blockedSitesFragment)
                .commit();
        this.skipButton = findViewById(R.id.button_skip);
        this.nextButton = findViewById(R.id.button_next);
        this.skipButton.setOnClickListener(view -> {
            this.nextActivity();
        });
        this.nextButton.setOnClickListener(view -> {
            if (this.saving) {
                return;
            }
            this.saving = true;
            this.blockedSitesFragment.save(this::nextActivity);
        });
    }

    /**
     * To the appropriate next activity.
     */
    protected void nextActivity() {
        if (getIntent().getBooleanExtra(INTENT_EXTRA_PIN_NEXT, true)) {
            startActivity(new Intent(this, RegistrationSetupPIN.class));
            finish();
        } else {
            Intent intent = new Intent(this, RevokeAccessActivity.class)
                    .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
            intent.putExtra(RevokeAccessActivity.INTENT_EXTRA_REVOKE_IMMEDIATE, true);
            startActivity(intent);
            finish();
        }
    }
}
