package com.safesurfer.screens.registration;

/**
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.Toolbar;

import com.google.gson.Gson;
import com.safesurfer.R;
import com.safesurfer.screens.MainActivity;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.screens.registration.revoke_access.RegistrationRevokeAccess;
import com.safesurfer.services.VirtualTunnelService;
import com.safesurfer.util.Constants;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.Tools;
import com.safesurfer.util.Util;

import javax.annotation.Nullable;

import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_DEVICEID;

public class RegisterActivity extends PINProtectedActivity {

    public static final String INTENT_EXTRA_FINISH_SETUP = "com.safesurfer.finishSetup";

    private LinearLayout mAgreeTerms;
    private WebView mWebview;
    private LinearLayout mRegistration;
    private LinearLayout mRegistrationLoadingLayout;
    private RelativeLayout mWebviewContainer;
    private WebView mRegistrationWebview;
    private Button mSkip;
    private Toolbar toolbar;
    /**
     * Whether we should load reauthentication instead of registration.
     */
    private boolean isReauth;

    private boolean bounded = false;
    ServiceConnection mSc;
    VirtualTunnelService mVPN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_register);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setTitle(R.string.registration_title);
        // Registration looks a bit sterile, chuck the Safe Surfer guy in there
        Drawable drawable = getResources().getDrawable(R.drawable.ic_launcher);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        int pixels = (int) (24 * getResources().getDisplayMetrics().density);
        Drawable newdrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, pixels, pixels, true));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(newdrawable);
        mAgreeTerms = findViewById(R.id.agree_terms);
        mRegistration = findViewById(R.id.registration);
        mRegistrationLoadingLayout = findViewById(R.id.loading_layout);
        mWebviewContainer = findViewById(R.id.reg_webview_container);
        mRegistrationWebview = findViewById(R.id.reg_webview);
        mWebview = findViewById(R.id.webview);
        mSkip = findViewById(R.id.reg_skip);
        mSkip.setOnClickListener(this::skip);
        isReauth = Tools.getInstance().getDeviceAuthToken() != null;
        if (!Tools.getInstance().getUserAgreeTerms()) {
            // User hasn't agreed to terms yet, show this instead
            mRegistration.setVisibility(View.GONE);
            mAgreeTerms.setVisibility(View.VISIBLE);
            mWebview.loadUrl("file:///android_asset/welcome_en.html");
            findViewById(R.id.button_get_started).setOnClickListener(view -> {
                Tools.getInstance().setUserAgreeTerms(true);
                mAgreeTerms.setVisibility(View.GONE);
                mRegistration.setVisibility(View.VISIBLE);
            });
            ((CheckBox) findViewById(R.id.checkbox_i_agree)).setOnCheckedChangeListener((compoundButton, isChecked) -> findViewById(R.id.button_get_started).setEnabled(isChecked));
            findViewById(R.id.button_get_started).setEnabled(false);
        }
        // Load the registration wv in the background
        mRegistrationWebview.getSettings().setJavaScriptEnabled(true);
        mRegistrationWebview.getSettings().setDomStorageEnabled(true);
        mRegistrationWebview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        mRegistrationWebview.getSettings().setAppCacheEnabled(true);
        mRegistrationWebview.getSettings().setDatabaseEnabled(true);
        mRegistrationWebview.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mWebviewContainer.setVisibility(View.VISIBLE);
                mRegistrationLoadingLayout.setVisibility(View.GONE);
            }
        });
        mRegistrationWebview.addJavascriptInterface(new WebAppInterface(this), "SSApp");
        mRegistrationWebview.loadUrl(this.getURL());
        // Load the VPN service
        mSc = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mVPN = ((VirtualTunnelService.VirtualTunnelServiceBinder) service).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
            }
        };
        if (Util.isServiceRunning()) {
            Intent service = new Intent(this, VirtualTunnelService.class);
            bindService(service, mSc, Context.BIND_AUTO_CREATE);
            bounded = true;
        }
    }

    /**
     * @return The URL for the webview.
     */
    private String getURL() {
        if (this.isReauth) {
            SharedPreferences preferences = getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
            String existingEmail = preferences.getString(Constants.REGISTRATION_PREFERENCES_KEY_EMAIL, "");
            String deviceId = preferences.getString(REGISTRATION_PREFERENCES_KEY_DEVICEID, "");
            return "https://my.safesurfer.io/device-reauth?titlebar=0&type=mobile&os=android&email=" + Uri.encode(existingEmail) +
                    "&device-id=" + Uri.encode(deviceId) +
                    "&name=" + Uri.encode(Build.MODEL);
        }
        return "https://my.safesurfer.io/device-registration?titlebar=0&os=android&type=mobile&name=" + Uri.encode(Build.MODEL);
    }

    /**
     * Complete the reauthentication process.
     * @param result The result of reauthentication.
     */
    private void completeReauth(WebviewResult result) {
        Tools.getInstance().setDeviceAuthToken(result.authToken);
        PreferencesAccessor.setAccountAccessRevoked(this, false);
        PreferencesAccessor.setPost73ReAuthDone(this, true);
        // If we re-registered, we have to change some stored options
        if (result.registered) {
            SharedPreferences preferences = getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
            preferences.edit().putString(REGISTRATION_PREFERENCES_KEY_DEVICEID, result.deviceID).commit();
            PreferencesAccessor.setDNSUUID(this, result.dnsToken);
        }
        // Restart VPN
        if (Util.isServiceRunning()) {
            startService(new Intent(this, VirtualTunnelService.class));
        }
        finish();
    }

    /**
     * Skip the registration process.
     */
    private void skip(@Nullable View v) {
        Intent intent = new Intent(this, MainActivity.class).putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
        startActivity(intent);
        finish();
    }

    /**
     * Complete the registration process.
     * @param result The device details and whether skipped.
     */
    private void completeRegistration(WebviewResult result) {
        if (result.skipped) {
            this.skip(null);
            return;
        }
        // Save the auth and other details
        PreferencesAccessor.setPost73ReAuthDone(this, true);
        Tools.getInstance().saveCurrentUserLogin(result.email);
        SharedPreferences preferences = getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putString(REGISTRATION_PREFERENCES_KEY_DEVICEID, result.deviceID).commit();
        preferences.edit().putString(Constants.REGISTRATION_PREFERENCES_KEY_EMAIL, result.email).commit();
        PreferencesAccessor.setDNSUUID(this, result.dnsToken);
        Tools.getInstance().setDeviceAuthToken(result.authToken);
        // Restart VPN
        if (bounded) {
            mVPN.restartVPN();
        }
        // If we are to finish setup, do that now
        if (getIntent().getBooleanExtra(INTENT_EXTRA_FINISH_SETUP, true)) {
            Intent intent = new Intent(this, RegistrationRevokeAccess.class);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

    private static class WebviewResult {
        public boolean skipped = false;
        public boolean registered = false;
        public String deviceID = "";
        public String dnsToken = "";
        public String authToken = "";
        public String email = "";
    }

    public class WebAppInterface {
        Context mContext;

        WebAppInterface(Context c) {
            this.mContext = c;
        }

        @JavascriptInterface
        public void result(String resultStr) {
            Gson gson = new Gson();
            WebviewResult result = gson.fromJson(resultStr, WebviewResult.class);
            if (RegisterActivity.this.isReauth) {
                RegisterActivity.this.completeReauth(result);
                return;
            }
            RegisterActivity.this.completeRegistration(result);
        }
    }
}
