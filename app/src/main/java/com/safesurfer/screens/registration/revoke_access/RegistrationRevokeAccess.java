package com.safesurfer.screens.registration.revoke_access;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.card.MaterialCardView;
import com.safesurfer.R;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.screens.registration.block_sites.RegistrationBlockSitesActivity;
import com.safesurfer.screens.subscription.SubscriptionActivity;

public class RegistrationRevokeAccess extends AppCompatActivity {

    private TextView adminDescription;
    private TextView protectedDescription;
    private MaterialCardView adminCard;
    private MaterialCardView protectedCard;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        setContentView(R.layout.activity_registration_revoke_access);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setTitle(R.string.registration_revoke_access_title);
        // Set descriptions
        this.adminDescription = findViewById(R.id.registration_revoke_access_admin_description);
        this.protectedDescription = findViewById(R.id.registration_revoke_access_protected_person_description);
        this.adminDescription.setText(Html.fromHtml(getResources().getString(R.string.registration_revoke_access_admin_description)));
        this.protectedDescription.setText(Html.fromHtml(getResources().getString(R.string.registration_revoke_access_protected_person_description)));
        this.adminCard = findViewById(R.id.role_admin);
        this.protectedCard = findViewById(R.id.role_protected_person);
        this.adminCard.setOnClickListener(view -> {
            this.goToSubscriptionActivity();
        });
        this.protectedCard.setOnClickListener(view -> {
            this.goToBlockedSitesActivity(true);
        });
    }

    private void goToSubscriptionActivity() {
        startActivity(new Intent(this, SubscriptionActivity.class)
                .putExtra(SubscriptionActivity.INTENT_EXTRA_SKIP_ON_ALREADY_HAS_PRO, true)
                .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true));
        finish();
    }

    private void goToBlockedSitesActivity(boolean pinNext) {
        startActivity(
                new Intent(
                        this,
                        RegistrationBlockSitesActivity.class
                ).putExtra(RegistrationBlockSitesActivity.INTENT_EXTRA_PIN_NEXT, pinNext));
        finish();
    }
}
