package com.safesurfer.screens.registration.setup_pin;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;

import com.safesurfer.R;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.screens.RevokeAccessActivity;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.Tools;

public class RegistrationSetupPIN extends AppCompatActivity {

    private AppCompatEditText editPIN;
    private Button skipButton;
    private Button confirmButton;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_setup_pin);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setTitle(R.string.registration_setup_pin_title);
        this.editPIN = findViewById(R.id.edit_pin);
        this.skipButton = findViewById(R.id.setup_pin_skip);
        this.confirmButton = findViewById(R.id.setup_pin_confirm);
        this.skipButton.setOnClickListener(view -> {
            this.nextActivity();
        });
        this.confirmButton.setOnClickListener(view -> {
            String newPIN = this.editPIN.getText().toString();
            if (newPIN.equals("")) {
                return;
            }
            PreferencesAccessor.setPin(this, newPIN);
            PreferencesAccessor.setPinEnabled(this, true);
            this.nextActivity();
        });
    }

    /**
     * Go to the next activity.
     */
    private void nextActivity() {
        Intent intent = new Intent(this, RevokeAccessActivity.class)
                .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
        intent.putExtra(RevokeAccessActivity.INTENT_EXTRA_REVOKE_IMMEDIATE, true);
        startActivity(intent);
        finish();
    }
}
