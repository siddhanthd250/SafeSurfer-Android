package com.safesurfer;

import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseViewController {

    public BaseViewController(Context context){
        setContext(context);
    }

    public Context getContext() {
        return context;
    }

    public AppCompatActivity getActivity() {
        return ((AppCompatActivity)context);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private Context context;

    public abstract void setUpView();

    public abstract View getRootView();

}
