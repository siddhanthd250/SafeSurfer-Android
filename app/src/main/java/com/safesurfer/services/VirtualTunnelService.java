package com.safesurfer.services;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.VpnService;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.util.Pair;

import com.safesurfer.network.L4Blocking;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.network.ForwarderPools;
import com.safesurfer.network.TunReadThread;
import com.safesurfer.network.TunWriteThread;
import com.safesurfer.util.Preferences;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.Tools;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Copyright (C) 2014  Yihang Song
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * Adapted from PrivacyGuard source 12/08/2021.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class VirtualTunnelService extends VpnService implements Runnable {
    private static final String TAG = "MyVpnService";
    private static boolean running = false;
    private static boolean started = false;

    //The virtual network interface, get and return packets to it
    private ParcelFileDescriptor mInterface;
    private TunWriteThread writeThread;
    private TunReadThread readThread;
    private Thread mThread;
    private Thread privateDNSCheckThread;
    //Pools
    private ForwarderPools forwarderPools;
    // Tracking
    private L4Blocking.DNSTracker dnsTracker;

    // Handler to restart and log if there's an uncaught exception in any of the threads
    private Thread.UncaughtExceptionHandler uncaughtExceptionHandler = (t, e) -> {
        e.printStackTrace();
        this.restartVPN();
    };

    private boolean logEvasionAttempt = false;

    public static boolean isRunning() {
        /** http://stackoverflow.com/questions/600207/how-to-check-if-a-service-is-running-on-android */
        return running;
    }

    public static boolean isStarted() {
        return started;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mThread = new Thread(this);
        mThread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
        mThread.start();
        return START_STICKY_COMPATIBILITY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new VirtualTunnelServiceBinder();
    }

    @Override
    public void onRevoke() {
        stop();
        super.onRevoke();
    }

    @Override
    public void onDestroy() {
        stop();
        super.onDestroy();
    }

    @Override
    public void run() {
        // If private DNS is enabled, don't start. This will mess up internet connection entirely.
        Pair<Boolean, Boolean> privateDNSStatus = Tools.getInstance().getPrivateDNSStatus();
        if (privateDNSStatus.first) {
            stopVPN();
            return;
        }
        boolean lightweightMode = PreferencesAccessor.getLightweightMode(getApplicationContext());

        if (!(setup_network(lightweightMode))) {
            return;
        }

        started = false;
        running = true;

        setup_workers(lightweightMode);
        wait_to_close();
    }

    /**
     * Do the normal cleanup process, then start back up again.
     */
    public void restartVPN() {
        this.stop();
        mThread = new Thread(this);
        mThread.start();
    }

    private boolean setup_network(boolean lightweightMode) {
        Builder b = new Builder();
        b.addAddress("10.8.0.1", 32);
        String dns1 = PreferencesAccessor.getDNS1(getApplicationContext());
        String dns2 = PreferencesAccessor.getDNS1(getApplicationContext());
        b.addDnsServer(dns1);
        b.addDnsServer(dns2);
        b.setSession("Safe Surfer");
        if (lightweightMode) {
            // Lightweight mode, just route DNS
            b.addRoute(dns1, 32);
            b.addRoute(dns2, 32);
        } else {
            // Super safe mode, route all traffic
            b.addRoute("0.0.0.0", 0);
        }
        b.setMtu(1500);
        b.setBlocking(true);
        // Set which apps are excluded/included
        Preferences preferences = Preferences.getInstance(this);
        Set<String> excludedApps = preferences.getStringSet( "excluded_apps");
        boolean excludedWhitelisted = preferences.getBoolean("excluded_whitelist", false);
        try {
            if (excludedWhitelisted) {
                for (String s : excludedApps) {
                    if (s.equals("com.android.vending")) continue;
                    b = b.addAllowedApplication(s);
                }
            } else {
                b = b.addDisallowedApplication("com.android.vending");
                for (String s : excludedApps) {
                    if (s.equals("com.android.vending")) continue;
                    b = b.addDisallowedApplication(s);
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        mInterface = b.establish();
        if (mInterface == null) {
            return false;
        }
        HashSet<String> dnsServers = new HashSet<String>();
        dnsServers.add(dns1);
        dnsServers.add(dns2);
        forwarderPools = new ForwarderPools(this, dnsServers, PreferencesAccessor.getDNSUUID(this), dnsTracker);
        return true;
    }

    private void setup_workers(boolean lightweightMode) {
        // Only need DNS tracker for super-safe mode
        if (!lightweightMode) {
            dnsTracker = new L4Blocking.DNSTracker(this);
        }
        readThread = new TunReadThread(mInterface.getFileDescriptor(), this);
        readThread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
        readThread.start();
        writeThread = new TunWriteThread(mInterface.getFileDescriptor(), this);
        writeThread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
        writeThread.start();
        this.runPrivateDNSChecker();
    }

    /**
     * Periodically check if private DNS is enabled so we can stop if it is.
     */
    private void runPrivateDNSChecker() {
        privateDNSCheckThread = new Thread(() -> {
            while (true) {
                // If private DNS is enabled, stop. This will mess up internet connection entirely.
                Pair<Boolean, Boolean> privateDNSStatus = Tools.getInstance().getPrivateDNSStatus();
                if (privateDNSStatus.first) {
                    this.onPrivateDNSDetected(true);
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        privateDNSCheckThread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
        privateDNSCheckThread.start();
    }

    private void wait_to_close() {
        // wait until all threads stop
        try {
            while (writeThread.isAlive())
                writeThread.join();

            while (readThread.isAlive())
                readThread.join();

            while (privateDNSCheckThread.isAlive())
                privateDNSCheckThread.join();

            while (mThread.isAlive()) {
                mThread.join();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void fetchResponse(byte[] response) {
        writeThread.write(response);
    }

    public ForwarderPools getForwarderPools() {
        return forwarderPools;
    }

    private void stop() {
        running = false;
        if (mInterface == null) return;
        try {
            mInterface.close();
            readThread.interrupt();
            writeThread.interrupt();
            privateDNSCheckThread.interrupt();
            mThread.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String jwtToken = Tools.getInstance().getDeviceAuthToken();
        if (this.logEvasionAttempt && jwtToken != null) {
            // Since enabling private DNS disables our service, count as an evasion attempt
            if (jwtToken != null) {
                SafeSurferNetworkApi.getInstance().getSRUseCase()
                        .postEvasionAttempt(jwtToken, "PRIVATE_DNS", System.currentTimeMillis() / 1000L, "");
            }
        }
        mInterface = null;
    }

    /**
     * When private DNS is detected, stop the thread after a delay.
     * @param logEvasionAttempt Whether to log an evasion attempt.
     */
    private void onPrivateDNSDetected(boolean logEvasionAttempt) {
        this.logEvasionAttempt = logEvasionAttempt;
        this.stopVPN();
    }

    public void stopVPN() {
        stop();
        stopSelf();
    }

    public class VirtualTunnelServiceBinder extends Binder {
        public VirtualTunnelService getService() {
            // Return this instance of MyVpnService so clients can call public methods
            return VirtualTunnelService.this;
        }

        @Override
        protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) {
            if (code == IBinder.LAST_CALL_TRANSACTION) {
                onRevoke();
                return true;
            }
            return false;
        }
    }
}
