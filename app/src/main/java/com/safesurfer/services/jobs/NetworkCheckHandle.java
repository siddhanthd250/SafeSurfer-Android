package com.safesurfer.services.jobs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;

import com.safesurfer.LogFactory;
import com.safesurfer.services.VirtualTunnelService;
import com.safesurfer.util.Util;
import com.safesurfer.util.Preferences;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 * <p>
 * This file is part of SafeSurfer-Android.
 * <p>
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class NetworkCheckHandle {
    private BroadcastReceiver connectivityChange;
    private ConnectivityManager.NetworkCallback networkCallback;
    private ConnectivityManager connectivityManager;
    private Context context;
    private final String LOG_TAG;
    private boolean running = true, wasAnotherVPNRunning = false;

    public NetworkCheckHandle(Context context, String logTag) {
        if (context == null)
            throw new IllegalStateException("Context passed to NetworkCheckHandle is null.");
        this.context = context;
        LOG_TAG = logTag;
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                NetworkRequest.Builder builder = new NetworkRequest.Builder();
                connectivityManager.registerNetworkCallback(builder.build(), networkCallback = new ConnectivityManager.NetworkCallback() {
                    @Override
                    public void onAvailable(Network network) {
                        handleChange();
                    }

                    @Override
                    public void onLost(Network network) {
                        handleChange();
                    }

                    private void handleChange() {
                        if (running) {
                            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
                            try {
                                handleConnectivityChange(activeNetwork);
                            } catch (ReallyWeiredExceptionOnlyAFewPeopleHave ignored) {
                            }//Look below.
                        }
                    }
                });
            } catch (SecurityException ignored) {
                // I think this is happening when always-on mode is enabled. In this case, we don't
                // need this job.
            }
        } else {
            context.registerReceiver(connectivityChange = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    try {
                        handleConnectivityChange(!intent.hasExtra("noConnectivity"), intent.getIntExtra("networkType", -1));
                    } catch (ReallyWeiredExceptionOnlyAFewPeopleHave ignored) {
                    }//Look below.
                }
            }, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        try {
            handleInitialState();
        } catch (ReallyWeiredExceptionOnlyAFewPeopleHave ignored) {
        }//Look below
    }

    private void handleInitialState() throws ReallyWeiredExceptionOnlyAFewPeopleHave {
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork == null) {
            LogFactory.writeMessage(accessContext(), LOG_TAG, "No active network.");
        } else {
            LogFactory.writeMessage(accessContext(), LOG_TAG, "[OnCreate] Thread running: " + Util.isServiceThreadRunning());
            if (!Util.isServiceThreadRunning()) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI && Preferences.getInstance(accessContext()).getBoolean("setting_auto_wifi", false)) {
                    LogFactory.writeMessage(accessContext(), LOG_TAG, "[OnCreate] Connected to WIFI and setting_auto_wifi is true. Starting Service..");
                    startService();
                } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE && Preferences.getInstance(accessContext()).getBoolean("setting_auto_mobile", false)) {
                    LogFactory.writeMessage(accessContext(), LOG_TAG, "[OnCreate] Connected to MOBILE and setting_auto_mobile is true. Starting Service..");
                    startService();
                }
            }
        }
    }

    public void stop() {
        if (!running) return;
        running = false;
        try {
            if (networkCallback != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                connectivityManager.unregisterNetworkCallback(networkCallback);
            else if (connectivityChange != null) context.unregisterReceiver(connectivityChange);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            // TODO: This method may be called before networkCallback is registered
        }
        connectivityManager = null;
        networkCallback = null;
        context = null;
    }

    private void startService() {
        if (!running) return;
        if (VirtualTunnelService.isRunning()) {
            return;
        }
        try {
            context.startService(new Intent(context, VirtualTunnelService.class));
        } catch (IllegalStateException e) {
            return; // On newer android versions, this will happen because we aren't in the foreground. That's what the always-on option is for anyway.
        }
    }

    private void handleConnectivityChange(NetworkInfo networkInfo) throws ReallyWeiredExceptionOnlyAFewPeopleHave {
        if (networkInfo != null)
            handleConnectivityChange(networkInfo.isConnected(), networkInfo.getType());
        else handleConnectivityChange(false, ConnectionType.OTHER);
    }

    private void handleConnectivityChange(boolean connected, int type) throws ReallyWeiredExceptionOnlyAFewPeopleHave {
        ConnectionType cType;
        if (type == ConnectivityManager.TYPE_WIFI) cType = ConnectionType.WIFI;
        else if (type == ConnectivityManager.TYPE_MOBILE || type == ConnectivityManager.TYPE_MOBILE_DUN)
            cType = ConnectionType.MOBILE;
        else if (type == ConnectivityManager.TYPE_VPN) cType = ConnectionType.VPN;
        else cType = ConnectionType.OTHER;
        handleConnectivityChange(connected, cType);
    }

    /**
     * Controls behaviour of when a network change event has occured.
     *
     * @param connected
     * @param connectionType
     * @throws ReallyWeiredExceptionOnlyAFewPeopleHave
     */
    private void handleConnectivityChange(boolean connected, ConnectionType connectionType) throws ReallyWeiredExceptionOnlyAFewPeopleHave {
        if (!running ) return;
        boolean serviceRunning = Util.isServiceRunning(),
                serviceThreadRunning = Util.isServiceThreadRunning(),
                autoWifi = Preferences.getInstance(accessContext()).getBoolean("setting_auto_wifi", false),
                autoMobile = Preferences.getInstance(accessContext()).getBoolean("setting_auto_mobile", false),
                disableNetChange = Preferences.getInstance(accessContext()).getBoolean("setting_disable_netchange", false);
        LogFactory.writeMessage(accessContext(), LOG_TAG, "Connectivity changed. Connected: " + connected + ", type: " + connectionType);
        LogFactory.writeMessage(accessContext(), LOG_TAG, "Service running: " + serviceRunning + "; Thread running: " + serviceThreadRunning);
        Intent i;
        if (connectionType == ConnectionType.VPN) {
            if (!Util.isServiceThreadRunning() && connected)
                wasAnotherVPNRunning = true;
            return;
        } else {
            if (connected && wasAnotherVPNRunning && Preferences.getInstance(accessContext()).getBoolean("start_service_when_available", false)) {
                wasAnotherVPNRunning = false;
                Preferences.getInstance(accessContext()).put("start_service_when_available", false);
                context.startService(new Intent(context, VirtualTunnelService.class));
            }
        }
        if (!connected && disableNetChange && serviceRunning) {
            return;
        }
        if (!connected || (connectionType != ConnectionType.WIFI && connectionType != ConnectionType.MOBILE)) {
            return;
        }
        if (!serviceThreadRunning) {
            if (connectionType == ConnectionType.WIFI && autoWifi) {
                LogFactory.writeMessage(accessContext(), LOG_TAG, "Connected to WIFI and setting_auto_wifi is true. Starting Service..");
                startService();
            } else if (connectionType == ConnectionType.MOBILE && autoMobile) {
                LogFactory.writeMessage(accessContext(), LOG_TAG, "Connected to MOBILE and setting_auto_mobile is true. Starting Service..");
                startService();
            }
        }
    }

    private enum ConnectionType {
        MOBILE, WIFI, VPN, OTHER
    }

    /*
      Here's the catch for this messy code: I keep getting crash reports (NPE) indicating that the context is null.
      However this shouldn't be possible because the variable running is set to false and every receiver is unregistered.
      So this method is used for accessing the context, the exception is thrown all up to the highest layer where it is gracefully ignored.
     */
    private Context accessContext() throws ReallyWeiredExceptionOnlyAFewPeopleHave {
        if (context == null) throw new ReallyWeiredExceptionOnlyAFewPeopleHave();
        return context;
    }

    private class ReallyWeiredExceptionOnlyAFewPeopleHave extends Exception {
        ReallyWeiredExceptionOnlyAFewPeopleHave() {
            super("It's strange, isn't it?");
        }
    }
}
