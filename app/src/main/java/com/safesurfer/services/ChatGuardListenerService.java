package com.safesurfer.services;

import android.app.Notification;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.safesurfer.util.KeywordsManager;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.network_api.entities.AddNotificationRequestBody;
import com.safesurfer.util.Tools;

import java.util.ArrayList;

import java9.util.Lists;
import java9.util.Sets;

public class ChatGuardListenerService  extends NotificationListenerService {

    private String mPreviousNotificationKey = null;


    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {

        Log.d("onNotificationPosted", "StatusBarNotification sbn, RankingMap rankingMap");

        if ((sbn.getNotification().flags & Notification.FLAG_GROUP_SUMMARY) != 0) {
            //Ignore the notification
            return;
        }


        //Log.d("onNotificationPosted", "StatusBarNotification sbn");
        dumpNotification(sbn);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn, RankingMap rankingMap) {

        Log.d("onNotificationPosted", "StatusBarNotification sbn, RankingMap rankingMap");

        if ((sbn.getNotification().flags & Notification.FLAG_GROUP_SUMMARY) != 0) {
            //Ignore the notification
            return;
        }
        //Log.d("onNotificationPosted", "StatusBarNotification sbn, RankingMap rankingMap");
        dumpNotification(sbn);
    }


    private String getNotificationKey(StatusBarNotification sbn) {
        return sbn.getKey() + "|" + String.valueOf(sbn.getPostTime());
    }


    public void dumpNotification(StatusBarNotification sbn) {

        if (Tools.isStringEmpty(mPreviousNotificationKey) == false && getNotificationKey(sbn).equals(mPreviousNotificationKey)) {
            //Log.d("onNotificationPosted", "IGNORE" + getNotificationKey(sbn));
            return;
        }

        //Log.d("onNotificationPosted", "PROCESSED " + getNotificationKey(sbn));

        mPreviousNotificationKey = getNotificationKey(sbn);

        String pack = "";
        String title = "";
        String text = "";
        String bigText = "";
        Bundle extras;


        pack = sbn.getPackageName();
        extras = sbn.getNotification().extras;


        if (extras != null) {
            if (extras.getCharSequence("android.title") != null) {
                title = extras.getCharSequence("android.title").toString();
            }

            if (extras.getCharSequence("android.text") != null) {
                text = extras.getCharSequence("android.text").toString();
            }

            if (extras.getCharSequence("android.bigText") != null) {
                bigText = extras.getCharSequence("android.bigText").toString();
            }

            //Log.d("onNotificationPosted", "extras: " + extras.toString());

        }

        if (KeywordsManager.getInstance().isCacheEmpty()) {
            KeywordsManager.getInstance().init(getApplicationContext());
        }

        String content = "";

        if (Tools.isStringEmpty(bigText) == false && bigText.contains(text)) {
            content += bigText;
        } else {
            content += text;
            if (Tools.isStringEmpty(bigText) == false) {
                content += "\n" + bigText;
            }
        }



        ArrayList<String> keywords = KeywordsManager.getInstance().contains(content);
        if (keywords != null) {
            final String appId = pack;
            final String eventTitle = title;
            final String eventContent = content;
            String authDeviceToken = Tools.getInstance().getDeviceAuthToken();
            final AddNotificationRequestBody body = new AddNotificationRequestBody();
            body.appID = appId;
            body.content = eventContent;
            body.title = eventTitle;
            body.timestamp = System.currentTimeMillis() / 1000;
            body.keywords = Lists.copyOf(Sets.copyOf(keywords)); // Remove duplicates
            if (authDeviceToken != null) {
                SafeSurferNetworkApi.getInstance().getSRUseCase()
                        .addNotification(authDeviceToken, body).toObservable().subscribe(success -> {
                });
            }
        }

    }
}


