package com.safesurfer.tensorflow;


import com.safesurfer.R;
import com.safesurfer.util.Tools;

public class EventModel {

    private long timestamp;
    private int eventModeltype;

    public static final int EventModelTypesNEUTRAL = 111;
    public static final int EventModelTypesEXPLICIT = 222;
    public static final int EventModelTypesSUGGESTIVE = 333;

    public String getEventModelTypeName() {
        switch (eventModeltype) {
            case EventModelTypesNEUTRAL:
                return Tools.getInstance().getContext().getString(R.string.EventModelTypesNEUTRAL_title);
            case EventModelTypesEXPLICIT:
                return Tools.getInstance().getContext().getString(R.string.EventModelTypesEXPLICIT_title);
            case EventModelTypesSUGGESTIVE:
                return Tools.getInstance().getContext().getString(R.string.EventModelTypesSUGGESTIVE_title);
        }
        return "";
    }

    public void setEventModeltype(int eventModeltype) {
        this.eventModeltype = eventModeltype;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}



