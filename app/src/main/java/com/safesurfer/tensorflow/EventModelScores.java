package com.safesurfer.tensorflow;

public class EventModelScores {

    private float neutral = 0;
    private float explicit = 0;
    private float suggestive = 0;

    public float getNeutral() {
        return neutral;
    }

    public void setNeutral(float neutral) {
        this.neutral = neutral;
    }

    public float getExplicit() {
        return explicit;
    }

    public void setExplicit(float porn) {
        this.explicit = porn;
    }

    public float getSuggestive() {
        return suggestive;
    }

    public void setSuggestive(float suggestive) {
        this.suggestive = suggestive;
    }
}