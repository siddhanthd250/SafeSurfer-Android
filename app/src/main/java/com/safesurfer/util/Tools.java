package com.safesurfer.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;


import com.safesurfer.R;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerActivity;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_DEVICEID;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_SCREENCAST_ID;

public class Tools {

    private Context context;

    private Tools(){}
    private static Tools mInstance;

    public static final Tools getInstance(){
        if(mInstance == null)mInstance = new Tools();
        return mInstance;
    }

    public void init(Context context){
        setContext(context);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static boolean isStringEmpty(String s) {
        return (s == null || s.isEmpty());
    }


    public boolean isNotificationServiceEnabled(){
        String pkgName = getContext().getPackageName();
        final String flat = Settings.Secure.getString(getContext().getContentResolver(),
                "enabled_notification_listeners");
        if (!TextUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (int i = 0; i < names.length; i++) {
                final ComponentName cn = ComponentName.unflattenFromString(names[i]);
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.getPackageName()) &&
                            TextUtils.equals(cn.getClassName(), "com.safesurfer.services.ChatGuardListenerService")) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static final String ScreencastDetectionACTIVE_KEY = "ScreencastDetectionACTIVE_KEY";

    public boolean isScreencastDetectionActive(){
        return ScreenCapturerService.isServiceRunning();
    }

    public boolean isScreencastDetectionNotificationsActive(){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        return sharedPref.getBoolean("content_detector_notifications", false);
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void setScreencastDetectionActive(boolean active){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(ScreencastDetectionACTIVE_KEY, active);
        editor.commit();
    }

    public static final int REQUEST_OVERLAY_PERMISSION = 100;

    /**
     * @return Whether we need permission to draw overlays in the settings.
     */
    public boolean needScreencastPermission() {
        return getSdkVersion() >= 23 && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(getContext());
    }

    @SuppressLint("CheckResult")
    public void startScreencast(Activity activity, Runnable onChange) {
        try {
            if (needScreencastPermission()) {
                // ask for setting
                Toast.makeText(getContext(), R.string.enable_overlay_toast,
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getContext().getPackageName()));
                activity.startActivityForResult(intent, REQUEST_OVERLAY_PERMISSION);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Tools.getInstance().saveScreencastMode(Tools.SCREENCAST_MODE_WATCHING);
        Intent intent = new Intent(getContext(), ScreenCapturerActivity.class);
        activity.startActivity(intent);
        onChange.run();

        String jwtToken = Tools.getInstance().getDeviceAuthToken();
        if (jwtToken != null) {
            SafeSurferNetworkApi.getInstance()
                    .getSRUseCase()
                    .screencastStart(jwtToken)
                    .toObservable()
                    .subscribe(
                            success -> {
                                int responseCode = success.code();
                                switch (responseCode){
                                    case 201:
                                    case 200:
                                        String screencastId = success.body();
                                        Tools.getInstance().setCurrentScreencastId(screencastId);
                                        break;
                                    case 403:
                                        refreshJWTToken(activity, onChange);
                                        break;
                                }
                            },
                            Throwable::printStackTrace);
        }
    }

    @SuppressLint("CheckResult")
    private void refreshJWTToken(Activity activity, Runnable onDone){
        SubscriptionUtils.JWTClaims jwtClaims = SubscriptionUtils.parseDeviceJWTClaims();
        String jwtToken = Tools.getInstance().getDeviceAuthToken();
        if(jwtToken != null){
            SafeSurferNetworkApi.getInstance()
                    .getSRUseCase()
                    .refreshJWTToken(jwtToken)
                    .toObservable()
                    .subscribe(
                            success -> {
                                if (success.code() == 200) {
                                    String newJWTToken = Tools.removeQuotes(success.body().string());
                                    Tools.getInstance().setDeviceAuthToken(newJWTToken);
                                    SubscriptionUtils.JWTClaims claimsNew = SubscriptionUtils.parseDeviceJWTClaims();
                                    if (!claimsNew.getSubStatus().equals(jwtClaims.getSubStatus())) {
                                        startScreencast(activity, onDone);
                                    }
                                }
                            },
                            Throwable::printStackTrace);
        }
    }

    public static final String SCREENCAST_MODE = "SCREENCAST_MODE";
    public static final int SCREENCAST_MODE_WATCHING = 222;

    public void saveScreencastMode(int mode){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt(SCREENCAST_MODE, mode);

        editor.apply();
    }

    public static final int SCREENCAST_SERVICE_ID = 111333;

    public void cancelSheduleJob(){

        Log.d(getClass().getName(), "private void cancelSheduleJob(){}");

        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(SCREENCAST_SERVICE_ID);

    }

    public static int getSdkVersion(){
        return android.os.Build.VERSION.SDK_INT;
    }

    public void viewFadeInOut(final View view ){
        Log.d(getClass().getName(), "viewFadeInOut");

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        int delayTimeSeconds = sharedPref.getInt("screencast_blackout_timeout", 5) - 2;

        AnimatorSet mAnimationSet = new AnimatorSet();

        float fadeValue = 0.8f;
        if(sharedPref.getBoolean("screencast_complete_blackout", false)){
            fadeValue = 1.0f;
        }else{
            fadeValue = 0.8f;
        }

        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(view, View.ALPHA, 0f, fadeValue);
        fadeIn.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {}

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        fadeIn.setInterpolator(new LinearInterpolator());
        fadeIn.setDuration(1000);

        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(view, View.ALPHA,  fadeValue, 0f);
        fadeOut.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        fadeOut.setInterpolator(new LinearInterpolator());
        fadeOut.setStartDelay(delayTimeSeconds * 1000);
        fadeOut.setDuration(1000);

        mAnimationSet.playSequentially( fadeIn, fadeOut);
        mAnimationSet.start();
    }

    public static final String USE_BLACKOUT = "USE_BLACKOUT";

    public void setUseBlackout(boolean use){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean(USE_BLACKOUT, use);
        editor.commit();
    }

    public boolean getUseBlackout(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        return prefs.getBoolean(USE_BLACKOUT, true);
    }

    public static final String USE_WHOLE_SCREEN = "SCREENCAST_WHOLESCREEN";

    public void setUseWholeScreen(boolean use){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean(USE_WHOLE_SCREEN, use);
        editor.commit();
    }

    public boolean getUseWholeScreen(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        return prefs.getBoolean(USE_WHOLE_SCREEN, true);
    }

    /**
     * @return The minimum confidence level to alert on a screencast.
     */
    public String getScreencastSensitivity() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        return prefs.getString("sensitivity", "2");
    }

    /**
     * @return The amount of time to wait in between screenshots.
     */
    public int getScreencastScreenshotInterval() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        return prefs.getInt("screencast_interval", 1);
    }

    public static final String SCREENCAST_DEBUG_MODE = "SCREENCAST_DEBUG_MODE";

    public void setScreencastDebugMode(boolean debug){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean(SCREENCAST_DEBUG_MODE, debug);
        editor.commit();
    }

    public boolean getScreencastDebugMode(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        return prefs.getBoolean(SCREENCAST_DEBUG_MODE, false);
    }

    public String getNotificationChannelId(){

        String CHANNEL_ID = getClass().getName();

        return CHANNEL_ID;
    }

    public void createNotificationChannel( NotificationManager notificationManager){
        NotificationChannel channel;
        String CHANNEL_ID = getNotificationChannelId();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
             channel = new NotificationChannel( CHANNEL_ID, "Safe surfer", NotificationManager.IMPORTANCE_LOW);
            channel.setDescription("Safe surfer");
            channel.enableLights(false);
            channel.setVibrationPattern(new long[]{ 0 });
            channel.setSound(null, null);
            channel.enableVibration(true);

            notificationManager.createNotificationChannel(channel);
        }
    }

    public static final int NOTIFICATION_ID = 111555;
    public static final int FOREGROUND_NOTIFICATION_ID = 111333;

    public void setDeviceAuthToken(String token){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, token).commit();
    }

    public String getDeviceAuthToken(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        if(preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, null) == null)return null;
        return "Bearer "+preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, null);
    }

    public String getClearedDeviceAuthToken(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        if(preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, null) == null)return null;
        return preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, null);
    }

    public void setCurrentScreencastId(String id){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putString(REGISTRATION_PREFERENCES_KEY_SCREENCAST_ID, id).commit();
    }

    public String getCurrentScreencastId(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        return preferences.getString(REGISTRATION_PREFERENCES_KEY_SCREENCAST_ID, null);
    }

    public void showKeyboard(EditText editableView){
        InputMethodManager keyboard = (InputMethodManager)editableView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.showSoftInput(editableView, 0);
    }

    public static final String USER_AGREE_TERMS = "USER_AGREE_TERMS";

    public void setUserAgreeTerms(boolean value){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putBoolean(USER_AGREE_TERMS, value).commit();
    }

    public boolean getUserAgreeTerms(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        return preferences.getBoolean(USER_AGREE_TERMS, false);
    }

    public static final String CURRENT_USER_LOGIN= "CURRENT_USER_LOGIN";

    public void saveCurrentUserLogin(String userLogin){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putString(CURRENT_USER_LOGIN, userLogin).commit();
    }
    public String getCurrentUserLogin(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        return preferences.getString(CURRENT_USER_LOGIN, "");
    }

    public String getDeviceId(){
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);

        return preferences.getString(REGISTRATION_PREFERENCES_KEY_DEVICEID, "");
    }

    /**
     * @return Returns [whether private DNS is enabled, whether we are connected to Safe Surfer private DNS].
     */
    public Pair<Boolean, Boolean> getPrivateDNSStatus() {
        boolean privateDNSRunning = false;
        boolean privateDNSIsSafeSurfer = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // Private DNS supported
            ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            Network activeNetwork = connectivityManager.getActiveNetwork();
            if (activeNetwork != null) {
                LinkProperties linkProperties = connectivityManager.getLinkProperties(activeNetwork);
                if (linkProperties != null) {
                    privateDNSRunning = linkProperties.isPrivateDnsActive();
                    if (privateDNSRunning) {
                        String dnsServer = linkProperties.getPrivateDnsServerName();
                        if (dnsServer == null) {
                            privateDNSRunning = false; // Opportunistic mode: our VPN will override so that private DNS won't be enabled
                        } else {
                            // Check if this is our private DNS link
                            String dnsToken = PreferencesAccessor.getDNSUUID(getContext());
                            if (dnsToken == null) {
                                dnsToken = "";
                            } else {
                                dnsToken += ".";
                            }
                            privateDNSIsSafeSurfer = dnsServer.endsWith(dnsToken + "dns.safesurfer.io");
                        }
                    }
                }
            }
        }
        return new Pair<>(privateDNSRunning, privateDNSIsSafeSurfer);
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < macBytes.length; i++) {
                    sb.append(String.format("%02X%s", macBytes[i], (i < macBytes.length - 1) ? ":" : ""));
                }
                return sb.toString().toLowerCase();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return getMacAddress();
    }

    public static String loadFileAsString(String filePath) throws java.io.IOException {
        StringBuffer data = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            data.append(readData);
        }
        reader.close();
        return data.toString();
    }

    public static String getMacAddress() {
        try {
            return loadFileAsString("/sys/class/net/eth0/address").toUpperCase().substring(0, 17);
        } catch (Exception e) {
            e.printStackTrace();
            return "02:00:00:00:00:00";
        }
    }

    public static String removeQuotes(String input){
        return input.replaceAll("^\"|\"$", "");
    }
}
