package com.safesurfer.util;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */

public class Constants {
    public static String REGISTRATION_PREFERENCES = "com.safesurfer.registration";
    public static String REGISTRATION_PREFERENCES_KEY_EMAIL = "com.safesurfer.registration.email";
    public static String REGISTRATION_PREFERENCES_KEY_ISFIRSTTIME = "com.safesurfer.registration.isfirsttime";
    public static String REGISTRATION_PREFERENCES_KEY_DEVICEID = "com.safesurfer.registration.device_id";
    public static String REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN = "com.safesurfer.registration.auth_device_token";
    public static String REGISTRATION_PREFERENCES_KEY_SCREENCAST_ID = "com.safesurfer.registration.screencast_id";
}
