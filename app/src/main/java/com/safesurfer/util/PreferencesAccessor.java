package com.safesurfer.util;

import android.content.Context;
import androidx.annotation.NonNull;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class PreferencesAccessor {

    public static boolean isBatteryOptimizationDismissed(@NonNull Context context) {
        return Preferences.getInstance(context).getBoolean("battery_optimization_dismissed", false);
    }

    public static void setBatteryOptimizationDismissed(@NonNull Context context, boolean dismissed) {
        Preferences.getInstance(context).putBoolean("battery_optimization_dismissed", dismissed);
    }

    public static boolean isDrawOnTopDismissed(@NonNull Context context) {
        return Preferences.getInstance(context).getBoolean("draw_on_top_dismissed", false);
    }

    public static void setDrawOnTopDismissed(@NonNull Context context, boolean dismissed) {
        Preferences.getInstance(context).putBoolean("draw_on_top_dismissed", dismissed);
    }

    public static boolean isIPv6Enabled(@NonNull Context context) {
        return Preferences.getInstance(context).getBoolean( "setting_ipv6_enabled", false);
    }

    public static boolean isIPv4Enabled(@NonNull Context context) {
        return Preferences.getInstance(context).getBoolean(  "setting_ipv4_enabled", true);
    }

    public static void setPinEnabled(@NonNull Context context, boolean enabled ){
        Preferences.getInstance(context).put(  "setting_pin_enabled", enabled);
    }

    public static void setPinOnAppStart(@NonNull Context context, boolean enabled ){
        Preferences.getInstance(context).put(  "pin_app", enabled);
    }

    public static void setPinOnNotify(@NonNull Context context, boolean enabled ){
        Preferences.getInstance(context).put(  "pin_notification", enabled);
    }

    public static void setPin(@NonNull Context context, String pin ){
        Preferences.getInstance(context).put(  "pin_value", pin);
    }

    public static boolean isDebugEnabled(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "debug", false);
    }

    public static boolean shouldHideNotificationIcon(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "hide_notification_icon", false);
    }

    public static boolean isNotificationEnabled(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "setting_show_notification", false);
    }

    public static boolean isPinProtectionEnabled(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "setting_pin_enabled", false);
    }

    @NonNull
    public static String getPinCode(@NonNull Context context){
        return Preferences.getInstance(context).getString(  "pin_value", "1234");
    }

    public static boolean canUseFingerprintForPin(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "pin_fingerprint", false);
    }

    public static void setDNS1(Context context, String dns1) {
        Preferences.getInstance(context).putString("dns1", dns1);
    }

    public static void setDNS2(Context context, String dns2) {
        Preferences.getInstance(context).putString("dns2", dns2);
    }

    @NonNull
    public static String getDNS1(@NonNull Context context) {
        return isIPv4Enabled(context) ? Preferences.getInstance(context).getString(  "dns1", "104.197.28.121") : "";
    }

    @NonNull
    public static boolean isPinProtected(@NonNull Context context, PinProtectable pinProtectable){
        return pinProtectable.isEnabled(context);
    }

    public static void setDNSUUID(Context context, String uuid) {
        Preferences.getInstance(context).put("dns_token", uuid);
    }

    public static String getDNSUUID(Context context) {
        return Preferences.getInstance(context).getString("dns_token", null);
    }

    public static boolean getAccountAccessRevoked(Context context) {
        return Preferences.getInstance(context).getBoolean("account_access_revoked", false);
    }

    /**
     * Set whether access to the signed in account has been revoked. This does not in itself
     * revoke access on the API side.
     */
    public static void setAccountAccessRevoked(Context context, boolean revoked) {
        Preferences.getInstance(context).putBoolean("account_access_revoked", revoked);
    }

    /**
     * Since update 72/73 added a bunch of account management features, we require a re-auth past
     * this version in case people are logged in to devices where their account shouldn't be manageable.
     * @return True if the re-auth has been done.
     */
    public static boolean getPost73ReAuthDone(Context context) {
        return Preferences.getInstance(context).getBoolean("post_73_reauth_done", false);
    }

    public static void setPost73ReAuthDone(Context context, boolean done) {
        Preferences.getInstance(context).putBoolean("post_73_reauth_done", done);
    }

    public static boolean getLightweightMode(Context applicationContext) {
        return true;
    }

    /**
     * @return Whether the swear words category for chat guard is enabled.
     */
    public static boolean isChatGuardSwearWordsEnabled(Context context) {
        return Preferences.getInstance(context).getBoolean("chat_guard_category_swear", true);
    }

    /**
     * Set whether the swear words category for chat guard is enabled.
     */
    public static void setChatGuardSwearWordsEnabled(Context context, boolean enabled) {
        Preferences.getInstance(context).putBoolean("chat_guard_category_swear", enabled);
        KeywordsManager.getInstance().init(context);
    }

    /**
     * @return Whether the drugs category for chat guard is enabled.
     */
    public static boolean isChatGuardDrugsEnabled(Context context) {
        return Preferences.getInstance(context).getBoolean("chat_guard_category_drugs", true);
    }

    /**
     * Set whether the drugs category for chat guard is enabled.
     */
    public static void setChatGuardDrugsEnabled(Context context, boolean enabled) {
        Preferences.getInstance(context).putBoolean("chat_guard_category_drugs", enabled);
        KeywordsManager.getInstance().init(context);
    }

    /**
     * @return Whether the sexting category for chat guard is enabled.
     */
    public static boolean isChatGuardSextingEnabled(Context context) {
        return Preferences.getInstance(context).getBoolean("chat_guard_category_sexting", true);
    }

    /**
     * Set whether the sexting category for chat guard is enabled.
     */
    public static void setChatGuardSextingEnabled(Context context, boolean enabled) {
        Preferences.getInstance(context).putBoolean("chat_guard_category_sexting", enabled);
        KeywordsManager.getInstance().init(context);
    }

    /**
     * @return Whether the self harm category for chat guard is enabled.
     */
    public static boolean isChatGuardSelfHarmEnabled(Context context) {
        return Preferences.getInstance(context).getBoolean("chat_guard_category_self_harm", true);
    }

    /**
     * Set whether the self harm category for chat guard is enabled.
     */
    public static void setChatGuardSelfHarmEnabled(Context context, boolean enabled) {
        Preferences.getInstance(context).putBoolean("chat_guard_category_self_harm", enabled);
        KeywordsManager.getInstance().init(context);
    }

    public enum PinProtectable{
        APP("pin_app"), APP_SHORTCUT("pin_app_shortcut"), TILE("pin_tile"), NOTIFICATION("pin_notification");

        @NonNull private final String settingsKey;
        PinProtectable(@NonNull String settingsKey){
            this.settingsKey = settingsKey;
        }

        private boolean isEnabled(Context context){
            return Preferences.getInstance(context).getBoolean(  "setting_pin_enabled", false) &&
                    Preferences.getInstance(context).getBoolean(  settingsKey, true);
        }

    }
}
