package com.safesurfer.util;

import android.util.Pair;

import org.xbill.DNS.DClass;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.Type;

import java.util.ArrayList;
import java.util.List;

import java9.util.function.Consumer;
import java9.util.stream.Collectors;
import java9.util.stream.IntStream;
import java9.util.stream.StreamSupport;

public class DNSSpeedTest implements Runnable {

    private final List<String> candidates;
    private final Consumer<List<String>> onDone;
    private final int numAttempts;
    private final int timeout;

    public DNSSpeedTest(List<String> candidates, int numAttempts, int timeout, Consumer<List<String>> onDone) {
        this.candidates = candidates;
        this.onDone = onDone;
        this.numAttempts = numAttempts;
        this.timeout = timeout;
    }

    @Override
    public void run() {
        long[][] results = new long[candidates.size()][numAttempts];
        IntStream.range(0, candidates.size()).parallel().forEach(i -> {
            for (int j = 0; j < numAttempts; j++) {
                try {
                    Resolver resolver = new SimpleResolver(candidates.get(i));
                    resolver.setTimeout(this.timeout);
                    Lookup lookup = new Lookup("google.com.", Type.A, DClass.IN);
                    lookup.setResolver(resolver);
                    lookup.getDefaultCache(DClass.IN).clearCache();
                    long before = System.currentTimeMillis();
                    lookup.run();
                    long duration = System.currentTimeMillis() - before;
                    results[i][j] = duration;
                } catch (Exception e) {
                    results[i][j] = Long.MAX_VALUE;
                }
            }
        });
        long[] avgs = new long[candidates.size()];
        for (int i = 0; i < results.length; i++) {
            long sum = 0;
            for (long r : results[i]) {
                sum += r;
            }
            double avg = 1.0d * sum / numAttempts;
            avgs[i] = Math.round(avg);
        }
        List<Pair<Long, String>> candidateResultPair = new ArrayList<>();
        for (int i = 0; i < candidates.size(); i++) {
            candidateResultPair.add(new Pair<>(avgs[i], candidates.get(i)));
        }
        onDone.accept(StreamSupport.stream(candidateResultPair)
                .sorted((p1, p2) -> (int) (p1.first - p2.first))
                .map(p -> p.second)
                .collect(Collectors.toList()));
    }
}
