package com.safesurfer.util;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.PersistableBundle;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.provider.Settings;

import com.safesurfer.BuildConfig;
import com.safesurfer.LogFactory;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.screens.subscription.SubscriptionActivity;
import com.safesurfer.services.ConnectivityBackgroundService;
import com.safesurfer.services.VirtualTunnelService;
import com.safesurfer.services.jobs.ConnectivityJobAPI21;

import com.frostnerd.general.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public final class Util {
    private static final String LOG_TAG = "[Util]";
    static final Pattern ipv6WithPort = Pattern.compile("(\\[[0-9a-f:]+]:[0-9]{1,5})|([0-9a-f:]+)");
    static final Pattern ipv4WithPort = Pattern.compile("([0-9]{1,3}\\.){3}[0-9]{1,3}(:[0-9]{1,5})?");

    public static boolean isServiceRunning() {
        return VirtualTunnelService.isRunning();
    }

    public static boolean isServiceThreadRunning() {
        return VirtualTunnelService.isRunning();
    }

    public static void runBackgroundConnectivityCheck(Context context, boolean handleInitialState){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LogFactory.writeMessage(context, LOG_TAG, "Using JobScheduler");
            if(isJobRunning(context, 0)){
                LogFactory.writeMessage(context, LOG_TAG, "Job is already running/scheduled, not doing anything");
                return;
            }
            PersistableBundle extras = new PersistableBundle();
            extras.putBoolean("initial", handleInitialState);
            System.out.println("HANDLE INITIAL STATE: " + handleInitialState);
            JobScheduler scheduler = Utils.requireNonNull((JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE));
            scheduler.schedule(new JobInfo.Builder(0, new ComponentName(context, ConnectivityJobAPI21.class)).setPersisted(true)
                    .setRequiresCharging(false).setMinimumLatency(0).setOverrideDeadline(0).setExtras(extras).build());
        } else {
            LogFactory.writeMessage(context, LOG_TAG, "Starting Service (Util below 21)");
            context.startService(new Intent(context, ConnectivityBackgroundService.class).putExtra("initial", handleInitialState));
        }
    }

    public static void stopBackgroundConnectivityCheck(Context context){
        LogFactory.writeMessage(context, LOG_TAG, "Stopping the background connectivity check..");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LogFactory.writeMessage(context, LOG_TAG, "Using JobScheduler");
            if(isJobRunning(context, 0)){
                LogFactory.writeMessage(context, LOG_TAG, "Job is running, stopping..");
                JobScheduler scheduler = Utils.requireNonNull((JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE));
                scheduler.cancel(0);
            }else {
                LogFactory.writeMessage(context, LOG_TAG, "Job is not running, thus not stopping.");
            }
        } else {
            if(isBackgroundConnectivityCheckRunning(context)){
                LogFactory.writeMessage(context, LOG_TAG, "Stopping Service (API below 21)");
                context.stopService(new Intent(context, ConnectivityBackgroundService.class));
            } else {
                LogFactory.writeMessage(context, LOG_TAG, "Service is not running, thus not stopping.");
            }
        }
    }

    public static boolean isBackgroundConnectivityCheckRunning(@NonNull Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return isJobRunning(context, 0);
        } else {
            return Utils.isServiceRunning(context, ConnectivityBackgroundService.class);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private static boolean isJobRunning(@NonNull Context context, @IntRange(from = 0) int jobID){
        JobScheduler scheduler = Utils.requireNonNull((JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE));
        for ( JobInfo jobInfo : scheduler.getAllPendingJobs())
            if (jobInfo.getId() == jobID) return true;
        return false;
    }

    /**
     * This Method is used instead of getActivity() in a fragment because getActivity() returns null in some rare cases
     * @param fragment
     * @return
     */
    public static FragmentActivity getActivity(Fragment fragment){
        if(fragment.getActivity() == null){
            if(fragment.getContext() != null && fragment.getContext() instanceof FragmentActivity){
                return (FragmentActivity)fragment.getContext();
            }else return null;
        }else return fragment.getActivity();
    }

    /**
     * @return An intent leading to the appropriate settings activity for disabling battery optimization
     *          for the app.
     */
    public static Intent getBatteryOptimizationIntent(Context ctx) {
        final List<Intent> powermanagerIntents = Arrays.asList(
            new Intent().setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")),
            new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity")),
                new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.startupmgr.ui.StartupNormalAppListActivity")),
            new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity")),
                new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity")),
            new Intent().setComponent(new ComponentName(
                    "com.huawei.systemmanager",
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
                            ? "com.huawei.systemmanager.startupmgr.ui.StartupNormalAppListActivity"
                            : "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity"
            )),
            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.privacypermissionsentry.PermissionTopActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.oppoguardelf", "com.coloros.powermanager.fuelgaue.PowerUsageModelActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.oppoguardelf", "com.coloros.powermanager.fuelgaue.PowerSaverModeActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.oppoguardelf", "com.coloros.powermanager.fuelgaue.PowerConsumptionActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity")),
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                    ? new Intent().setComponent(
                            new ComponentName(
                                    "com.coloros.safecenter",
                                    "com.coloros.safecenter.startupapp.StartupAppListActivity")
                        )
                        .setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
                        .setData(Uri.parse("package:"+ ctx.getPackageName()))
                    : null,
            new Intent().setComponent(new ComponentName("com.oppo.safe", "com.oppo.safe.permission.startup.StartupAppListActivity")),
            new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity")),
            new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager")),
            new Intent().setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")),
            new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.entry.FunctionActivity")),
            new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.autostart.AutoStartActivity")),
            new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.MainActivity")),
            new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"))
                .setData(android.net.Uri.parse("mobilemanager://function/entry/AutoStart")),
            new Intent().setComponent(
                    new ComponentName(
                            "com.meizu.safe",
                            "com.meizu.safe.security.SHOW_APPSEC")
                    )
                    .addCategory(Intent.CATEGORY_DEFAULT).putExtra("packageName", BuildConfig.APPLICATION_ID),
            new Intent().setComponent(new ComponentName("com.samsung.android.lool", "com.samsung.android.sm.ui.battery.BatteryActivity")),
            new Intent().setComponent(new ComponentName("com.htc.pitroad", "com.htc.pitroad.landingpage.activity.LandingPageActivity"))
        );
        for (Intent intent : powermanagerIntents) {
            if (isCallable(ctx, intent)) {
                return intent;
            }
        }
        return new Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
    }

    /**
     * @return An appropriate intent for acquiring a pro surfer subscription. Null if we can't
     *         determine an appropriate intent. Does not check if we have pro or not.
     */
    public static Intent getProIntent(Context context) {
        if (SubscriptionUtils.getState() != SubscriptionUtils.BillingState.INITIALIZED) {
            return null;
        }
        if (SubscriptionUtils.getSubscriptionPlatform(SubscriptionUtils.parseDeviceJWTClaims()) == SubscriptionUtils.SubscriptionPlatform.GOOGLE_PLAY) {
            return new Intent(context, SubscriptionActivity.class)
                    .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true)
                    .putExtra(SubscriptionActivity.INTENT_EXTRA_SHOW_CLOSE, true);
        }
        return new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://my.safesurfer.io/account-settings?section=payment"));
    }

    private static boolean isCallable(Context context, Intent intent) {
        try {
            if (intent == null || context == null) {
                return false;
            } else {
                List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
                return list.size() > 0;
            }
        } catch (Exception ignored) {
            return false;
        }
    }
}