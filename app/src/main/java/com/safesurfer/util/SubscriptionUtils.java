package com.safesurfer.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Base64;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.gson.Gson;
import com.safesurfer.LogFactory;
import com.safesurfer.R;
import com.safesurfer.network_api.SafeSurferNetworkApi;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nullable;

import java9.util.Sets;
import java9.util.function.Consumer;
import java9.util.stream.Collectors;
import java9.util.stream.StreamSupport;

/**
 * Tools and nested classes for dealing with subscriptions. This contains some global billing
 * state that views can subscribe to. Begin by calling init() which will fetch billing information
 * and if successful, may automatically register purchases with the API etc. If a view is interested
 * in billing state, it can register listeners and then query the static methods when it updates.
 */
public class SubscriptionUtils {

    private static final Set<String> PRO_SURFER_SUB_IDS = Sets.of(
            "pro_surfer_0_1",
            "pro_surfer_1_2",
            "pro_surfer_3_4",
            "pro_surfer_5_50"
    );

    public enum BillingState {
        UNINITIALIZED,
        INITIALIZING,
        INITIALIZED,
        PURCHASING,
        FAILED
    }

    /**
     * Describes the result of an init or purchase operation.
     */
    public enum Result {
        /**
         * May be returned to an init listener to indicate that fetching the initial info
         * failed, e.g. due to connectivity issues.
         */
        INIT_FAILED,
        /**
         * May be returned to an init listener to indicate that everything was OK.
         */
        INIT_SUCCESS,
        /**
         * May be returned to an init listener to indicate that the pro subscription isn't available.
         */
        PRO_NOT_AVAILABLE,
        /**
         * May be returned to an init listener to indicate that an external purchase of pro
         * has occurred and the user must sign in to claim it.
         */
        PRO_WAITING_FOR_SIGN_IN,
        /**
         * May be returned to an init or purchase listener to indicate that the local purchase
         * succeeded, but couldn't be registered with the API due to a conflict.
         */
        PURCHASE_FAILED_CONFLICT,
        /**
         * May be returned to an init or purchase listener to indicate that the purchase couldn't
         * be made due to an unknown error during the registration stage.
         */
        PURCHASE_FAILED_UNKNOWN,
        /**
         * May be returned to a purchase listener to indicate that it failed within the billing
         * library, usually meaning there isn't enough money on the payment method.
         */
        PURCHASE_FAILED_BILLING
    }

    private static final AtomicReference<BillingState> state = new AtomicReference<>(BillingState.UNINITIALIZED);
    private static final List<Consumer<Result>> initListeners = new LinkedList<>();
    private static Result lastInitResult = null;

    private static Context context = null;
    private static BillingClient client = null;
    private static boolean skusLoaded = false;
    private static List<SkuDetails> skuDetailsList = null;
    private static boolean purchasesLoaded = false;
    private static List<Purchase> purchasesList = null;
    private static boolean jwtLoaded = false;
    private static Set<String> repurchaseBlacklist = new HashSet<>();

    /**
     * Run when a new purchase is made using the billing library. Will register the purchase with
     * the API and then re-init with the new data.
     */
    private static PurchasesUpdatedListener purchasesUpdatedListener = (billingResult, purchases) -> {
        if (billingResult.getResponseCode() != BillingClient.BillingResponseCode.OK) {
            state.set(BillingState.FAILED);
            dispatchOnInit(Result.PURCHASE_FAILED_BILLING);
            return;
        }
        if (purchases == null) {
            return;
        }
        // This is run when a user successfully completes the billing flow for pro surfer.
        for (Purchase purchase : purchases) {
            if (PRO_SURFER_SUB_IDS.contains(purchase.getSku())) {
                SubscriptionUtils.registerPurchase(purchase, false);
            }
        }
    };

    /**
     * Register the purchase with the API and refresh the stream view once done.
     * @param purchase The purchase to register.
     * @param duringInit Whether we are registering during the init process. If not, init will be started
     *                   after the purchase has been registered.
     */
    @SuppressLint("CheckResult")
    private static void registerPurchase(Purchase purchase, boolean duringInit) {
        // Register with API
        SafeSurferNetworkApi.getInstance().getSRUseCase()
                .registerSubscription(
                        Tools.getInstance().getDeviceAuthToken(),
                        "google-play",
                        purchase.getSku(),
                        purchase.getPurchaseToken())
                .subscribe(response -> {
                    switch (response.code()) {
                        case 200:
                            // Response should be a new JWT
                            String newJWT = response.body().string().replaceAll("^\"|\"$", "");
                            Tools.getInstance().setDeviceAuthToken(newJWT);
                            if (duringInit) {
                                postProcessAsyncTasks();
                            } else {
                                // Now that we've registered a new purchase, make sure that we
                                // don't try to re-register our current one, as this will now
                                // be cancelled but still present.
                                if (purchasesList != null) {
                                    for (Purchase currPurchase : purchasesList) {
                                        repurchaseBlacklist.add(currPurchase.getPurchaseToken());
                                    }
                                }
                                if (getState() != BillingState.INITIALIZING) {
                                    init(context);
                                }
                            }
                            break;
                        case 409:
                            if (duringInit) {
                                state.set(BillingState.FAILED);
                                dispatchOnInit(Result.PURCHASE_FAILED_CONFLICT);
                            } else if (getState() != BillingState.INITIALIZING) {
                                init(context);
                            }
                            break;
                        default:
                            if (duringInit) {
                                state.set(BillingState.FAILED);
                                dispatchOnInit(Result.PURCHASE_FAILED_UNKNOWN);
                            } else if (getState() != BillingState.INITIALIZING) {
                                init(context);
                            }
                    }
                }, fail -> {
                    if (duringInit) {
                        state.set(BillingState.FAILED);
                        dispatchOnInit(Result.PURCHASE_FAILED_UNKNOWN);
                    } else {
                        init(context);
                    }
                });
    }

    /**
     * Reset the fields of this class used for billing to their original state.
     */
    private static void resetBillingParams () {
        skusLoaded = false;
        skuDetailsList = null;
        purchasesLoaded = false;
        purchasesList = null;
        jwtLoaded = false;
        lastInitResult = null;
    }

    /**
     * Register interest in the init() process completing. If the process has already completed,
     * will be dispatched immediately.
     */
    public static void onInit(Consumer<Result> listener) {
        switch (getState()) {
            case PURCHASING:
            case INITIALIZING:
                initListeners.add(listener);
                return;
            default:
                try {
                    listener.accept(lastInitResult);
                } catch (Exception e) {
                    LogFactory.writeStackTrace(context, new String[]{LogFactory.Tag.ERROR.toString()}, e);
                    e.printStackTrace();
                }
        }
    }

    /**
     * Run all registered listeners for init completing, and ensure that future added ones
     * are dispatched.
     * @param result The result to give to listeners.
     */
    private static void dispatchOnInit(Result result) {
        lastInitResult = result;
        for (Consumer<Result> c : initListeners) {
            try {
                c.accept(result);
            } catch (Exception e) {
                LogFactory.writeStackTrace(context, new String[]{LogFactory.Tag.ERROR.toString()}, e);
                e.printStackTrace();
            }
        }
        initListeners.clear();
    }

    /**
     * Start initializing. This will refresh the app's JWT token, get local purchases, and
     * available purchases. If necessary, it will register a local purchase with the API.
     * Register a handler using onceInitialized to get a callback when done.
     * Should run in a thread.
     */
    @SuppressLint("CheckResult")
    public static void init(Context ctx) {
        if (getState() == BillingState.INITIALIZING) {
            throw new Error("Already initializing");
        }
        state.set(BillingState.INITIALIZING);
        context = ctx;
        client = BillingClient.newBuilder(context)
                .setListener(purchasesUpdatedListener)
                .enablePendingPurchases()
                .build();
        resetBillingParams();
        // There are three async tasks we need in order to decide what to do:
        // - Get local purchases
        // - Get available subscriptions
        // - Get refreshed JWT token
        // Once done, delegate further processing to postProcessAsyncTasks
        client.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() ==  BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    // Query SKU details
                    List<String> skuList = Arrays.asList(PRO_SURFER_SUB_IDS.toArray(new String[]{}));
                    SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                    params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
                    client.querySkuDetailsAsync(params.build(),
                            (billingResult1, skuDetailsList) -> {
                                if (billingResult1.getResponseCode() ==  BillingClient.BillingResponseCode.OK) {
                                    SubscriptionUtils.skuDetailsList = skuDetailsList;
                                }
                                SubscriptionUtils.skusLoaded = true;
                                SubscriptionUtils.postProcessAsyncTasks();
                            });
                    // Query purchases
                    Purchase.PurchasesResult result = client.queryPurchases(BillingClient.SkuType.SUBS);
                    if (result.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        SubscriptionUtils.purchasesList = result.getPurchasesList();
                    }
                    SubscriptionUtils.purchasesLoaded = true;
                    SubscriptionUtils.postProcessAsyncTasks();
                }
            }
            @Override
            public void onBillingServiceDisconnected() {
                SubscriptionUtils.purchasesLoaded = true;
                SubscriptionUtils.skusLoaded = true;
                SubscriptionUtils.postProcessAsyncTasks();
            }
        });
        // Refresh the JWT token to get any new subscription info
        if (Tools.getInstance().getDeviceAuthToken() != null) {
            String jwtToken = Tools.getInstance().getDeviceAuthToken();
            SafeSurferNetworkApi.getInstance()
                    .getSRUseCase()
                    .refreshJWTToken(jwtToken)
                    .toObservable()
                    .subscribe(
                            success -> {
                                switch (success.code()){
                                    case 200:
                                        String newJWTToken = Tools.removeQuotes( success.body().string() );
                                        Tools.getInstance().setDeviceAuthToken(newJWTToken);
                                    default:
                                        SubscriptionUtils.jwtLoaded = true;
                                        SubscriptionUtils.postProcessAsyncTasks();
                                }
                            },
                            fail -> {
                                SubscriptionUtils.jwtLoaded = true;
                                SubscriptionUtils.postProcessAsyncTasks();
                            });
        } else {
            SubscriptionUtils.jwtLoaded = true;
            SubscriptionUtils.postProcessAsyncTasks();
        }
    }

    /**
     * Once we have the purchases, subscriptions, and JWT, do any actions required and finish
     * initialization.
     */
    private static void postProcessAsyncTasks() {
        if (jwtLoaded && purchasesLoaded && skusLoaded) {
            // If any haven't loaded, something has failed
            if (skuDetailsList == null || purchasesList == null) {
                state.set(BillingState.FAILED);
                dispatchOnInit(Result.INIT_FAILED);
                return;
            }
            // Check sku is available
            if (skuDetailsList.size() == 0) {
                state.set(BillingState.FAILED);
                dispatchOnInit(Result.PRO_NOT_AVAILABLE);
                return;
            }
            // If there's a relevant unregistered local purchase, try to register it now.
            Purchase relevantPurchase = null;
            for (Purchase purchase : purchasesList) {
                 if (PRO_SURFER_SUB_IDS.contains(purchase.getSku()) && !repurchaseBlacklist.contains(purchase.getPurchaseToken())) {
                      relevantPurchase = purchase;
                      break;
                 }
            }
            if (relevantPurchase != null) {
                  // Check if registered
                  JWTClaims claims = parseDeviceJWTClaims();
                  if (claims == null) {
                            // Haven't signed in yet, show prompt to sign in
                      state.set(BillingState.INITIALIZED);
                      dispatchOnInit(Result.PRO_WAITING_FOR_SIGN_IN);
                      return;
                  } else {
                      // Have signed in, check if we're already using the relevant purchase.
                      // If not, try to register.
                      // registerPurchase will run this function again if successful, but this
                      // shouldn't be an issue is if successful, the local purchase token should
                      // match the remote one.
                      if (!claims.getPurchaseToken().equals(relevantPurchase.getPurchaseToken())) {
                          registerPurchase(relevantPurchase, true);
                          return;
                      }
                  }
            }
            state.set(BillingState.INITIALIZED);
            dispatchOnInit(Result.INIT_SUCCESS);
        }
    }

    /**
     * @return The current billing state.
     */
    public static BillingState getState() {
        return state.get();
    }

    /**
     * Launch the billing flow, if we are initialized. Once the billing flow completes,
     * re-initialize.
     * @param activity The activity to give to the billing client.
     */
    public static void launchBilling(Activity activity, String skuID) {
        if (!getState().equals(BillingState.INITIALIZED)) {
            throw new Error("Must be initialized before launching billing");
        }
        List<SkuDetails> skuDetails = StreamSupport.stream(skuDetailsList)
                .filter(currSku -> currSku.getSku().equals(skuID))
                .collect(Collectors.toList());
        if (skuDetails.isEmpty()) {
            return;
        }
        BillingFlowParams billingFlowParams = BillingFlowParams
                .newBuilder()
                .setSkuDetails(skuDetails.get(0))
                .build();
        state.set(BillingState.PURCHASING);
        client.launchBillingFlow(activity, billingFlowParams);
    }

    /**
     * @return If we have loaded successfully, return the available SKUs.
     */
    public static List<SkuDetails> getSkuDetailsList() {
        return skuDetailsList;
    }

    /**
     * @return An object representing the claims of the device's JWT, or null if we are not signed in.
     */
    public static JWTClaims parseDeviceJWTClaims() {
        String jwt = Tools.getInstance().getDeviceAuthToken();
        if (jwt == null) {
            return null;
        }
        String[] splitJWT = jwt.split("\\.");
        if (splitJWT.length != 3) {
            throw new Error("Invalid JWT");
        }
        byte[] claimsBytes = Base64.decode(splitJWT[1], Base64.DEFAULT);
        Gson gson = new Gson();
        return gson.fromJson(new String(claimsBytes), JWTClaims.class);
    }

    /**
     * BASIC means pro features are not enabled.
     * TRIAL means they are, but only for a short time.
     * PRO means they are.
     */
    public enum SubscriptionBadgeMode {
        BASIC,
        TRIAL,
        PRO
    }

    /**
     * @return What to display in the subscription badge.
     */
    public static SubscriptionBadgeMode getSubscriptionBadgeMode(@Nullable JWTClaims claims) {
        if (claims == null) {
            return SubscriptionBadgeMode.BASIC;
        }
        switch (claims.getSubStatus()) {
            case SUBSCRIPTION_STATUS_MISSING:
            case SUBSCRIPTION_STATUS_INACTIVE_UNKNOWN:
            case SUBSCRIPTION_STATUS_NOT_PAID:
            case SUBSCRIPTION_STATUS_NO_CARD:
            case SUBSCRIPTION_STATUS_CANCELLED:
                return SubscriptionBadgeMode.BASIC;
            case SUBSCRIPTION_STATUS_TRIAL:
            case SUBSCRIPTION_STATUS_TRIAL_CANCELLED:
                return SubscriptionBadgeMode.TRIAL;
            default:
                return SubscriptionBadgeMode.PRO;
        }
    }

    /**
     * @return An R constant string ID describing the current state of the subscription.
     */
    public static int getSubscriptionDescription(@Nullable JWTClaims claims) {
        if (claims == null) {
            return R.string.subscription_description_not_signed_in;
        }
        switch (claims.getSubStatus()) {
            case SUBSCRIPTION_STATUS_MISSING:
                return R.string.subscription_description_missing;
            case SUBSCRIPTION_STATUS_COURTESY:
            case SUBSCRIPTION_STATUS_ACTIVE:
                return R.string.subscription_description_active;
            case SUBSCRIPTION_STATUS_INACTIVE_UNKNOWN:
                return R.string.subscription_description_inactive_unknown;
            case SUBSCRIPTION_STATUS_TRIAL_CANCELLED:
                return R.string.subscription_status_trial_cancelled;
            case SUBSCRIPTION_STATUS_TRIAL:
                return R.string.subscription_status_trial;
            case SUBSCRIPTION_STATUS_NOT_PAID:
            case SUBSCRIPTION_STATUS_NO_CARD:
                return R.string.subscription_status_not_paid;
            case SUBSCRIPTION_STATUS_CANCELLED:
                return R.string.subscription_status_cancelled;
            case SUBSCRIPTION_STATUS_NON_RENEWING:
                return R.string.subcription_status_non_renewing;
            default:
                return R.string.empty;
        }
    }

    /**
     * @param claims The parsed JWT claims.
     * @return Whether pro features should be enabled.
     */
    public static boolean areProFeaturesEnabled(@Nullable JWTClaims claims) {
        if (claims == null) {
            return false;
        }
        switch (getSubscriptionBadgeMode(claims)) {
            case BASIC:
                return false;
            default:
                return true;
        }
    }

    public enum SubscriptionPlatform {
        NONE,
        WEB,
        GOOGLE_PLAY
    }

    /**
     * @return The subscription platform the user is using.
     */
    public static SubscriptionPlatform getSubscriptionPlatform(@Nullable JWTClaims claims) {
        if (claims == null) {
            return SubscriptionPlatform.NONE;
        }
        if (claims.getPurchaseToken().equals("")) {
            return SubscriptionPlatform.WEB;
        }
        return SubscriptionPlatform.GOOGLE_PLAY;
    }

    /**
     * Should only be called after initialization.
     * @param claims The current JWT claims.
     * @return Whether we should show options to launch the pro surfer billing flow.
     */
    public static boolean shouldOfferSignUp(@Nullable JWTClaims claims) {
        // Offer if:
        // - Is signed in
        // - We have loaded the subscription and purchases successfully
        // - There is no existing subscription attached to the JWT OR there is, but it's an android
        //  subscription that's been cancelled.
        if (claims == null) {
            return false;
        }
        if (!getState().equals(BillingState.INITIALIZED)) {
            return false;
        }
        return claims.getSubStatus().equals(SUBSCRIPTION_STATUS_MISSING) ||
                (claims.getSubStatus().equals(SUBSCRIPTION_STATUS_CANCELLED) && !claims.getPurchaseToken().equals(""));
    }

    public static final String SUBSCRIPTION_STATUS_MISSING = "MISSING";
    public static final String SUBSCRIPTION_STATUS_COURTESY = "COURTESY";
    public static final String SUBSCRIPTION_STATUS_INACTIVE_UNKNOWN = "INACTIVE_UNKNOWN";
    public static final String SUBSCRIPTION_STATUS_ACTIVE = "ACTIVE";
    public static final String SUBSCRIPTION_STATUS_TRIAL_CANCELLED = "TRIAL_CANCELLED";
    public static final String SUBSCRIPTION_STATUS_TRIAL = "TRIAL";
    public static final String SUBSCRIPTION_STATUS_NOT_PAID = "NOT_PAID";
    public static final String SUBSCRIPTION_STATUS_NO_CARD = "NO_CARD";
    public static final String SUBSCRIPTION_STATUS_CANCELLED = "CANCELLED";
    public static final String SUBSCRIPTION_STATUS_NON_RENEWING = "NON_RENEWING";

    public static class JWTClaims {
        public String getId() {
            return id;
        }

        public String getDeviceID() {
            return deviceID;
        }

        public String getSubStatus() {
            return subStatus;
        }

        public long getNextBill() {
            return nextBill;
        }

        public String getPurchaseToken() {
            return purchaseToken;
        }

        public String getPlanID() {
            return planID;
        }

        public long getExp() {
            return exp;
        }

        private String id = "";
        private String deviceID = "";
        private String subStatus = "";
        private long nextBill = 0;
        private String purchaseToken = "";
        private String planID = "";
        private long exp = 0;
        public JWTClaims () {}
    }
}
