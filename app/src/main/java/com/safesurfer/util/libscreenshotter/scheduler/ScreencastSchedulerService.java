package com.safesurfer.util.libscreenshotter.scheduler;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import static com.safesurfer.util.Tools.SCREENCAST_SERVICE_ID;


public class ScreencastSchedulerService extends JobIntentService {

    private static final String ACTION_START = "com.safesurfer.util.libscreenshotter.scheduler.ScreencastSchedulerService.ACTION_START";


    public static void startAction(Context context) {

        Intent intent = new Intent(context, ScreencastSchedulerService.class);
        intent.setAction(ACTION_START);

        ScreencastSchedulerService.enqueueWork(context,  ScreencastSchedulerService.class, SCREENCAST_SERVICE_ID,  intent);

    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_START.equals(action)) {
                Intent __intent = new Intent(this, ScreenCapturerActivity.class);

                __intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_MULTIPLE_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY );

                startActivity(__intent);
            }
        }
    }
}
