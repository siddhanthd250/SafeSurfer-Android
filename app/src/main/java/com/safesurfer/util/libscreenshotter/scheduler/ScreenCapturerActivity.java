package com.safesurfer.util.libscreenshotter.scheduler;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.safesurfer.util.Tools;

public class ScreenCapturerActivity extends Activity {

    Context context;

    ServiceConnection serviceConnection;
    ScreenCapturerService serviceReference;
    Intent serviceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Tools.getInstance().isScreencastDetectionActive()) {
            finish();
            return;
        }
        Tools.getInstance().setScreencastDetectionActive(true);

        Log.d(getClass().getName(), "ScreenCapturerActivity onCreate");
        context = this;


        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

                Log.d(getClass().getName(), "onServiceConnected");

                serviceReference = ((ScreenCapturerService.ScreenCapturerServiceBinder) iBinder).getmServiceInstance();

                if (data == null) {
                    startMediaProjection();
                } else {
                    serviceReference.onActivityResult(requestCode, resultCode, data);

                }
                setBinded(true);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
//                serviceReference = null;

                Log.d(getClass().getName(), "onServiceDisconnected");
                setBinded(false);
            }
        };

        try {
            serviceIntent = new Intent(this, ScreenCapturerService.class);
            startService(serviceIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(getClass().getName(), "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(getClass().getName(), "onResume()");

        try {

            if (!isBinded()) {
                try {
                    bindService(serviceIntent, serviceConnection, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isBinded() {
        return isBinded;
    }

    public void setBinded(boolean binded) {
        isBinded = binded;
    }

    boolean isBinded;

    @Override
    protected void onPause() {
        super.onPause();

        Log.d(getClass().getName(), "onPause()");
//        unbindService(serviceConnection);
    }

    private void startMediaProjection() {
        mediaProjectionManager = (MediaProjectionManager) getSystemService(MEDIA_PROJECTION_SERVICE);
        startActivityForResult(mediaProjectionManager.createScreenCaptureIntent(), 1);
        Log.d(getClass().getName(), "ScreenCapturerActivity  startActivityForResult(mediaProjectionManager.createScreenCaptureIntent(), 1);");
    }

    int requestCode;
    int resultCode;
    Intent data;

    Handler mUiHandler = new Handler();

    MediaProjectionManager mediaProjectionManager;
    MediaProjection mediaProjection;

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        Log.d(getClass().getName(), "ScreenCapturerActivity  onActivityResult");
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                this.requestCode = requestCode;
                this.resultCode = requestCode;
                this.data = data;

                serviceReference.onActivityResult(requestCode, resultCode, data);

                finish();
            }else{
                Tools.getInstance().setScreencastDetectionActive(false);
                finish();
            }
        }
    }

}
