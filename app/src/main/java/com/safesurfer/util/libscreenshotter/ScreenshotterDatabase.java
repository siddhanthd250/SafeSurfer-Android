package com.safesurfer.util.libscreenshotter;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Pair;

import androidx.core.app.NotificationCompat;

import com.safesurfer.R;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.tensorflow.EventModel;
import com.safesurfer.tensorflow.EventModelScores;
import com.safesurfer.util.Tools;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.support.common.FileUtil;
import org.tensorflow.lite.support.image.ImageProcessor;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.image.ops.ResizeOp;
import org.tensorflow.lite.support.label.TensorLabel;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.safesurfer.tensorflow.EventModel.EventModelTypesEXPLICIT;
import static com.safesurfer.tensorflow.EventModel.EventModelTypesNEUTRAL;
import static com.safesurfer.tensorflow.EventModel.EventModelTypesSUGGESTIVE;
import static com.safesurfer.util.Tools.NOTIFICATION_ID;

public class ScreenshotterDatabase {

    AtomicBoolean canMakeScreenshot = new AtomicBoolean(true);

    private final int RECORDING_LEVEL_NONE = 1;
    private final int RECORDING_LEVEL_SCORES_SCREENS = 3;
    private static final String MODEL_PATH = "neutral-porn-suggestive-classifier.tflite";
    private static final int INPUT_SIZE = 224;

    /**
     * Thresholds for what's considered each confidence value.
     */
    private static final float HIGH_CONFIDENCE_MIN = 4;
    private static final float MEDIUM_CONFIDENCE_MIN = 2;

    final String ASSOCIATED_AXIS_LABELS = "labels.txt";
    List<String> associatedAxisLabels = null;

    Handler mUIHandler = new Handler();
    private Executor executor = Executors.newSingleThreadExecutor();

    ImageProcessor imageProcessor;
    MappedByteBuffer tfliteModel;
    Interpreter tflite;

    Context context;

    ExecutorService mSingleThreadExecutor = Executors.newSingleThreadExecutor();

    private static ScreenshotterDatabase instance;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }


    public ExecutorService getmSingleThreadExecutor() {
        return mSingleThreadExecutor;
    }

    private ScreenshotterDatabase() {
    }

    public static ScreenshotterDatabase getInstance() {
        if (instance == null) instance = new ScreenshotterDatabase();
        return instance;
    }

    public interface ScreenshotterDatabaseListener {
        void onBitmapIsSuggestive();
        void onBitmapIsExplicit();
        void onBitmapAnalyzed();
        void bitmapAnalyzed(float neutral, float suggestive, float explicit);
    }


    public ScreenshotterDatabaseListener getmListener() {
        return mListener;
    }

    public void setmListener(ScreenshotterDatabaseListener mListener) {
        this.mListener = mListener;
    }

    private ScreenshotterDatabaseListener mListener;

    /**
     * Given a bitmap, return the scores based on analyzing one or more
     * subsections of the image.
     * @param bitmap The bitmap showing the user's screen.
     * @param width The width of the bitmap.
     * @param height The height of the bitmap.
     * @return The scores.
     */
    private List<Map<String, Float>> getScoreFromBitmap(Bitmap bitmap, float width, float height) throws IOException {
        // Rounded values
        int widthInt = Math.round(width);
        int heightInt = Math.round(height);
        // Bitmaps to analyze
        List<Bitmap> scaledBmps = new ArrayList<Bitmap>();
        boolean isPortrait = width <= height;
        if (Tools.getInstance().getUseWholeScreen()) {
            // Tile to analyze the whole screen
            if (isPortrait) {
                // Split into tiles vertically
                // Find the minimum tiles to cover the entire screen, and add one for good measure -
                // on most devices there will be 1 square on the top and bottom and 1 in the middle.
                int numTiles = ((int) Math.ceil(height / width)) + 1;
                // How many units to move for each tile
                float yEachTime = (height - width) / numTiles;
                for (int i = 0; i < numTiles; i++) {
                    // For each tile
                    float startY = i * yEachTime;
                    // Crop this section of screen
                    Bitmap cropped = Bitmap.createBitmap(bitmap, 0, (int) startY, widthInt, widthInt);
                    scaledBmps.add(Bitmap.createScaledBitmap(cropped, INPUT_SIZE, INPUT_SIZE, true));
                }
            } else {
                // Is landscape, split into tiles horizontally
                // Find the minimum tiles to cover the entire screen, and add one for good measure -
                // on most devices there will be 3 squares evenly distributed.
                int numTiles = ((int) Math.ceil(width / height)) + 1;
                // How many units to move for each tile
                float xEachTime = (width - height) / numTiles;
                for (int i = 0; i < numTiles; i++) {
                    // For each tile
                    float startX = i * xEachTime;
                    // Crop this section of screen
                    Bitmap cropped = Bitmap.createBitmap(bitmap, (int) startX, 0, heightInt, heightInt);
                    scaledBmps.add(Bitmap.createScaledBitmap(cropped, INPUT_SIZE, INPUT_SIZE, true));
                }
            }
        } else {
            // Just look at the center
            if (isPortrait) {
                // Find the vertical middle
                Bitmap cropped = Bitmap.createBitmap(bitmap, 0, (heightInt / 2) - (widthInt / 2), widthInt, widthInt);
                scaledBmps.add(Bitmap.createScaledBitmap(cropped, INPUT_SIZE, INPUT_SIZE, true));
            } else {
                // Find the horizontal middle
                Bitmap cropped = Bitmap.createBitmap(bitmap, (widthInt / 2) - (heightInt / 2), 0, heightInt, heightInt);
                scaledBmps.add(Bitmap.createScaledBitmap(cropped, INPUT_SIZE, INPUT_SIZE, true));
            }
        }
        // Load the labels
        associatedAxisLabels = FileUtil.loadLabels(getContext(), ASSOCIATED_AXIS_LABELS);
        // Now we have bitmaps, feed them to tf
        List<Map<String, Float>> results = new ArrayList<Map<String, Float>>(scaledBmps.size());
        for (Bitmap bmp : scaledBmps) {
            TensorImage tImage = new TensorImage(DataType.FLOAT32);
            tImage.load(bmp);
            tImage = imageProcessor.process(tImage);

            TensorBuffer probabilityBuffer = TensorBuffer.createFixedSize(new int[]{1, 3}, DataType.FLOAT32);

            tflite.run(tImage.getBuffer(), probabilityBuffer.getBuffer());
            TensorLabel labels = new TensorLabel(associatedAxisLabels,  probabilityBuffer );
            Map<String, Float> floatMap = labels.getMapWithFloatValue();
            results.add(floatMap);
        }
        return results;
    }

    public void onNewScreenshot(Bitmap bitmap, ScreenshotterDatabaseListener listener) {
        setmListener(listener);
        try {

            if(!canMakeScreenshot.get())return;

            // Compress the bitmap such that the smallest dimension of the screen fits the input size
             float bmWidth = (float) bitmap.getWidth();
             float bmHeight = (float) bitmap.getHeight();
             float scaleFactor;
             if (bmWidth > bmHeight) {
                 // Landscape, scale height to match
                 scaleFactor = INPUT_SIZE / bmHeight;
             } else {
                 // Portrait, scale width to match
                 scaleFactor = INPUT_SIZE / bmWidth;
             }
            int newHeight = Math.round(bmHeight * scaleFactor);
            int newWidth = Math.round(bmWidth * scaleFactor);
            bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);

            List<Map<String, Float>> scores = getScoreFromBitmap(bitmap, (float) newWidth, (float) newHeight);

            Log.d(getClass().getName(), "======================================================");
            for (Map<String, Float> imgScores : scores) {
                for (String key : imgScores.keySet()) {
                    Log.d(getClass().getName(), String.format("floatMap %s %.5f",key, imgScores.get(key) ));
                }
            }
            Log.d(getClass().getName(), "======================================================");

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            sendAnalyzedScreenshots(byteArray, scores);

            canMakeScreenshot.set(false);

            mUIHandler.postDelayed(() -> canMakeScreenshot.set(true), Tools.getInstance().getScreencastScreenshotInterval() * 1000);


            getmListener().onBitmapAnalyzed();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return The minimum confidence score value to trigger an alert for the user.
     */
    private float getUserMinScore() {
        String min = Tools.getInstance().getScreencastSensitivity();
        return new float[]{HIGH_CONFIDENCE_MIN, MEDIUM_CONFIDENCE_MIN, 0}[
            Arrays.asList(context.getResources().getStringArray(R.array.sensitivities_values)).indexOf(min)
        ];
    }

    /**
     * Given the neutral score and score of the predicted category, return the confidence.
     * @param neutral The score of the neutral category.
     * @param predicted The score of the predicted category.
     * @return (Name of the confidence as used by the API, raw confidence score).
     */
    private static Pair<String, Float> computeScoreConfidence(float neutral, float predicted) {
        // Neutral = 0 always means high confidence
        if (neutral == 0) {
            return new Pair("HIGH", Float.MAX_VALUE);
        }
        float score = predicted / neutral;
        if (score >= HIGH_CONFIDENCE_MIN) {
            return new Pair("HIGH", score);
        } else if (score >= MEDIUM_CONFIDENCE_MIN) {
            return new Pair("MEDIUM", score);
        } else {
            return new Pair("LOW", score);
        }
    }

    @SuppressLint("CheckResult")
    private void sendAnalyzedScreenshots(final byte[] byteArray, List<Map<String, Float>> allScores) {
        Date currentTime = Calendar.getInstance().getTime();
        final long timeStamp = currentTime.getTime();
        if (getRecordingLevel() == RECORDING_LEVEL_NONE) return;
            for (final Map<String, Float> floatMap : allScores) {
                EventModelScores scores = new EventModelScores();

                Log.d(getClass().getName(), floatMap.keySet().toString());

                for (String key : floatMap.keySet()) {

                    if (key.equals("neutral")) {
                        Log.d(getClass().getName(), "key " + key);
                        float value = floatMap.get(key) * 100f;
                        Log.d(getClass().getName(), String.format("value %.5f ", value));

                        scores.setNeutral(value);
                    }
                    if (key.equals("porn")) {
                        Log.d(getClass().getName(), "key " + key);
                        float value = floatMap.get(key) * 100f;
                        Log.d(getClass().getName(), String.format("value %.5f ", value));

                        scores.setExplicit(value);
                    }
                    if (key.equals("suggestive")) {
                        Log.d(getClass().getName(), "key " + key);
                        float value = floatMap.get(key) * 100f;
                        Log.d(getClass().getName(), String.format("value %.5f ", value));

                        scores.setSuggestive(value);
                    }

                }

                Log.d(getClass().getName(), "====================================");

                getmListener().bitmapAnalyzed(scores.getNeutral(),
                        scores.getSuggestive(),
                        scores.getExplicit());

                int currentModelType = EventModelTypesNEUTRAL;

                Log.d(getClass().getName(), "(int) scores.getNeutral() " + String.valueOf(scores.getNeutral()));

                // Compute the category and confidence
                Pair<String, Float> confidence = null;
                if (scores.getExplicit() > scores.getNeutral()) {
                    currentModelType = EventModelTypesEXPLICIT;
                    confidence = ScreenshotterDatabase.computeScoreConfidence(scores.getNeutral(), scores.getExplicit());
                } else if (scores.getSuggestive() > scores.getNeutral()) {
                    currentModelType = EventModelTypesSUGGESTIVE;
                    confidence = ScreenshotterDatabase.computeScoreConfidence(scores.getNeutral(), scores.getSuggestive());
                }


                /*====================================================================================*/
                /*====================================================================================*/
                /*====================================================================================*/
                if (currentModelType != EventModelTypesNEUTRAL && confidence.second >= ScreenshotterDatabase.this.getUserMinScore()) {

                    Log.d(getClass().getName(), "xxxUserConfidence " + String.valueOf(ScreenshotterDatabase.this.getUserMinScore()));

                    /*=========================================================*/
                    /*=========================================================*/
                    EventModel model = new EventModel();
                    model.setTimestamp(timeStamp);
                    //                Explicit: score - 78%, threshold - 40%"
                    /*=====================================================================================*/
                    if (currentModelType == EventModelTypesSUGGESTIVE) {
                        model.setEventModeltype(EventModelTypesSUGGESTIVE);

                        String result = String.format("Suggestive: score %s", String.valueOf((int) scores.getSuggestive()));

                        createNotification(result);

                        getmListener().onBitmapIsSuggestive();
                    }
                    if (currentModelType == EventModelTypesEXPLICIT) {
                        model.setEventModeltype(EventModelTypesEXPLICIT);

                        String result = String.format("Explicit: score %s", String.valueOf((int) scores.getExplicit()));

                        createNotification(result);

                        getmListener().onBitmapIsExplicit();
                    }
                    /*====================================================================================*/

                    String deviceAuthToken = Tools.getInstance().getDeviceAuthToken();
                    String currentCastId = Tools.getInstance().getCurrentScreencastId();

                    if (deviceAuthToken != null & currentCastId != null) {

                        long timestamp = System.currentTimeMillis();
                        RequestBody requestFile = RequestBody.create(MediaType.parse("image/png"), byteArray);

                        MultipartBody.Part imagePart = getRecordingLevel() == RECORDING_LEVEL_SCORES_SCREENS ?
                                MultipartBody.Part.createFormData("image", "image", requestFile) :
                                null;
                        SafeSurferNetworkApi.getInstance().getSRUseCase().addCastEvent(
                                deviceAuthToken,
                                currentCastId,
                                timestamp / 1000, // попросили присылать секунды
                                scores,
                                model.getEventModelTypeName(),
                                confidence.first,
                                imagePart
                        ).toObservable().subscribe(success -> {
                            Log.d(getClass().getName(), "addCastEvent success ");

                        }, Throwable::printStackTrace);
                    }
                }
            }
    }

    private void createNotification(String contentText) {

        if (!Tools.getInstance().isScreencastDetectionNotificationsActive()) return;

        NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Tools.getInstance().createNotificationChannel(notificationManager);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Tools.getInstance().getNotificationChannelId())
                .setSmallIcon(R.drawable.ic_stat_name)
                .setColor(Color.parseColor("#00A7D6"))
                .setContentTitle("Safe surfer")
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setVibrate(new long[]{0L})
                .setContentText(contentText);

        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    public void initTensorFlowAndLoadModel() {
        executor.execute(() -> {
            imageProcessor = new ImageProcessor.Builder()
                    .add(new ResizeOp(INPUT_SIZE, INPUT_SIZE, ResizeOp.ResizeMethod.BILINEAR))
                    .build();
            try {
                tfliteModel = FileUtil.loadMappedFile(getContext(), MODEL_PATH);
                tflite = new Interpreter(tfliteModel);
            } catch (IOException e) {
                Log.e("tfliteSupport", "Error reading model", e);
            }
        });
    }

    private int getRecordingLevel() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return Integer.parseInt(sharedPref.getString("recording_level", "1"));
    }
}
