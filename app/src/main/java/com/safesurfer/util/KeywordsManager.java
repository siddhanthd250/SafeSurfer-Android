package com.safesurfer.util;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class KeywordsManager {
    private static final String FILE_NAME = "custom_keywords.json";

    private static final List<String> KEYWORDS_SWEAR_WORDS = Arrays.asList(
            " ass ",
            "asshole",
            " arse ",
            "arsehole",
            " whore ",
            "asswipe",
            "fuck",
            "bastard",
            "bitch",
            " crap ",
            " shit ",
            "nigga",
            "nigger",
            " dick ",
            " cunt ",
            " fag ",
            "faggot",
            " dyke ",
            " wanker ",
            " piss ",
            " slut ",
            " twat ",
            " kys",
            " kill your self",
            " kill yourself"
    );
    private static final List<String> KEYWORDS_DRUGS = Arrays.asList(
            " acid ",
            " alcohol ",
            "alprazolam",
            "amphetamine",
            "angel dust",
            "baccy",
            "benzo",
            "diazepam",
            "bhang",
            "black mamba",
            " booze ",
            "cannabis",
            "fentanyl",
            "cigg",
            "cocaine",
            "codeine",
            " meth ",
            "diazepam",
            "ecstasy",
            "gabapentin",
            "hashish",
            "heroin",
            "hookah",
            "ketamine",
            "magic mushroom",
            " lsd ",
            "mephedrone",
            "marijuana",
            "mdma",
            "methamphetamine",
            "methadone",
            " molly ",
            "morphine",
            "opioid",
            "opiate",
            "opium",
            " pcp ",
            "peyote",
            " shisha ",
            "tobacco",
            "valium",
            "viagra",
            "xanax",
            "roofie",
            "weed",
            "drugs",
            " smoke",
            " pills "
    );
    private static final List<String> KEYWORDS_SELF_HARM = Arrays.asList(
            "overdose",
            "cutting",
            "suicide",
            "self harm",
            "kill my self",
            "kill myself",
            "slit wrist",
            "slit my wrist",
            "depressed",
            "depression",
            "suicidal"
    );
    private static final List<String> KEYWORDS_SEXTING = Arrays.asList(
            " anal ",
            "sex",
            "fist",
            " ass ",
            " butt ",
            "boob",
            "breast",
            "tits",
            "titty",
            "titti",
            "clit",
            "cum",
            "aroused",
            "areola",
            "dick",
            "fuck",
            "ejaculate",
            "asshole",
            "make out",
            "kiss",
            "suck",
            "blowjob",
            "blowie",
            " lick",
            " wank",
            "doggystyle",
            "doggiestyle",
            "doggy-style",
            "doggie-style",
            "doggy style",
            "missionary",
            "whore",
            "slut",
            "beat off",
            "handjob",
            "finger",
            "g spot",
            "g-spot",
            "pussy",
            "vagina",
            "thrust ",
            "boner",
            "erect",
            "hardon",
            "hard-on",
            "booty",
            "fondle",
            "buttplug",
            "butt plug",
            "dildo",
            "vibrator",
            "nipple",
            " cock",
            "condom",
            " lube",
            "deepthroat",
            "dominate",
            "bdsm",
            "dominant",
            "submissive",
            "bondage",
            "fellatio",
            "foreskin",
            "gangbang",
            "give head",
            "give you head",
            "jizz",
            "masturbate",
            "milf",
            "prostitute",
            "naked",
            " nude",
            "panties",
            "penetrate",
            "penetration",
            "seduce",
            "penis",
            "porn",
            "pubic",
            "semen",
            "girth",
            "spank",
            "sperm",
            "strip",
            "threesome",
            "threeway"
    );

    private Context context;

    private List<String> keywords = new ArrayList<>();

    private static final KeywordsManager ourInstance = new KeywordsManager();

    public ArrayList<String> contains(String txt) {
        if (TextUtils.isEmpty(txt)) {
            return null;
        }

        ArrayList<String> result = null;

        // Process the string for analysis. We will:
        // - Add a space at the start. This allows us to match words that stand on their own and are
        //   either in the middle or the start of their sentence.
        // - Lowercase to compare without case
        // - Remove punctuation and add a space at the end so we can match words that stand on their
        //   own and are in the middle or at the ending clause within a sentence.
        String lower = (" " + txt.toLowerCase() + " ").replaceAll("[\\!\\?\\.\\\";:']", " ");

        if (keywords != null && keywords.size() > 0) {
            for (String keyword : keywords) {
                if (lower.contains(keyword)) {
                    if (result == null) {
                        result = new ArrayList<String>();
                    }
                    result.add(keyword);
                }
            }
        }
        return result;
    }

    public static KeywordsManager getInstance() {
        return ourInstance;
    }

    public List<String> getCustomKeywords() {
        try {
            FileInputStream fileInputStream = context.openFileInput(FILE_NAME);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            Gson gson = new Gson();
            return gson.fromJson(inputStreamReader, new TypeToken<ArrayList<String>>(){}.getType());
        } catch (FileNotFoundException e) {
            return new ArrayList<>();
        }
    }

    private void saveCustomKeywords(List<String> newKeywords) {
        try (FileOutputStream fileOutputStream = context.openFileOutput(FILE_NAME, MODE_PRIVATE)) {
            fileOutputStream.write(new Gson().toJson(newKeywords).getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addKeyWord(final String word) {
        List<String> customKeywords = getCustomKeywords();
        customKeywords.add(0, word);
        saveCustomKeywords(customKeywords);
        this.init(context);
    }

    public void changeKeyWord(final int position, final String word) {
        List<String> customKeywords = getCustomKeywords();
        customKeywords.set(position, word);
        saveCustomKeywords(customKeywords);
        this.init(context);
    }

    public boolean keyWordExists(String word) {
        return keywords.contains(word);
    }

    public void removeKeyword(int position) {
        List<String> customKeywords = getCustomKeywords();
        customKeywords.remove(position);
        saveCustomKeywords(customKeywords);
        this.init(context);
    }

    public boolean isCacheEmpty() {
        return keywords.size() == 0;
    }

    public void init(Context ctx) {
        this.context = ctx;
        this.keywords = getCustomKeywords();
        // Add each of the predefined types, if enabled
        if (PreferencesAccessor.isChatGuardSwearWordsEnabled(ctx)) {
            this.keywords.addAll(KEYWORDS_SWEAR_WORDS);
        }
        if (PreferencesAccessor.isChatGuardDrugsEnabled(context)) {
            this.keywords.addAll(KEYWORDS_DRUGS);
        }
        if (PreferencesAccessor.isChatGuardSelfHarmEnabled(context)) {
            this.keywords.addAll(KEYWORDS_SELF_HARM);
        }
        if (PreferencesAccessor.isChatGuardSextingEnabled(context)) {
            this.keywords.addAll(KEYWORDS_SEXTING);
        }
    }
}
