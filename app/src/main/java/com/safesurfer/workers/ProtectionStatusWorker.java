package com.safesurfer.workers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.services.VirtualTunnelService;
import com.safesurfer.util.Tools;

import org.jetbrains.annotations.NotNull;

/**
 * Worker to update device protection status periodically.
 */
public class ProtectionStatusWorker extends Worker {
    public ProtectionStatusWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
    }

    @NotNull
    @Override
    public Result doWork() {
        try {
            SafeSurferNetworkApi.getInstance().getSRUseCase().putDeviceStatus(
                            Tools.getInstance().getDeviceAuthToken(),
                            VirtualTunnelService.isRunning()
            );
        } catch (Exception e) {
            e.printStackTrace();
            return Result.failure();
        }
        return Result.success();
    }
}
