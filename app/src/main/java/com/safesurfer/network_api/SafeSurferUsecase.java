package com.safesurfer.network_api;

import com.safesurfer.network_api.entities.AddNotificationRequestBody;
import com.safesurfer.network_api.entities.AddNotificationResponse;
import com.safesurfer.network_api.entities.DeviceAuthResponse;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.network_api.entities.GetCastEventsResponse;
import com.safesurfer.network_api.entities.GetCastsListResponse;
import com.safesurfer.network_api.entities.LogInResponse;
import com.safesurfer.network_api.entities.SetBlockedSitesRequestBody;
import com.safesurfer.network_api.entities.categories.CategoriesRules;
import com.safesurfer.tensorflow.EventModelScores;

import java.math.BigInteger;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.safesurfer.util.Tools.loadFileAsString;

public class SafeSurferUsecase {

    public SafeSurferRepo getmSafeSurferRepo() {
        return mSafeSurferRepo;
    }

    private SafeSurferRepo mSafeSurferRepo;

    public SafeSurferUsecase(SafeSurferRepo mSafeSurferRepo) {
        this.mSafeSurferRepo = mSafeSurferRepo;
    }

    public Single<Response<ResponseBody>> putDeviceStatus(String jwtToken, boolean isProtected) {
        return getmSafeSurferRepo().putDeviceStatus(jwtToken, isProtected)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> getTokenWithRoles(String jwtToken, String role) {
        return getmSafeSurferRepo().getTokenWithRoles(jwtToken, role)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> postEvasionAttempt(String jwtToken, String method, Long timestamp, String extra) {
        return getmSafeSurferRepo().postEvasionAttempt(jwtToken, method, timestamp, extra)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> postDeviceSignout(String jwtToken, String deviceID) {
        return getmSafeSurferRepo().postDeviceSignout(jwtToken, deviceID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> getDNSToken(String jwtToken, String deviceID) {
        return getmSafeSurferRepo().getDNSToken(jwtToken, deviceID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> getCategoriesRules(String jwtToken, String deviceID, String mac) {
        return getmSafeSurferRepo().getCategoriesRules(jwtToken, deviceID, mac)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> patchCategoriesRules(String jwtToken, String deviceID, String mac, CategoriesRules categoriesRules) {
        return getmSafeSurferRepo().patchCategoriesRules(jwtToken, deviceID, mac, categoriesRules)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> getDevices(String jwtToken) {
        return getmSafeSurferRepo().getDevices(jwtToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> registerSubscription(String jwtToken, String platform, String plan, String purchaseToken) {
        return getmSafeSurferRepo().registerSubscription(jwtToken, platform, plan, purchaseToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> createNewUser(String email, String password) {
        return getmSafeSurferRepo().createNewUser(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<LogInResponse>> logIn(String email, String password, boolean enableSub) {
        return getmSafeSurferRepo().logIn(email, password, enableSub)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> getScreencasts(String jwtToken, String page, String num_per_page, String device) {
        return getmSafeSurferRepo()
                .getScreencasts(jwtToken, page, num_per_page, device)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> updateDeviceIp(String jwtToken, String deviceIp) {
        return getmSafeSurferRepo().updateDeviceIp(jwtToken, deviceIp)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<ResponseBody>> registerDevice(String jwtToken, String type, String name) {
        return getmSafeSurferRepo().registerDevice(jwtToken, type, name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<DeviceAuthResponse>> deviceAuth(String jwtToken, String deviceID, String email, String password, boolean enableSub) {
        return getmSafeSurferRepo().deviceAuth(jwtToken, deviceID, email, password, enableSub)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<GetBlockedSitesResponse>> getBlockedSites(String jwtToken) {
        return getmSafeSurferRepo().getBlockedSites(jwtToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public Single<Response<String>> screencastStart(String jwtToken) {
        return getmSafeSurferRepo().screencastStart(jwtToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<String>> screencastStop(String jwtToken) {
        return getmSafeSurferRepo()
                .screencastStop(jwtToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<String>> addCastEvent(String jwtToken, String castId, long timestamp, EventModelScores scores, String category, String confidence, MultipartBody.Part image) {
        return getmSafeSurferRepo()
                .addCastEvent(jwtToken, castId, timestamp, scores, category, confidence, image)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<GetCastEventsResponse>> getCastEvents(String jwtToken, String castId, String page, String num_per_page) {
        return getmSafeSurferRepo()
                .getCastEvents(jwtToken, castId, page, num_per_page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<GetCastsListResponse>> getCastsList(String jwtToken, int page, int num_per_page, String device) {
        return getmSafeSurferRepo()
                .getCastsList(jwtToken, page, num_per_page, device)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<String>> setBlockedSitesWhiteList(GetBlockedSitesResponse blockedSitesResponse, String jwtToken, String url) {

        SetBlockedSitesRequestBody body = new SetBlockedSitesRequestBody();
        body.whitelist.addAll(blockedSitesResponse.whitelist);
        body.whitelist.add(url);

        body.blacklist.addAll(blockedSitesResponse.blacklist);
        body.deviceMac = getMacAddress();
        body.overrideEnabled = true;
        body.presetBlocked.addAll(blockedSitesResponse.presetBlocked);
        body.presetNotBlocked.add( BigInteger.valueOf(0) );
        body.youtube = blockedSitesResponse.youtube;

        return getmSafeSurferRepo().setBlockedSites(jwtToken, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<String>> setBlockedSitesBlackList(GetBlockedSitesResponse blockedSitesResponse, String jwtToken, String url) {
        SetBlockedSitesRequestBody body = new SetBlockedSitesRequestBody();

        body.blacklist.addAll(blockedSitesResponse.blacklist);
        body.blacklist.add(url);
        body.whitelist.addAll(blockedSitesResponse.whitelist);

        body.deviceMac = getMacAddress();
        body.overrideEnabled = true;
        body.presetBlocked.addAll(blockedSitesResponse.presetBlocked);
        body.presetNotBlocked.add( BigInteger.valueOf(0) );
        body.youtube = blockedSitesResponse.youtube;

        return getmSafeSurferRepo().setBlockedSites(jwtToken, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public Single<Response<AddNotificationResponse>> addNotification(String jwtToken, AddNotificationRequestBody body) {
        return getmSafeSurferRepo().addNotification(jwtToken, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Single<Response<Void>> deleteNotification(String jwtToken, String id) {
        return getmSafeSurferRepo().deleteNotification(jwtToken, id)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    static final String URL_PLAIN_DNS       = "plain.proto.safesurfer.io";
    static final String URL_PLAIN_DNSCrypt  = "dnscrypt.proto.safesurfer.io";
    static final String URL_PLAIN_DOH       = "doh.proto.safesurfer.io";
    static final String URL_PLAIN_DOT       = "dot.proto.safesurfer.io";

    public Single<Response<ResponseBody>> checkConnectionSec_PlainDNS() {
        return getmSafeSurferRepo().requestCheck( URL_PLAIN_DNS);
    }

    public Single<Response<ResponseBody>> checkConnectionSec_DNSCrypt() {
        return getmSafeSurferRepo().requestCheck( URL_PLAIN_DNSCrypt);
    }

    public Single<Response<ResponseBody>> checkConnectionSec_DOH() {
        return getmSafeSurferRepo().requestCheck( URL_PLAIN_DOH);
    }

    public Single<Response<ResponseBody>> checkConnectionSec_DOT() {
        return getmSafeSurferRepo().requestCheck( URL_PLAIN_DOT);
    }

    public Single<Response<ResponseBody>> refreshJWTToken(String jwtToken) {
        return getmSafeSurferRepo()
                .refreshJWTToken( jwtToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static String getMacAddress() {
        try {
            return loadFileAsString("/sys/class/net/eth0/address").toUpperCase().substring(0, 17);
        } catch (Exception e) {
            e.printStackTrace();
            return "02:00:00:00:00:00";
        }
    }
}
