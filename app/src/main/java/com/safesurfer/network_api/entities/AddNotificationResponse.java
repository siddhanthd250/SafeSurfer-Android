package com.safesurfer.network_api.entities;

import com.google.gson.annotations.SerializedName;

public class AddNotificationResponse {

    @SerializedName("spec")
    public long spec;

}
