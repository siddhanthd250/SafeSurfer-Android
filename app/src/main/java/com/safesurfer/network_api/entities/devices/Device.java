package com.safesurfer.network_api.entities.devices;

public class Device {
    public static final String TYPE_ANDROID = "android-device";
    public static final String TYPE_IOS = "ios-device";
    public static final String TYPE_WINDOWS = "windows-device";
    public static final String TYPE_DNS = "dns-device";
    public static final String TYPE_LIFEGUARD = "op-device";

    public String name;
    public long lastUpdated;
    public long registeredTime;
    public String ip;
    public String mac;
    public String id;
    public String type;
    public Device[] devices;
}
