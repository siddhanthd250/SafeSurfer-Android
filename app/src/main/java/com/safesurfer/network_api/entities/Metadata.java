package com.safesurfer.network_api.entities;

public class Metadata {
    public long timestamp;
    public int code;
    public String message;
}
