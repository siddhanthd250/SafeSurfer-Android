package com.safesurfer.network_api.entities;

import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;
import java.util.List;

public class GetBlockedSitesResponse {

    @SerializedName("blacklist")
    public List<String> blacklist;

    @SerializedName("overrideEnabled")
    public boolean overrideEnabled;

    @SerializedName("presetBlocked")
    public List<BigInteger> presetBlocked;

    @SerializedName("whitelist")
    public List<String> whitelist;

    @SerializedName("youtube")
    public String youtube;

}
