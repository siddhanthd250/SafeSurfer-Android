package com.safesurfer.network_api.entities.categories;

public class Category {
    public int id;
    public String displayName;
    public String icon;
    public int action;
    public boolean isSite;
    public boolean defaultAlert;
    public boolean frozen;
    public int parent;
    public String description;
}
