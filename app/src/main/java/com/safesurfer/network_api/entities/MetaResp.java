package com.safesurfer.network_api.entities;

public class MetaResp<T> {
    public Metadata meta;
    public T spec;
}
