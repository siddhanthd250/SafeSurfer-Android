package com.safesurfer.network_api.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddNotificationRequestBody {

    @SerializedName("appID")
    public String appID;

    @SerializedName("content")
    public String content;

    @SerializedName("keywords")
    public List<String> keywords;

    @SerializedName("timestamp")
    public long timestamp;

    @SerializedName("title")
    public String title;
}
