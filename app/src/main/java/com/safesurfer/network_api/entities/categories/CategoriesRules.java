package com.safesurfer.network_api.entities.categories;

public class CategoriesRules {
    public Category[] categories;
    public Restriction[] restrictions;
    public boolean hasRules;
    public String[] whitelist;
    public String[] blacklist;
}
