package com.safesurfer.network_api.entities;

import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;
import java.util.List;

public class GetCastEventsResponse {

    @SerializedName("available")
    BigInteger available;

    @SerializedName("events")
    List<Event> events;

    public static class Event {

        @SerializedName("category")
        String category;

        @SerializedName("confidence")
        String confidence;

        @SerializedName("deviceID")
        String deviceID;

        @SerializedName("id")
        BigInteger id;

        @SerializedName("image")
        boolean image;

        @SerializedName("screencastID")
        BigInteger screencastID;

        @SerializedName("timestamp")
        BigInteger timestamp;

    }

}
