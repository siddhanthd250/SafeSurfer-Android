package com.safesurfer.network_api.entities.categories;

public class Restriction {
    public int id;
    public String displayName;
    public String icon;
    public String[] actions;
    public int action;
}
