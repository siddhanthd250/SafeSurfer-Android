package com.safesurfer.network_api;

import com.google.gson.GsonBuilder;
import com.safesurfer.network_api.entities.AddNotificationRequestBody;
import com.safesurfer.network_api.entities.AddNotificationResponse;
import com.safesurfer.network_api.entities.DeviceAuthResponse;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.network_api.entities.GetCastEventsResponse;
import com.safesurfer.network_api.entities.GetCastsListResponse;
import com.safesurfer.network_api.entities.LogInResponse;
import com.safesurfer.network_api.entities.SetBlockedSitesRequestBody;
import com.safesurfer.network_api.entities.categories.CategoriesRules;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RealNetworkClient {

    public static final String BASE_URL = "https://api.safesurfer.io";
//    public static final String BASE_URL = "https://staging.api.safesurfer.io/";

    private HttpLoggingInterceptor loggingInterceptor;
    private OkHttpClient httpClient;
    private Retrofit mRetrofitInstance;

    ApiRequests.PutDeviceStatus putDeviceStatus;
    ApiRequests.GetTokenWithRoles getTokenWithRoles;
    ApiRequests.PostEvasionAttempt postEvasionAttempt;
    ApiRequests.PostDeviceSignout postDeviceSignout;
    ApiRequests.GetDNSToken getDNSToken;
    ApiRequests.GetCategoriesRules getCategoriesRules;
    ApiRequests.PatchCategoriesRules patchCategoriesRules;
    ApiRequests.GetDevices getDevices;
    ApiRequests.RegisterSubscription registerSubscription;
    ApiRequests.CreateNewUser createNewUser;
    ApiRequests.LogIn logIn;
    ApiRequests.DeviceAuth deviceAuth;
    ApiRequests.RegisterDevice registerDevice;
    ApiRequests.GetBlockedSites getBlockedSites;
    ApiRequests.ScreencastStart startScreencast;
    ApiRequests.ScreencastStop stopScreencast;
    ApiRequests.AddScreencastEvent addScreencastEvent;
    ApiRequests.GetScreencastEventsList getScreencastEvents;
    ApiRequests.GetCastsList getCastsList;
    ApiRequests.SetBlockedSites setBlockedSites;
    ApiRequests.UpdateDeviceIP updateDeviceIp;
    ApiRequests.GetScreencasts getScreencasts;
    ApiRequests.RefreshJWTToken refreshJWTToken;
    ApiRequests.AddNotification addNotification;
    ApiRequests.DeleteNotification deleteNotification;

    public RealNetworkClient(){
        initRetroFit();
    }

    private void initRetroFit() {

        loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(3, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        if (mRetrofitInstance == null) {
            mRetrofitInstance = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient)
                    .addCallAdapterFactory( RxJava2CallAdapterFactory.create() )
                    .addConverterFactory(createGsonConverter())
                    .build();
        }

        putDeviceStatus = mRetrofitInstance.create(ApiRequests.PutDeviceStatus.class);
        getTokenWithRoles = mRetrofitInstance.create(ApiRequests.GetTokenWithRoles.class);
        postEvasionAttempt = mRetrofitInstance.create(ApiRequests.PostEvasionAttempt.class);
        postDeviceSignout = mRetrofitInstance.create(ApiRequests.PostDeviceSignout.class);
        getDNSToken = mRetrofitInstance.create(ApiRequests.GetDNSToken.class);
        getCategoriesRules = mRetrofitInstance.create(ApiRequests.GetCategoriesRules.class);
        patchCategoriesRules = mRetrofitInstance.create(ApiRequests.PatchCategoriesRules.class);
        getDevices = mRetrofitInstance.create(ApiRequests.GetDevices.class);
        registerSubscription = mRetrofitInstance.create(ApiRequests.RegisterSubscription.class);
        createNewUser = mRetrofitInstance.create(ApiRequests.CreateNewUser.class);
        logIn = mRetrofitInstance.create(ApiRequests.LogIn.class);
        deviceAuth = mRetrofitInstance.create(ApiRequests.DeviceAuth.class);
        registerDevice = mRetrofitInstance.create(ApiRequests.RegisterDevice.class);

        getBlockedSites = mRetrofitInstance.create(ApiRequests.GetBlockedSites.class);
        startScreencast = mRetrofitInstance.create(ApiRequests.ScreencastStart.class);
        stopScreencast = mRetrofitInstance.create(ApiRequests.ScreencastStop.class);
        addScreencastEvent = mRetrofitInstance.create(ApiRequests.AddScreencastEvent.class);
        getScreencastEvents = mRetrofitInstance.create(ApiRequests.GetScreencastEventsList.class);
        getCastsList = mRetrofitInstance.create(ApiRequests.GetCastsList.class);

        setBlockedSites = mRetrofitInstance.create(ApiRequests.SetBlockedSites.class);
        updateDeviceIp = mRetrofitInstance.create(ApiRequests.UpdateDeviceIP.class);
        getScreencasts = mRetrofitInstance.create(ApiRequests.GetScreencasts.class);
        refreshJWTToken = mRetrofitInstance.create(ApiRequests.RefreshJWTToken.class);

        addNotification = mRetrofitInstance.create(ApiRequests.AddNotification.class);
        deleteNotification = mRetrofitInstance.create(ApiRequests.DeleteNotification.class);

//        checkRequest = mRetrofitInstance.create(ApiRequests.CheckRequest.class);
    }

    private Converter.Factory  createGsonConverter(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        // You can add multiple custom serialization adapters here
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    public Single<Response<ResponseBody>> putDeviceStatus(String jwtToken, boolean isProtected) {
        return putDeviceStatus.putDeviceStatus(jwtToken, isProtected);
    }

    public Single<Response<ResponseBody>> getTokenWithRoles(String jwtToken, String role) {
        return getTokenWithRoles.getTokenWithRoles(jwtToken, role);
    }

    public Single<Response<ResponseBody>> postEvasionAttempt(String jwtToken, String method, Long timestamp, String extra) {
        return postEvasionAttempt.postEvasionAttempt(jwtToken, method, timestamp, extra);
    }

    public Single<Response<ResponseBody>> postDeviceSignout(String jwtToken, String deviceID) {
        return postDeviceSignout.postDeviceSignout(jwtToken, deviceID);
    }

    public Single<Response<ResponseBody>> getDNSToken(String jwtToken, String deviceID) {
        return getDNSToken.getDNSToken(jwtToken, deviceID);
    }

    public Single<Response<ResponseBody>> getCategoriesRules(String jwtToken, String deviceID, String mac) {
        return getCategoriesRules.getCategoriesRules(jwtToken, deviceID, mac);
    }

    public Single<Response<ResponseBody>> patchCategoriesRules(String jwtToken, String deviceID, String mac, CategoriesRules categoriesRules) {
        return patchCategoriesRules.patchCategoriesRules(jwtToken, deviceID, mac, categoriesRules);
    }

    public Single<Response<ResponseBody>> getDevices(String jwtToken) {
        return getDevices.getDevices(jwtToken);
    }

    public Single<Response<ResponseBody>> registerSubscription(String jwtToken, String platform, String plan, String purchaseToken) {
        return registerSubscription.registerSubscription(jwtToken, platform, plan, purchaseToken);
    }

    public Single<Response<ResponseBody>> createNewUser(String email, String password) {
        return createNewUser.createNewUser(email,password);
    }

    public Single<Response<LogInResponse>> logIn(String email, String password, boolean enableSub) {
        return logIn.logIn(email,password,enableSub);
    }

    public Single<Response<ResponseBody>> registerDevice(String jwtToken, String type, String name) {
        return registerDevice.registerDevice(jwtToken, type,name);
    }

    public Single<Response<DeviceAuthResponse>> deviceAuth(String authToken, String deviceID, String email, String password, boolean enableSub ) {
        return deviceAuth.deviceAuth(authToken, deviceID, email, password, enableSub);
    }

    public Single<Response<GetBlockedSitesResponse>> getBlockedSites(String jwtToken) {
        return getBlockedSites.getBlockedSites( jwtToken);
    }

    public Single<Response<String>> screencastStart(String jwtToken) {
        return startScreencast.screencastStart(jwtToken);
    }

    public Single<Response<String>> screencastStop(String jwtToken) {
        return stopScreencast.screencastStop(jwtToken);
    }

    public Single<Response<String>> addCastEvent(String jwtToken, String castId, long timestamp, String levels, String category, String confidence, MultipartBody.Part image) {
        RequestBody requestBodyTimestamp = RequestBody.create( MediaType.parse("text/plain"), String.valueOf(timestamp) );
        RequestBody requestBodyLevels = RequestBody.create( MediaType.parse("text/plain"), String.valueOf(levels) );
        RequestBody requestBodyCategory = RequestBody.create( MediaType.parse("text/plain"), String.valueOf(category) );
        RequestBody requestBodyConfidence = RequestBody.create( MediaType.parse("text/plain"), String.valueOf(confidence) );
        return addScreencastEvent.addCastEvent(jwtToken, castId, requestBodyTimestamp, requestBodyLevels, requestBodyCategory, requestBodyConfidence, image);
    }

    public Single<Response<GetCastEventsResponse>> getCastEventsList(String jwtToken, String castId, String page, String num_per_page) {
        return getScreencastEvents.getCastEventsList(jwtToken, castId, page, num_per_page);
    }

    public Single<Response<GetCastsListResponse>> getCastsList(String jwtToken, int page, int num_per_page, String device) {
        return getCastsList.getCastsList(jwtToken,page,num_per_page,device);
    }

    public Single<Response<String>> setBlockedSites(String jwtToken, SetBlockedSitesRequestBody body) {
        return setBlockedSites.setBlockedSites(jwtToken,body);
    }

    public Single <Response<ResponseBody>> requestCheck(String url) {
        Retrofit mRetrofitDynamicBaseUrlInstance = new Retrofit.Builder()
                .baseUrl(String.format("https://%s",url))
                .client(httpClient)
                .addCallAdapterFactory( RxJava2CallAdapterFactory.create() )
                /*.addConverterFactory(createGsonConverter())*/
                .build();
        ApiRequests.CheckRequest checkRequest = mRetrofitDynamicBaseUrlInstance.create(ApiRequests.CheckRequest.class);

        return checkRequest.requestCheck();
    }

    public Single<Response<ResponseBody>> updateDeviceIp(String jwtToken, String deviceIp) {
        return updateDeviceIp.updateDeviceIp(jwtToken);
    }

    public Single<Response<ResponseBody>> getScreencasts(String jwtToken, String page, String num_per_page, String device) {
        return getScreencasts.getScreencasts(jwtToken, page, num_per_page, device);
    }

    public Single<Response<AddNotificationResponse>> addNotification(String jwtToken, AddNotificationRequestBody body) {
        return addNotification.addNotification(jwtToken, body);
    }

    public Single<Response<Void>> deleteNotification(String jwtToken, String id) {
        return deleteNotification.deleteNotification(jwtToken, id);
    }

    public Single<Response<ResponseBody>> refreshJWTToken(String jwtToken) {
        return refreshJWTToken.refreshJWTToken(jwtToken);
    }
}
