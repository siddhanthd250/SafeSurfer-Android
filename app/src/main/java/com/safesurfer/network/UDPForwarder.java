package com.safesurfer.network;

import android.util.Log;

import androidx.core.util.Pair;

import com.safesurfer.services.VirtualTunnelService;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.xbill.DNS.DClass;
import org.xbill.DNS.EDNSOption;
import org.xbill.DNS.GenericEDNSOption;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.OPTRecord;
import org.xbill.DNS.Record;
import org.xbill.DNS.Section;
import org.xbill.DNS.Type;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Adapted from PrivacyGuard source 12/08/2021.
 */
public class UDPForwarder extends AbsForwarder {
    private static final String TAG = UDPForwarder.class.getSimpleName();
    private static final boolean DEBUG = false;
    private final int LIMIT = 32767;
    private final int WAIT_BEFORE_RELEASE_PERIOD = 60000;
    //private InetAddress dstAddress;
    //private int dstPort;
    private DatagramSocket socket;
    private ByteBuffer packet;
    private DatagramPacket response;
    private UDPForwarderWorker worker;
    private Set<String> dnsServers;
    private boolean first = true;
    protected long releaseTime;
    private GenericEDNSOption uuidOption;
    private final L4Blocking.DNSTracker dnsTracker;

    public UDPForwarder(VirtualTunnelService vpnService, int port, Set<String> dnsServers, String uuid, L4Blocking.DNSTracker dnsTracker) throws DecoderException {
        super(vpnService, port);
        this.dnsTracker = dnsTracker;
        this.dnsServers = dnsServers;
        packet = ByteBuffer.allocate(LIMIT);
        response = new DatagramPacket(packet.array(), LIMIT);
        if (uuid != null) {
            this.uuidOption = new GenericEDNSOption(
                    65002,
                    Hex.decodeHex(uuid.replaceAll("\\-", "").toCharArray())
            );
        }
    }

    @Override
    public void forwardRequest(IPDatagram ipDatagram) {
        UDPDatagram udpDatagram = (UDPDatagram)ipDatagram.payLoad();
        boolean isDNSConnection = dnsServers.contains(ipDatagram.header().getDstAddress().getHostAddress());
        if (first) {
            setup(ipDatagram, isDNSConnection);
            first = false;
        }
        send(udpDatagram, ipDatagram.header().getDstAddress(), ipDatagram.payLoad().getDstPort(), isDNSConnection);
        releaseTime = System.currentTimeMillis() + WAIT_BEFORE_RELEASE_PERIOD;
    }

    @Override
    // Should never get called since we don't use LocalServer for UDP
    public void forwardResponse(byte[] response) {
    }

    public boolean setup(IPDatagram firstRequest, boolean isDNSConnection) {
        try {
            socket = new DatagramSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
        vpnService.protect(socket);
        worker = new UDPForwarderWorker(firstRequest, socket, this, isDNSConnection, dnsTracker);
        worker.start();
        return true;
    }

    public void send(IPPayLoad payLoad, InetAddress dstAddress, int dstPort, boolean isDNSConnection) {
        try {
            byte[] payloadData = payLoad.data();
            if (isDNSConnection) {
                payloadData = this.modifyDeviceDNSPacket(payloadData, dstAddress, dstPort);
            }
            socket.send(new DatagramPacket(payloadData, payloadData.length, dstAddress, dstPort));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param rawPacket The DNS packet.
     * @return Given a raw DNS packet, modify it if necessary and return the new raw DNS packet.
     *         If there are any issues parsing the packet, return an unmodified version.
     *         Also send a meta-message, unless the packet is already a meta-message.
     *         Register with the DNS tracker.
     */
    private byte[] modifyDeviceDNSPacket(byte[] rawPacket, InetAddress dstAddress, int dstPort) {
        if (this.uuidOption == null) {
            return rawPacket;
        }
        try {
            Message dnsMsg = new Message(rawPacket);
            if (dnsMsg.getQuestion() == null) {
                return rawPacket; // May be invalid
            }
            // If this is a meta-lookup already, ignore
            String qString = dnsMsg.getQuestion().getName().toString(true);
            if (qString.endsWith("safesurfer")) {
                return rawPacket;
            }
            // Add the uuid
            List<EDNSOption> existingENDSOptions = new ArrayList<>();
            if (dnsMsg.getOPT() != null && dnsMsg.getOPT().getOptions() != null) {
                existingENDSOptions = dnsMsg.getOPT().getOptions();
            }
            dnsMsg.removeRecord(dnsMsg.getOPT(), Section.ADDITIONAL);
            existingENDSOptions.add(this.uuidOption);
            OPTRecord optRecord = new OPTRecord(
                    0,
                    0,
                    0,
                    0,
                    existingENDSOptions
            );
            dnsMsg.addRecord(optRecord, Section.ADDITIONAL);
            // If super-safe mode is on then dnsTracker will be defined. In this case we need to:
            // - remap DNS IDs
            // - sent a meta-query
            if (dnsTracker != null) {
                Pair<Integer, Integer> ids = dnsTracker.genMessageIDs(dnsMsg);
                dnsMsg.getHeader().setID(ids.first);
                // Send the meta-lookup. Note that socket isn't protected, so it will be handled
                // by a new forwarder and ignored above.
                Message metaMsg = Message.newQuery(
                        Record.newRecord(
                                new Name(qString + ".explain.safesurfer."),
                                Type.TXT,
                                DClass.IN
                        )
                );
                metaMsg.getHeader().setID(ids.second);
                metaMsg.addRecord(optRecord, Section.ADDITIONAL);
                byte[] metaBytes = metaMsg.toWire();
                DatagramSocket metaSock = new DatagramSocket();
                metaSock.send(new DatagramPacket(metaBytes, metaBytes.length, dstAddress, dstPort));
                metaSock.close();
            }
            return dnsMsg.toWire();
        } catch (Exception | NoClassDefFoundError e) {
            // This may not be a DNS packet, ignore
        }
        return rawPacket;
    }

    @Override
    public void release() {
        if (worker != null) {
            worker.interrupt();
            try {
                worker.join();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        worker = null;
        if(socket != null && !socket.isClosed()) {
            socket.close();
        }
    }

    @Override
    public boolean hasExpired() { return releaseTime < System.currentTimeMillis();}
}
