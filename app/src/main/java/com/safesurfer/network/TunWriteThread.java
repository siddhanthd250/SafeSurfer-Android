package com.safesurfer.network;

import com.safesurfer.services.VirtualTunnelService;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Copyright (C) 2014  Yihang Song
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * Adapted from PrivacyGuard source 12/08/2021.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class TunWriteThread extends Thread {
    private final FileOutputStream localOut;
    private LinkedBlockingQueue<byte[]> writeQueue = new LinkedBlockingQueue<>();

    public TunWriteThread(FileDescriptor fd, VirtualTunnelService vpnService) {
        localOut = new FileOutputStream(fd);
    }

    public void run() {
        try {
            while (!isInterrupted()) {
                byte[] temp = writeQueue.take();
                try {
                    localOut.write(temp);
                } catch (Exception e) {
                    e.printStackTrace();
                    clean();
                    return;
                }
            }
            clean();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
            clean();
        }
    }

    public void write(byte[] data) {
        writeQueue.offer(data);
    }

    private void clean() {
        try {
            localOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        writeQueue.clear();
    }
}
