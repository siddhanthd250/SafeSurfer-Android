package com.safesurfer.network;

import java.net.InetAddress;
import java.util.Arrays;

/**
 * Copyright (C) 2014  Yihang Song
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * Adapted from PrivacyGuard source 12/08/2021.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public abstract class IPHeader extends AbsHeader {
    protected int headerLength, length;
    protected InetAddress srcAddress, dstAddress;
    protected byte protocol = 0;
    protected int srcIndex, dstIndex, addressSize;
    protected int lengthIndex;

    public static IPHeader create(byte[] data) {
        int version = (data[0] >> 4);
        if(version == 4) return new IPv4Header(data);
        else return new IPv6Header(data);
    }

    public int length() {
        return length;
    }

    public void setLength(int l) {
        length = l;
        data[lengthIndex] = (byte)(l >> 8);
        data[lengthIndex + 1] = (byte)(l % 256);
    }

    public byte protocol() {
        return protocol;
    }

    public InetAddress getSrcAddress() {
        return srcAddress;
    }
    public InetAddress getDstAddress() {
        return dstAddress;
    }

    public abstract byte[] getPseudoHeader(int dataLength);

    public byte[] getSrcAddressByteArray() {
        return Arrays.copyOfRange(data, srcIndex, srcIndex + addressSize);
    }

    public byte[] getDstAddressByteArray() {
        return Arrays.copyOfRange(data, dstIndex, dstIndex + addressSize);
    }

    @Override
    public IPHeader reverse() {
        byte[] reverseData = Arrays.copyOfRange(data, 0, data.length);
        ByteOperations.swap(reverseData, srcIndex, dstIndex, addressSize);
        return IPHeader.create(reverseData);
    }

    public void updateSrcAddress(InetAddress srcAddr) {
        this.srcAddress = srcAddr;
        System.arraycopy(srcAddr.getAddress(), 0, data, srcIndex, addressSize);
    }
}
