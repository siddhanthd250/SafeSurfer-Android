package com.safesurfer.network;

import com.safesurfer.services.VirtualTunnelService;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Copyright (C) 2014  Yihang Song
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * Adapted from PrivacyGuard source 12/08/2021.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class TCPForwarder extends AbsForwarder {
    private static final String TAG = TCPForwarder.class.getSimpleName();
    private static final boolean DEBUG = false;
    private final int WAIT_BEFORE_RELEASE_PERIOD_AFTER_CLOSE = 60000;
    private final int WAIT_BEFORE_RELEASE_PERIOD_IF_IDLE = 300000;
    private TCPForwarderWorker mWorker;

    public TCPForwarder(VirtualTunnelService vpnService, int port) {
        super(vpnService, port);
        this.mWorker = new TCPForwarderWorker(vpnService, port);
        this.mWorker.start();
    }

    @Override
    public void release() {
        this.mWorker.interrupt();
    }

    @Override
    public boolean hasExpired() {
        return mWorker.hasExpired();
    }

    @Override
    public void forwardRequest(IPDatagram ip) {
        this.mWorker.forwardRequests.offer(ip);
    }

    @Override
    public synchronized void forwardResponse(byte[] response) {
        if (mWorker.conn_info == null) return;
        mWorker.conn_info.increaseSeq(
                forwardResponse(mWorker.conn_info.getIPHeader(), new TCPDatagram(mWorker.conn_info.getTransHeader(0, TCPHeader.DATA), response, mWorker.conn_info.getDstAddress()))
        );
    }

    public void setup(InetAddress clientAddress, int clientPort, InetAddress serverAddress, int serverPort) {
        this.mWorker.setup(clientAddress, clientPort, serverAddress, serverPort);
    }

    public boolean isClosed() {
        return this.mWorker.closed;
    }

    public enum Status {
        DATA, LISTEN, SYN_ACK_SENT, HALF_CLOSE_BY_CLIENT, HALF_CLOSE_BY_SERVER, CLOSED
    }

    /**
     * A thread for this TCP forwarder that does the actual work. Calls to TCPForwarder
     * methods happen on the TunReadThread so make concurrent requests very slow
     * unless we do this.
     */
    private class TCPForwarderWorker extends Thread {
        protected Status status;
        protected boolean firstData = true;
        private TCPInputWorker worker;
        public TCPConnectionInfo conn_info;
        protected long releaseTimeAfterClose = System.currentTimeMillis() + WAIT_BEFORE_RELEASE_PERIOD_AFTER_CLOSE;
        protected long releaseTimeIfIdle = System.currentTimeMillis() + WAIT_BEFORE_RELEASE_PERIOD_AFTER_CLOSE;
        private boolean closed = true;
        private Socket mSocket;
        private int src_port;
        public LinkedBlockingQueue<IPDatagram> forwardRequests = new LinkedBlockingQueue<>();

        public TCPForwarderWorker(VirtualTunnelService vpnService, int port) {
            status = Status.LISTEN;
            closed = false;
        }

        @Override
        public void run() {
            while (!isInterrupted()) {
                try {
                    IPDatagram request = forwardRequests.take();
                    this.forwardRequest(request);
                } catch (InterruptedException e) {
                    this.close(false);
                    return;
                }
            }
            this.close(false);
        }

        public void release() { }

        private boolean handle_LISTEN(IPDatagram ipDatagram, byte flag, int len) {
            if (flag != TCPHeader.SYN) {
                close(true);
                return false;
            }
            conn_info.reset(ipDatagram);
            conn_info.setup(TCPForwarder.this);
            if (worker == null || !worker.isValid()) {
                close(false);
                return false;
            }
            TCPDatagram response = new TCPDatagram(conn_info.getTransHeader(len, TCPHeader.SYNACK), null, conn_info.getDstAddress());
            conn_info.increaseSeq(
                    TCPForwarder.this.forwardResponse(conn_info.getIPHeader(), response)
            );
            status = Status.SYN_ACK_SENT;
            return true;
        }

        /*
         * step 1 : reverse the IP header
         * step 2 : create a new TCP header, set the syn, ack right
         * step 3 : get the response if necessary
         * step 4 : combine the response and create a new tcp datagram
         * step 5 : update the datagram's checksum
         * step 6 : combine the tcp datagram and the ip datagram, update the ip header
         */

        private boolean handle_SYN_ACK_SENT(byte flag) {
            if(flag != TCPHeader.ACK) {
                // don't close since sometimes we get multiple duplicate SYNs
                return false;
            }
            status = Status.DATA;
            return true;
        }

        private boolean handle_DATA(IPDatagram ipDatagram, byte flag, int len, int rlen) {
            if(firstData){
                firstData = false;
            }else{
                assert ((flag & TCPHeader.ACK) == 0);
                if (((flag & TCPHeader.ACK) == 0) && ((flag & TCPHeader.RST) == 0)) {
                    return false;
                }
            }

            if(rlen > 0) { // send data
                send(ipDatagram.payLoad());
                conn_info.increaseSeq(
                        TCPForwarder.this.forwardResponse(conn_info.getIPHeader(), new TCPDatagram(conn_info.getTransHeader(len, TCPHeader.ACK), null, conn_info.getDstAddress()))
                );
            } else if(flag == TCPHeader.FINACK) { // FIN
                conn_info.increaseSeq(
                        TCPForwarder.this.forwardResponse(conn_info.getIPHeader(), new TCPDatagram(conn_info.getTransHeader(len, TCPHeader.ACK), null, conn_info.getDstAddress()))
                );
                conn_info.increaseSeq(
                        TCPForwarder.this.forwardResponse(conn_info.getIPHeader(), new TCPDatagram(conn_info.getTransHeader(0, TCPHeader.FINACK), null, conn_info.getDstAddress()))
                );
                close(false);
            } else if((flag & TCPHeader.RST) != 0) { // RST
                close(false);
            }
            // if none of the above hold, we have an empty ACK
            return true;
        }

        private boolean handle_HALF_CLOSE_BY_CLIENT(byte flag) {
            assert(flag == TCPHeader.ACK);
            if ((flag != TCPHeader.ACK)) {
//TODO: find out why this would happen
                return false;
            }
            status = Status.CLOSED;
            close(false);
            return true;
        }

        private boolean handle_HALF_CLOSE_BY_SERVER(byte flag, int len) {
            if(flag == TCPHeader.FINACK) {
                conn_info.increaseSeq(
                        TCPForwarder.this.forwardResponse(conn_info.getIPHeader(), new TCPDatagram(conn_info.getTransHeader(len, TCPHeader.ACK), null, conn_info.getDstAddress()))
                );
                status = Status.CLOSED;
                close(false);
            } // ELSE ACK for the finack sent by the server
            return true;
        }

        protected synchronized void handle_packet (IPDatagram ipDatagram) {
            if(closed) return;
            byte flag;
            int len, rlen;
            if(ipDatagram != null) {
                flag = ((TCPHeader)ipDatagram.payLoad().header()).getFlag();
                len = ipDatagram.payLoad().virtualLength();
                rlen = ipDatagram.payLoad().dataLength();
                if(conn_info == null) conn_info = new TCPConnectionInfo(ipDatagram);
            } else return;
            switch(status) {
                case LISTEN:
                    if(!handle_LISTEN(ipDatagram, flag, len)) return;
                    else break;
                case SYN_ACK_SENT:
                    if(!handle_SYN_ACK_SENT(flag)) return;
                    else break;
                case DATA:
                    if(!handle_DATA(ipDatagram, flag, len, rlen)) return;
                    else break;
                case HALF_CLOSE_BY_CLIENT:
                    if(!handle_HALF_CLOSE_BY_CLIENT(flag)) return;
                    else break;
                case HALF_CLOSE_BY_SERVER:
                    if(!handle_HALF_CLOSE_BY_SERVER(flag, len)) return;
                    else break;
                case CLOSED:
                    //status = Status.CLOSED;
                default:
                    break;
            }
        }

        /*
         *  methods for AbsForwarder
         */
        public boolean setup(InetAddress srcAddress, int src_port, InetAddress dstAddress, int dst_port) {
            try {
                //socketChannel = SocketChannel.open();
                //Socket socket = socketChannel.socket();
                mSocket = new Socket();
                mSocket.bind(null);
                vpnService.protect(mSocket);
                this.src_port = src_port;
                try {
                    //socketChannel.connect(new InetSocketAddress(LocalServer.port));
                    mSocket.connect(new InetSocketAddress(dstAddress, dst_port), 10000);
                    //while (!socketChannel.finishConnect()) ;
                } catch (SocketException e) {
                    this.close(true);
                    return false;
                }
                //socketChannel.configureBlocking(false);
                //selector = Selector.open();
                //socketChannel.register(selector, SelectionKey.OP_READ);
                worker = new TCPInputWorker(mSocket, TCPForwarder.this, src_port);
                worker.start();

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        //@Override
        //public void open() {
        //   if (!closed) return;
        //   //super.open();
        //  status = Status.LISTEN;
        //}

        //public void close() {
        //    close(false);
        //}

        public boolean isClosed() {
            return closed;
        }

        public void forwardRequest(IPDatagram ipDatagram) {
            handle_packet(ipDatagram);
            releaseTimeIfIdle = System.currentTimeMillis() + WAIT_BEFORE_RELEASE_PERIOD_IF_IDLE;
        }

        /*
         * Methods for ICommunication
         */
        public void send(IPPayLoad payLoad) {
            if(isClosed()) {
                status = Status.HALF_CLOSE_BY_SERVER;
                conn_info.increaseSeq(
                        TCPForwarder.this.forwardResponse(conn_info.getIPHeader(), new TCPDatagram(conn_info.getTransHeader(0, TCPHeader.FINACK), null, conn_info.getDstAddress()))
                );
            } else send(payLoad.data());
        }

        private void close(boolean sendRST) {
            closed = true;
            if(sendRST && conn_info != null) TCPForwarder.this.forwardResponse(conn_info.getIPHeader(), new TCPDatagram(conn_info.getTransHeader(0, TCPHeader.RST), null, conn_info.getDstAddress()));
            status = Status.CLOSED;
            try {
                if (mSocket != null) mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (worker != null) {
                worker.interrupt();
            }
            // don't release this forwarder right away since we may see more packets for this connection, which would then unnecessarily
            // re-create this forwarder
            releaseTimeAfterClose = System.currentTimeMillis() + WAIT_BEFORE_RELEASE_PERIOD_AFTER_CLOSE;
        }

        public boolean hasExpired() {
            long current = System.currentTimeMillis();

            if (closed) {
                if (releaseTimeAfterClose < current) return true;
            }
            else if (releaseTimeIfIdle < current) {
                close(true);
            }

            return false;
        }

        private void send(byte[] request) {
            try {
                mSocket.getOutputStream().write(request);
            } catch (SocketException e) {
                this.close(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
