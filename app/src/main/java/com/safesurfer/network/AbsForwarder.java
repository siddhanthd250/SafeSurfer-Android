package com.safesurfer.network;

import com.safesurfer.services.VirtualTunnelService;

import java.net.InetAddress;

/**
 * Copyright (C) 2014  Yihang Song
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * Adapted from PrivacyGuard source 12/08/2021.
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public abstract class AbsForwarder {
    private static final String TAG = AbsForwarder.class.getSimpleName();
    private static final boolean DEBUG = false;
    protected VirtualTunnelService vpnService;
    //protected boolean closed = true;
    protected int port;
    public AbsForwarder(VirtualTunnelService vpnService, int port) {
        this.vpnService = vpnService;
        this.port = port;
    }

    //public abstract boolean setup(InetAddress srcAddress, int srcPort, InetAddress dstAddress, int dstPort);

    //public void open() {
    //    closed = false;
    //}

    //public void close() {
    //closed = true;
    //}

    public abstract void release();

    public int getPort() { return port; }

    //public boolean isClosed() {
    //    return closed;
    //}

    public abstract boolean hasExpired();

    public abstract void forwardRequest(IPDatagram ip);

    public abstract void forwardResponse(byte[] response);

    public int forwardResponse(IPHeader ipHeader, IPPayLoad datagram) {
        if(ipHeader == null || datagram == null) return 0;
        datagram.update(ipHeader); // set the checksum
        IPDatagram newIpDatagram = new IPDatagram(ipHeader, datagram); // set the ip datagram, will update the length and the checksum
        vpnService.fetchResponse(newIpDatagram.toByteArray());
        return datagram.virtualLength();
    }
}
