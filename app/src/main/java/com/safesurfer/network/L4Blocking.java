package com.safesurfer.network;

import androidx.core.util.Pair;

import com.safesurfer.services.VirtualTunnelService;

import org.xbill.DNS.Message;
import org.xbill.DNS.RRset;
import org.xbill.DNS.Record;
import org.xbill.DNS.Section;
import org.xbill.DNS.Type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import java9.util.Lists;
import java9.util.stream.Collectors;
import java9.util.stream.StreamSupport;
import kotlin.Triple;

/**
 * Classes for applying blocking at the IP level. Even though DNS blocking takes effect immediately,
 * many social media apps will keep connections open even after being blocked, giving the effect
 * that they keep working until restarted. With this effect, the screentime features are basically
 * useless. The strategy here is to track DNS requests to tell when particular categories have
 * become blocked, then terminate any relevant and currently active forwarders. So technically we're
 * not blocking, just severing any existing connections to IP addresses with particular categories.
 */
public class L4Blocking {
    /**
     * Data class for storing a set of DNS messages we're tracking.
     */
    private static class DNSMessageSet {
        /**
         * The original DNS message ID sent by the device.
         */
        public int origID;
        /**
         * The response to the message, if any yet.
         */
        public Message dnsResponse;
        /**
         * The response to the meta-message, if any yet.
         */
        public Message metaDNSResponse;
    }

    /**
     * Data class describing an action to be taken with a DNS response.
     */
    public static class DNSResponseAction {
        /**
         * Whether the response should be routed back to the device.
         */
        public boolean returnToDevice;
        /**
         * The ID to set, if any.
         */
        public int origID = -1;

        public DNSResponseAction(boolean returnToDevice, int origID) {
            this.returnToDevice = returnToDevice;
            this.origID = origID;
        }
    }

    /**
     * Class for mapping from categories to IP addresses and telling when particular categories
     * have been blocked.
     */
    public static class DNSTracker {
        private static final int MAX_TRACKED_CATEGORIES = 2048;
        private static final int MAX_TRACKED_IPS = 128;
        /**
         * The max amount of in-flight DNS messages to track. This IS important for functionality
         * of the devices internet. Since we proxy DNS and set our own message IDs, we have to
         * track every message that we want to be routed back properly. If there are more in-flight
         * DNS packets than this, requests will start to time out.
         */
        private static final int MAX_TRACKED_DNS = 512;

        /**
         * Map from category ID to set of recent IPs, limited to MAX_TRACKED_CATEGORIES entries.
         */
        private final Map<Integer, Set<String>> categoryToIPs = new LinkedHashMap<Integer, Set<String>>(){
            @Override
            protected boolean removeEldestEntry(Map.Entry<Integer, Set<String>> eldest) {
                return size() > MAX_TRACKED_CATEGORIES;
            }
        };

        /**
         * For each DNS request we actually send two: a regular lookup and a meta-lookup.
         * The meta-lookup isn't forwarded back to the device, but it tells us what the DNS
         * actually did with the request so we can take appropriate action. These fields
         * track the DNS message IDs of the lookup/meta-lookup pair. Each is limited to
         * MAX_TRACKED_DNS entries.
         */
        private final Map<Integer, DNSMessageSet> inflightDNS = new LinkedHashMap<Integer, DNSMessageSet>(MAX_TRACKED_DNS){
            @Override
            protected boolean removeEldestEntry(Map.Entry<Integer, DNSMessageSet> eldest) {
                boolean remove = size() > MAX_TRACKED_DNS;
                if (remove && eldest.getValue().metaDNSResponse != null) {
                    // Also need to remove the corresponding meta-dns entry
                    inflightMetaDNS.remove(eldest.getValue().metaDNSResponse.getHeader().getID());
                }
                return remove;
            }
        };
        private final Map<Integer, DNSMessageSet> inflightMetaDNS = new LinkedHashMap<Integer, DNSMessageSet>(MAX_TRACKED_DNS){
            @Override
            protected boolean removeEldestEntry(Map.Entry<Integer, DNSMessageSet> eldest) {
                boolean remove = size() > MAX_TRACKED_DNS;
                if (remove && eldest.getValue().dnsResponse != null) {
                    // Also need to remove the corresponding dns entry
                    inflightDNS.remove(eldest.getValue().dnsResponse.getHeader().getID());
                }
                return remove;
            }
        };
        /**
         * Random number generator for message IDs.
         */
        private final Random random = new Random();
        /**
         * To operate on either of the above we must lock this.
         */
        private final Object inflightDNSLock = new Object();
        /**
         * The virtual tunnel service, so we can interact with other bits.
         */
        private final VirtualTunnelService vts;

        public DNSTracker(VirtualTunnelService vts) {
            this.vts = vts;
        }

        /**
         * Start tracking a DNS message. Safe for concurrent use.
         * @param msg The DNS message from the device. A new ID will be generated for the message.
         * @return A safe pair of IDs (message ID, meta-message ID) to use for each message.
         *         Further calls are expected to reference these IDs to handle the replies.
         */
        public Pair<Integer, Integer> genMessageIDs(Message msg) {
            int msgID = -1;
            int metaMsgID = -1;
            // Generate ID for each
            synchronized (inflightDNSLock) {
                do {
                    msgID = random.nextInt(65536);
                } while (inflightDNS.containsKey(msgID) || inflightMetaDNS.containsKey(msgID));
                do {
                    metaMsgID = random.nextInt(65536);
                } while (inflightDNS.containsKey(metaMsgID) || inflightMetaDNS.containsKey(metaMsgID));
                DNSMessageSet msgSet = new DNSMessageSet();
                msgSet.origID = msg.getHeader().getID();
                inflightDNS.put(msgID, msgSet);
                inflightMetaDNS.put(metaMsgID, msgSet);
            }
            return new Pair(msgID, metaMsgID);
        }

        /**
         * @return An empty IP set that has a max capacity of MAX_TRACKED_IPS.
         */
        private Set<String> getEmptyIPSet() {
            return Collections.newSetFromMap(new LinkedHashMap<String, Boolean>(){
                @Override
                protected boolean removeEldestEntry(Map.Entry<String, Boolean> eldest) {
                    return size() > MAX_TRACKED_IPS;
                }
            });
        }


        /**
         * Given the regular results of a lookup and a list of the lookup's categories, record
         * the returned IP addresses.
         * @param dnsResp The real (non-meta) DNS response.
         * @param categories The categories (as CSV ints) that apply to the lookup.
         */
        private void recordReturnedLookup(Message dnsResp, String categories) {
            // Parse the categories
            List<Integer> cats = StreamSupport.stream(Lists.of(categories.split(",")))
                    .map(catStr -> {
                        int cat = -1;
                        try {
                            cat = Integer.valueOf(catStr);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                        return cat;
                    })
                    .filter(cat -> cat != -1)
                    .collect(Collectors.toList());
            // Parse the addresses
            List<String> addresses = new ArrayList<>(1);
            for (RRset rrset : dnsResp.getSectionRRsets(Section.ANSWER)) {
                if (rrset.getType() != Type.A && rrset.getType() != Type.AAAA) {
                    continue; // Only record A or AAAA
                }
                for (Iterator it = rrset.rrs(); it.hasNext(); ) {
                    Record record = (Record) it.next();
                    addresses.add(record.rdataToString());
                }
            }
            // If we haven't recorded this category yet, do so
            for (Integer cat : cats) {
                if (!categoryToIPs.containsKey(cat)) {
                    categoryToIPs.put(cat, getEmptyIPSet());
                }
                categoryToIPs.get(cat).addAll(addresses);
            }
        }

        /**
         * Block any current connections for IPs with the category.
         * @param category The category ID as a string.
         */
        private void blockCategory(String category) {
            System.out.println("Blocking " + category);
            Integer cat = Integer.valueOf(category);
            Set<String> blockIPs = categoryToIPs.remove(cat);
            System.out.println(blockIPs);
            if (blockIPs == null) {
                // Didn't know of any IPs
                return;
            }
            // Block them
            for (String ip : blockIPs) {
                vts.getForwarderPools().denyIP(ip);
            }
        }

        /**
         * Take action to block/record IP addresses when both a response and meta-response are here.
         * @param msgSet The message set.
         */
        private void onMessageSetReturned(DNSMessageSet msgSet) {
            if (msgSet.metaDNSResponse == null || msgSet.dnsResponse == null) {
                return; // Can't do anything yet
            }
            // Check the results of the meta-response
            RRset[] metaRRs = msgSet.metaDNSResponse.getSectionRRsets(Section.ANSWER);
            String action = null; // 1 for block
            String categories = null; // CSV of category IDs
            String actionCategory = null; // The category that was blocked
            for (RRset rr : metaRRs) {
                if (rr.getType() != Type.TXT) {
                    continue; // Ignore non-TXT
                }
                for (Iterator it = rr.rrs(); it.hasNext(); ) {
                    Record record = (Record) it.next();
                    // Remove quotes and backslashes
                    String[] recordSplit = record
                            .rdataToString()
                            .replaceAll("[\"\\\\]", "")
                            .split(":");
                    switch (recordSplit[0]) {
                        case "Action": action = recordSplit[1].trim(); break;
                        case "Categories": categories = recordSplit[1].trim(); break;
                        case "Action category": actionCategory = recordSplit[1].trim(); break;
                    }
                }
            }
            System.out.println(action);
            System.out.println(categories);
            System.out.println(actionCategory);
            if (action != null && action.equals("1") && actionCategory != null) {
                // Is blocked
                this.blockCategory(actionCategory);
            } else if (action != null && action.equals("0")) {
                // Record the categories
                this.recordReturnedLookup(msgSet.dnsResponse, categories);
            }
            // Action is either null (something is wrong) or 2 which is a redirect, which we
            // don't do anything about.
        }

        /**
         * Track a DNS response, taking action to apply L4 blocking if needed.
         * @param msg The DNS message response.
         * @return What to do with the response.
         */
        public DNSResponseAction onDNSResponse(Message msg) {
            int id = msg.getHeader().getID();
            boolean wasMeta = false;
            DNSMessageSet msgSet;
            synchronized (inflightDNSLock) {
                if (inflightMetaDNS.containsKey(id)) {
                    // This is a meta-response
                    msgSet = inflightMetaDNS.get(id);
                    msgSet.metaDNSResponse = msg;
                    wasMeta = true;
                } else if (inflightDNS.containsKey(id)) {
                    // Is a regular response
                    msgSet = inflightDNS.get(id);
                    msgSet.dnsResponse = msg;
                } else {
                    // No-op. We must have exceeded the max inflight DNS requests, so there's
                    // no way to know what the original ID was. So we can't do anything.
                    return new DNSResponseAction(false, -1);
                }
                // If both responses are fulfilled, remove
                if (msgSet.dnsResponse != null && msgSet.metaDNSResponse != null) {
                    inflightMetaDNS.remove(msgSet.metaDNSResponse.getHeader().getID());
                    inflightDNS.remove(msgSet.dnsResponse.getHeader().getID());
                }
            }
            // Take action
            this.onMessageSetReturned(msgSet);
            return new DNSResponseAction(!wasMeta, msgSet.origID);
        }
    }
}
