package com.safesurfer.network;

import android.util.Pair;

import com.safesurfer.services.VirtualTunnelService;

import org.apache.commons.codec.DecoderException;
import org.sandrop.webscarab.plugin.spider.Link;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Copyright (C) 2014  Yihang Song
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * Adapted from PrivacyGuard source 12/08/2021.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class ForwarderPools {
    private static final int MAX_TRACKED_IPS = 128;

    private HashMap<Pair<Integer, Byte>, AbsForwarder> portToForwarder;
    private LinkedHashMap<String, AbsForwarder> ipToForwarder = new LinkedHashMap<String, AbsForwarder>(){
        @Override
        protected boolean removeEldestEntry(Map.Entry<String, AbsForwarder> eldest) {
            return size() > MAX_TRACKED_IPS;
        }
    };
    private VirtualTunnelService vpnService;
    private static final String TAG = ForwarderPools.class.getSimpleName();
    private static final boolean DEBUG = false;
    private Set<String> dnsServers;
    private String uuid;
    private final L4Blocking.DNSTracker dnsTracker;

    public ForwarderPools(VirtualTunnelService vpnService, Set<String> dnsServers, String uuid, L4Blocking.DNSTracker dnsTracker) {
        this.dnsTracker = dnsTracker;
        this.vpnService = vpnService;
        this.dnsServers = dnsServers;
        this.uuid = uuid;
        portToForwarder = new HashMap<>();
    }

    public AbsForwarder get(int port, byte protocol, String ip) {
        releaseExpiredForwarders();
        // Using only src port and protocol for key will fail if the same src port is
        // used for multiple connections to different hosts or dest ports, which is
        // legitimate for TCP. Does it actually happen on Android?
        Pair<Integer, Byte> key = new Pair<>(port, protocol);
        if (portToForwarder.containsKey(key)) { //&& !portToForwarder.get(key).isClosed()) {
            // if forwarder is closed, packet will be discarded; don't create a new forwarder
            // since packet is likely for old connection
            return portToForwarder.get(key);
        } else {
            AbsForwarder temp = getByProtocol(protocol, port);
            if (temp != null) {
                portToForwarder.put(key, temp);
                ipToForwarder.put(ip, temp);
            }
            return temp;
        }
    }

    /**
     * Release any currently active forwarders for the IP.
     * @param ip The IP to release forwarders for.
     */
    public void denyIP(String ip) {
        if (!ipToForwarder.containsKey(ip)) {
            return;
        }
        AbsForwarder forwarder = ipToForwarder.get(ip);
        if (forwarder instanceof TCPForwarder && !((TCPForwarder) forwarder).isClosed()) {
            System.out.println("Blocking " + ip);
            forwarder.release();
        }
    }

    void releaseExpiredForwarders() {
        Iterator it = portToForwarder.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            AbsForwarder fw = (AbsForwarder)pair.getValue();
            if (fw.hasExpired()) {
                fw.release();
                it.remove(); // avoids a ConcurrentModificationException
            }
        }
    }

    private AbsForwarder getByProtocol(byte protocol, int port) {
        switch (protocol) {
            case IPDatagram.TCP:
                return new TCPForwarder(vpnService, port);
            case IPDatagram.UDP:
                try {
                    return new UDPForwarder(vpnService, port, dnsServers, uuid, dnsTracker);
                } catch (DecoderException e) {
                    e.printStackTrace();
                }
            default:
                return null;
        }
    }

    public void release(UDPForwarder udpForwarder) {
        portToForwarder.values().removeAll(Collections.singleton(udpForwarder));
    }

    public void release(TCPForwarder tcpForwarder) {
        portToForwarder.values().removeAll(Collections.singleton(tcpForwarder));
    }
}
