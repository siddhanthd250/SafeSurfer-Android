package com.safesurfer.network;

import android.util.Pair;

import org.xbill.DNS.Message;
import org.xbill.DNS.RRset;
import org.xbill.DNS.Record;
import org.xbill.DNS.Section;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Adapted from PrivacyGuard source 12/08/2021.
 */
public class UDPForwarderWorker extends Thread {
    private static final String TAG = UDPForwarderWorker.class.getSimpleName();
    private static final boolean DEBUG = false;
    private final int LIMIT = 32767;
    private UDPForwarder forwarder;
    private IPHeader newIPHeader;
    private UDPHeader newUDPHeader;
    private int srcPort;
    private DatagramSocket socket;
    private boolean isDNSConnection;
    private final L4Blocking.DNSTracker dnsTracker;

    // ipDatagram is a request UDP packet that is going to be put into socket by forwarder
    // this thread will wait for responses to this request (or later request UDP packets sent from same source port)
    public UDPForwarderWorker(IPDatagram ipDatagram, DatagramSocket socket, UDPForwarder forwarder, boolean isDNSConnection, L4Blocking.DNSTracker dnsTracker) {
        this.dnsTracker = dnsTracker;
        this.socket = socket;
        this.forwarder = forwarder;
        this.isDNSConnection = isDNSConnection;
        this.newIPHeader = ipDatagram.header().reverse();
        UDPDatagram udpDatagram = (UDPDatagram)ipDatagram.payLoad();
        this.newUDPHeader = (UDPHeader)udpDatagram.header().reverse();
        // will have to update address/port of sender of response before we can create the UDP response
    }

    @Override
    public void interrupt(){
        super.interrupt();
        // closing the socket will interrupt the receive() operation
        if (socket != null) socket.close();
    }

    public void run() {
        ByteBuffer packet = ByteBuffer.allocate(LIMIT);
        DatagramPacket datagram = new DatagramPacket(packet.array(), LIMIT);

        try {
            while (!isInterrupted()) {
                packet.clear();
                socket.receive(datagram);

                byte[] received = Arrays.copyOfRange(datagram.getData(), 0, datagram.getLength());
                if (received != null) {
                    if (this.isDNSConnection) {
                        Pair<Boolean, byte[]> action = this.trackDNSReply(received);
                        if (!action.first) {
                            return;
                        }
                        received = action.second;
                    }
                    newIPHeader.updateSrcAddress(datagram.getAddress());
                    newUDPHeader.updateSrcPort(datagram.getPort());
                    UDPDatagram response = new UDPDatagram(newUDPHeader, received);
                    //response.debugInfo(dstAddress);
                    forwarder.forwardResponse(newIPHeader, response);
                }
            }
        } catch (SocketException e) {
            // receive() got interrupted
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }

    /**
     * Track the response to a DNS query, if it can and should be parsed.
     * @param rawPacket The raw DNS packet data.
     * @return Whether to send the reply to the device, and the reply to send if any.
     */
    private Pair<Boolean, byte[]> trackDNSReply(byte[] rawPacket) {
        // If lightweight mode is on, we don't need to do this.
        if (dnsTracker == null) {
            return new Pair(true, rawPacket);
        }
        try {
            Message dnsMsg = new Message(rawPacket);
            L4Blocking.DNSResponseAction action = dnsTracker.onDNSResponse(dnsMsg);
            if (!action.returnToDevice) {
                return new Pair(false, null);
            }
            List<Record> newRecords = new ArrayList<>();
            for (RRset rrset : dnsMsg.getSectionRRsets(Section.ANSWER)) {
                for (Iterator it = rrset.rrs(); it.hasNext(); ) {
                    Record record = (Record) it.next();
                    newRecords.add(Record.newRecord(record.getName(), record.getType(), record.getDClass(), record.getTTL() > 20 ? 20 : record.getTTL(), record.rdataToWireCanonical()));
                    dnsMsg.removeRecord(record, Section.ANSWER);
                }
            }
            for (Record record : newRecords) {
                dnsMsg.addRecord(record, Section.ANSWER);
            }
            dnsMsg.getHeader().setID(action.origID);
            return new Pair(true, dnsMsg.toWire());
        } catch (Exception | NoClassDefFoundError e) {
            // This may not be a DNS packet, ignore
        }
        return new Pair(true, rawPacket);
    }
}
