package com.safesurfer.network;

import com.safesurfer.services.VirtualTunnelService;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Copyright (C) 2014  Yihang Song
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * Adapted from PrivacyGuard source 12/08/2021.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class LocalServerForwarder extends Thread {
    private static final String TAG = LocalServerForwarder.class.getSimpleName();
    private static final boolean DEBUG = false;
    private static int LIMIT = 1368;

    private boolean outgoing = false;
    private VirtualTunnelService vpnService;
    private InputStream in;
    private OutputStream out;
    private ConnectionMetaData metaData;

    public LocalServerForwarder(Socket inSocket, Socket outSocket, boolean isOutgoing, VirtualTunnelService vpnService, String packageName, String appName) {
        try {
            this.in = inSocket.getInputStream();
            this.out = outSocket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.outgoing = isOutgoing;
        this.vpnService = vpnService;

        this.metaData = new ConnectionMetaData(packageName, appName, null, 0, null, 0, null, outgoing);

        metaData.destIP = outSocket.getInetAddress().getHostAddress();
        metaData.destPort = outSocket.getPort();
        metaData.destHostName = outSocket.getInetAddress().getCanonicalHostName();
        metaData.srcIP = inSocket.getInetAddress().getHostAddress();
        metaData.srcPort = inSocket.getPort();
        metaData.encrypted = (isOutgoing && metaData.destPort == 443) || (!isOutgoing && metaData.srcPort == 443);

        setDaemon(true);
    }

    public static void connect(Socket clientSocket, Socket serverSocket, VirtualTunnelService vpnService, String packageName, String appName) throws Exception {
        if (clientSocket != null && serverSocket != null && clientSocket.isConnected() && serverSocket.isConnected()) {

            clientSocket.setSoTimeout(0);
            serverSocket.setSoTimeout(0);
            LocalServerForwarder clientServer = new LocalServerForwarder(clientSocket, serverSocket, true, vpnService, packageName, appName);
            LocalServerForwarder serverClient = new LocalServerForwarder(serverSocket, clientSocket, false, vpnService, packageName, appName);
            clientServer.start();
            serverClient.start();

            while (clientServer.isAlive() && serverClient.isAlive()) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                }
            }
            clientSocket.close();
            serverSocket.close();
            clientServer.join();
            serverClient.join();

        } else {
            if (clientSocket != null && clientSocket.isConnected()) {
                clientSocket.close();
            }
            if (serverSocket != null && serverSocket.isConnected()) {
                serverSocket.close();
            }
        }
    }

    public void run() {
        try {
            byte[] buff = new byte[LIMIT];
            int got;
            while ((got = in.read(buff)) > -1) {
                out.write(buff, 0, got);
                out.flush();
            }
        } catch (Exception ignore) {
            ignore.printStackTrace();
            // can happen when app opens a connection and then terminates it right away so
            // this thread will start running only after a FIN has already been to the server
        }
    }
}
