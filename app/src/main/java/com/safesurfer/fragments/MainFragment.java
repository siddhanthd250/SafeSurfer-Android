package com.safesurfer.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.net.VpnService;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;

import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.safesurfer.R;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.services.VirtualTunnelService;
import com.safesurfer.util.DNSSpeedTest;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.SubscriptionUtils;
import com.safesurfer.util.Tools;
import com.safesurfer.util.Util;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import java9.util.function.Consumer;

import static android.app.Activity.RESULT_OK;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class MainFragment extends Fragment {
    private enum ProtectionState {
        VPN_RUNNING,
        VPN_NOT_RUNNING,
        PRIVATE_DNS_RUNNING_SS,
        PRIVATE_DNS_RUNNING_NON_SS
    }

    private static final List<String> ALL_SERVERS = Arrays.asList(
            "104.197.28.121",
            "104.155.237.225",
            "34.116.72.241"
    );

    private CardView mStartProtection;
    private CardView mStopProtection;
    private CardView mStopProtectionPrivateDNS;
    private ProtectionState protectionState;

    private ActionBar supportActionBar;
    private Consumer<Integer> setMenuItem;

    private RelativeLayout mProtectedLayout;
    private RelativeLayout mNotProtectedLayout;
    private RelativeLayout mPrivateDNSSSLayout;
    private RelativeLayout mPrivateDNSNonSSLayout;
    public boolean settingV6 = false;
    private View contentView;
    private ViewGroup streamViewGroup;
    private LinearLayout streamSpinnerContainer;
    private CircularProgressIndicator startProtectionLoading;
    private TextView startProtectionLabel;
    private Button enableAlwaysOnButton;
    private boolean startingVPN;
    private boolean bounded = false;
    ServiceConnection mSc;
    VirtualTunnelService mVPN;

    /**
     * Whether the subscription details have loaded.
     */
    private boolean subLoaded = false;

    public MainFragment(ActionBar supportActionBar, Consumer<Integer> setMenuItem) {
        super();
        this.supportActionBar = supportActionBar;
        this.setMenuItem = setMenuItem;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.fragment_main, container, false);
        setHasOptionsMenu(false);
        return contentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /** use bound service here because stopservice() doesn't immediately trigger onDestroy of VPN service */
        mSc = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mVPN = ((VirtualTunnelService.VirtualTunnelServiceBinder) service).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                MainFragment.this.checkStatusAndUpdate();
            }
        };
        settingV6 = !PreferencesAccessor.isIPv4Enabled(requireContext()) || (PreferencesAccessor.isIPv6Enabled(requireContext()) && settingV6);
        mProtectedLayout = (RelativeLayout) findViewById(R.id.protected_layout);
        mNotProtectedLayout = (RelativeLayout) findViewById(R.id.not_protected_layout);
        mPrivateDNSSSLayout = (RelativeLayout) findViewById(R.id.private_dns_ss_layout);
        mPrivateDNSNonSSLayout = (RelativeLayout) findViewById(R.id.private_dns_non_ss_layout);
        mStartProtection = (CardView) findViewById(R.id.start_protection);
        mStopProtection = (CardView) findViewById(R.id.stop_button);
        mStopProtectionPrivateDNS = (CardView) findViewById(R.id.stop_private_dns_button);
        streamSpinnerContainer = (LinearLayout) findViewById(R.id.bottom_stream_spinner);
        startProtectionLoading = (CircularProgressIndicator) findViewById(R.id.start_protection_loading);
        startProtectionLabel = (TextView) findViewById(R.id.protection_label);
        enableAlwaysOnButton = (Button) findViewById(R.id.enable_always_on);
        enableAlwaysOnButton.setOnClickListener(v ->
                new AlertDialog.Builder(requireContext())
                .setTitle(R.string.always_on_vpn_title)
                .setMessage(R.string.always_on_vpn_body)
                .setPositiveButton(R.string.battery_optimization_ok, (dialog, which) -> {
                    final Intent intent = new Intent(Settings.ACTION_VPN_SETTINGS);
                    if (intent.resolveActivity(requireContext().getPackageManager()) != null) {
                        try {
                            startActivity(intent);
                        } catch (Exception e) {
                            new AlertDialog.Builder(requireContext())
                                    .setTitle(R.string.no_always_on_vpn_title)
                                    .setMessage(R.string.no_always_on_vpn_body)
                                    .setPositiveButton(R.string.no_battery_optimization_ok, null)
                                    .show();
                        }
                    }
                })
                .setNegativeButton(R.string.battery_optimization_cancel, null)
                .show());
        mStartProtection.setOnClickListener(v -> {
            final Intent i = VpnService.prepare(requireContext());
            if (i != null) {
                startActivityForResult(i, 1111);
            } else {
                this.startVpn();
            }
        });
        mStopProtection.setOnClickListener(v -> {
            this.stopVpn();
        });
        mStopProtectionPrivateDNS.setOnClickListener(v -> {
            final Intent intent = new Intent(Settings.ACTION_SETTINGS);
            if (intent.resolveActivity(requireContext().getPackageManager()) != null) {
                try {
                    Toast.makeText(getContext(), R.string.fix_private_dns_toast,
                            Toast.LENGTH_LONG).show();
                    startActivity(intent);
                } catch (Exception e) {
                    new AlertDialog.Builder(requireContext())
                            .setTitle(R.string.no_fix_private_dns_title)
                            .setMessage(R.string.no_fix_private_dns_message)
                            .setPositiveButton(R.string.no_battery_optimization_ok, null)
                            .show();
                }
            }
        });
        if (!bounded) {
            Intent service = new Intent(getContext(), VirtualTunnelService.class);
            getActivity().bindService(service, mSc, Context.BIND_AUTO_CREATE);
            bounded = true;
        }
        // Load dynamic stream
        this.streamViewGroup = (ViewGroup) findViewById(R.id.bottom_stream);
    }

    /**
     * @return Whether we should show the always-on prompt.
     */
    private boolean shouldPromptAlwaysOn(boolean ignoreWhetherRunning) {
        return MainFragment.this.supportsAlwaysOnVPN() &&
                VpnService.prepare(requireContext()) == null && // Permission must already be granted
                (ignoreWhetherRunning || Util.isServiceRunning()) &&
                !MainFragment.this.isVPNAlwaysOn();
    }

    private View findViewById(@IdRes int id){
        return contentView.findViewById(id);
    }

    /**
     * Set the action bar and background for this fragment.
     */
    private void setupActivity() {
        getActivity().findViewById(R.id.drawer).setBackground(getResources().getDrawable(R.drawable.red_gradient));
        supportActionBar.setBackgroundDrawable(getResources().getDrawable(R.color.transparent));
        supportActionBar.setTitle(R.string.menu_home);
        this.setMenuItem.accept(R.id.menu_home);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setupActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.setupActivity();
        if (!startingVPN) {
            this.checkStatusAndUpdate();
        }
        if (shouldPromptAlwaysOn(false)) {
            this.enableAlwaysOnButton.setVisibility(View.VISIBLE);
        } else {
            this.enableAlwaysOnButton.setVisibility(View.GONE);
        }
        this.doInitialAsyncTasks();
    }

    /**
     * Check whether the VPN/private DNS is running and update the UI accordingly.
     */
    private void checkStatusAndUpdate() {
        // Check private DNS active
        Pair<Boolean, Boolean> privateDNSStatus = Tools.getInstance().getPrivateDNSStatus();
        // Update protection state
        if (privateDNSStatus.first) {
            if (privateDNSStatus.second) {
                protectionState = ProtectionState.PRIVATE_DNS_RUNNING_SS;
            } else {
                protectionState = ProtectionState.PRIVATE_DNS_RUNNING_NON_SS;
            }
        } else {
            if (Util.isServiceRunning()) {
                protectionState = ProtectionState.VPN_RUNNING;
            } else {
                protectionState = ProtectionState.VPN_NOT_RUNNING;
            }
        }
        this.updateUIState();
    }

    /**
     * Given the info on whether vpn/private DNS is running, update the UI.
     */
    private void updateUIState() {
        switch (this.protectionState) {
            case VPN_RUNNING:
                mProtectedLayout.setVisibility(View.VISIBLE);
                mNotProtectedLayout.setVisibility(View.GONE);
                mPrivateDNSNonSSLayout.setVisibility(View.GONE);
                mPrivateDNSSSLayout.setVisibility(View.GONE);
                getActivity().findViewById(R.id.drawer).setBackground(getResources().getDrawable(R.drawable.blue_gradient));
                break;
            case VPN_NOT_RUNNING:
                mProtectedLayout.setVisibility(View.GONE);
                mNotProtectedLayout.setVisibility(View.VISIBLE);
                mPrivateDNSNonSSLayout.setVisibility(View.GONE);
                mPrivateDNSSSLayout.setVisibility(View.GONE);
                getActivity().findViewById(R.id.drawer).setBackground(getResources().getDrawable(R.drawable.red_gradient));
                break;
            case PRIVATE_DNS_RUNNING_SS:
                mProtectedLayout.setVisibility(View.GONE);
                mNotProtectedLayout.setVisibility(View.GONE);
                mPrivateDNSNonSSLayout.setVisibility(View.GONE);
                mPrivateDNSSSLayout.setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.drawer).setBackground(getResources().getDrawable(R.drawable.blue_gradient));
                break;
            case PRIVATE_DNS_RUNNING_NON_SS:
                mProtectedLayout.setVisibility(View.GONE);
                mNotProtectedLayout.setVisibility(View.GONE);
                mPrivateDNSNonSSLayout.setVisibility(View.VISIBLE);
                mPrivateDNSSSLayout.setVisibility(View.GONE);
                getActivity().findViewById(R.id.drawer).setBackground(getResources().getDrawable(R.drawable.red_gradient));
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1111 && resultCode == RESULT_OK) {
            this.startVpn();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void startVpn() {
        Runnable start = () -> {
            getActivity().startService(new Intent(requireContext(), VirtualTunnelService.class));
            if (!bounded) {
                getActivity().bindService(new Intent(requireContext(), VirtualTunnelService.class), mSc, Context.BIND_AUTO_CREATE);
                bounded = true;
            }
            requireActivity().runOnUiThread(() -> {
                startProtectionLabel.setVisibility(View.VISIBLE);
                startProtectionLoading.setVisibility(View.GONE);
                protectionState = ProtectionState.VPN_RUNNING;
                startingVPN = false;
                if (this.shouldPromptAlwaysOn(true)) {
                    enableAlwaysOnButton.setVisibility(View.VISIBLE);
                }
                updateUIState();
            });
        };
        Runnable startAfterFetch = () -> {
            // We have not fetched a DNS token yet, do so now
            if (Tools.getInstance().getDeviceAuthToken() != null && PreferencesAccessor.getDNSUUID(requireContext()) == null) {
                SafeSurferNetworkApi.getInstance().getSRUseCase()
                        .getDNSToken(Tools.getInstance().getDeviceAuthToken(), Tools.getInstance().getDeviceId())
                        .subscribe(result -> {
                            if (result.code() == 200) {
                                PreferencesAccessor.setDNSUUID(requireContext(), result.body().string().replaceAll("^\"|\"$", ""));
                            }
                            start.run();
                        });
            } else {
                start.run();
            }
        };
        if (startingVPN) {
            return;
        }
        startingVPN = true;
        startProtectionLabel.setVisibility(View.GONE);
        startProtectionLoading.setVisibility(View.VISIBLE);
        // Find the fastest servers
        new Thread(new DNSSpeedTest(ALL_SERVERS, 4, 2, results -> {
            PreferencesAccessor.setDNS1(requireContext(), results.get(0));
            PreferencesAccessor.setDNS2(requireContext(), results.get(1));
            startAfterFetch.run();
        })).start();
    }

    private void stopVpn() {
        if (supportsAlwaysOnVPN() && this.isVPNAlwaysOn()) {
            // Can't stop always on VPN until always on turned off
            final Intent intent = new Intent(Settings.ACTION_VPN_SETTINGS);
            if (intent.resolveActivity(requireContext().getPackageManager()) != null) {
                Toast.makeText(getContext(), R.string.disable_always_on_toast,
                        Toast.LENGTH_LONG).show();
                startActivity(intent);
                return;
            }
        }
        mVPN.stopVPN();
        if (bounded) {
            getActivity().unbindService(mSc);
            bounded = false;
        }
        getActivity().stopService(new Intent(getContext(), VirtualTunnelService.class));
        protectionState = ProtectionState.VPN_NOT_RUNNING;
        updateUIState();
        populateStream();
    }

    /**
     * @return Whether the app has battery optimizations applied to it.
     */
    private boolean isBatteryOptimized() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return false;
        }
        final Context context = requireContext();
        final PowerManager pwrm = (PowerManager) context.getSystemService(context.POWER_SERVICE);
        return !pwrm.isIgnoringBatteryOptimizations(context.getPackageName());
    }

    private void addFinishSetupHeader() {
        TextView text = new TextView(requireContext());
        text.setText(R.string.finish_setup_header);
        text.setTextColor(ContextCompat.getColor(requireContext(), R.color.preference_primary_color));
        text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        text.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        text.setPaddingRelative(40, 80, 40, 40);
        text.setTypeface(ResourcesCompat.getFont(requireContext(), R.font.raleway_bold));
        this.streamViewGroup.addView(text);
    }

    /**
     * Add a button to the stream view to go to the settings and turn off battery optimization.
     */
    private void addFixPrivateDNSButton() {
        Button btn = new Button(requireContext());
        btn.setPaddingRelative(10, 10, 10, 10);
        String title = getResources().getString(R.string.fix_private_dns_button);
        String subTitle = getResources().getString(R.string.fix_private_dns_subtitle);
        int titleLength = title.length();
        int subtitleLength = subTitle.length();
        Spannable span = new SpannableString(title + "\n" + subTitle);
        span.setSpan(new RelativeSizeSpan(0.8f), titleLength, (titleLength + subtitleLength + 1), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        btn.setText(span);
        btn.setMinimumHeight(200);
        btn.setOnClickListener(v -> new AlertDialog.Builder(requireContext())
                .setTitle(R.string.fix_private_dns_button)
                .setMessage(R.string.fix_private_dns_message)
                .setPositiveButton(R.string.battery_optimization_ok, (dialog, which) -> {
                    final Intent intent = new Intent(Settings.ACTION_SETTINGS);
                    if (intent.resolveActivity(requireContext().getPackageManager()) != null) {
                        try {
                            Toast.makeText(getContext(), R.string.fix_private_dns_toast,
                                    Toast.LENGTH_LONG).show();
                            startActivity(intent);
                        } catch (Exception e) {
                            new AlertDialog.Builder(requireContext())
                                    .setTitle(R.string.no_fix_private_dns_title)
                                    .setMessage(R.string.no_fix_private_dns_message)
                                    .setPositiveButton(R.string.no_battery_optimization_ok, null)
                                    .show();
                        }
                    }
                })
                .setNegativeButton(R.string.battery_optimization_cancel, null)
                .show());
        this.streamViewGroup.addView(btn);
    }

    /**
     * Add a button to the stream view to go to the settings and turn off battery optimization.
     */
    private void addBatteryOptimizationButton() {
        Button btn = new Button(requireContext());
        btn.setPaddingRelative(10, 10, 10, 10);
        String title = getResources().getString(R.string.battery_optimization_button);
        String subTitle = getResources().getString(R.string.battery_optimization_button_subtitle);
        int titleLength = title.length();
        int subtitleLength = subTitle.length();
        Spannable span = new SpannableString(title + "\n" + subTitle);
        span.setSpan(new RelativeSizeSpan(0.8f), titleLength, (titleLength + subtitleLength + 1), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        btn.setText(span);
        btn.setMinimumHeight(200);
        btn.setOnClickListener(v -> new AlertDialog.Builder(requireContext())
                .setTitle(R.string.battery_optimization_title)
                .setMessage(R.string.battery_optimization_body)
                .setPositiveButton(R.string.battery_optimization_ok, (dialog, which) -> {
                    final Intent intent = Util.getBatteryOptimizationIntent(requireContext());
                    if (intent.resolveActivity(requireContext().getPackageManager()) != null) {
                        try {
                            startActivity(intent);
                        } catch (Exception e) {
                            new AlertDialog.Builder(requireContext())
                                    .setTitle(R.string.no_battery_optimization_title)
                                    .setMessage(R.string.no_battery_optimization_body)
                                    .setPositiveButton(R.string.no_battery_optimization_ok, null)
                                    .show();
                        }
                    }
                })
                .setNegativeButton(R.string.battery_optimization_cancel, null)
                .show());
        btn.setOnLongClickListener(v -> {
            new AlertDialog.Builder(MainFragment.this.requireContext())
                    .setTitle(R.string.stream_dismiss_title)
                    .setPositiveButton(R.string.stream_dismiss_ok, (dialog, which) -> {
                        PreferencesAccessor.setBatteryOptimizationDismissed(MainFragment.this.requireContext(), true);
                        MainFragment.this.populateStream();
                    })
                    .setNegativeButton(R.string.stream_dismiss_cancel, null)
                    .show();
            return true;
        });
        this.streamViewGroup.addView(btn);
    }

    /**
     * Add a button to the stream view to go to the settings and turn off battery optimization.
     */
    private void addDrawOnTopButton() {
        Button btn = new Button(requireContext());
        btn.setPaddingRelative(10, 10, 10, 10);
        String title = getResources().getString(R.string.draw_on_top_button);
        String subTitle = getResources().getString(R.string.battery_optimization_button_subtitle);
        int titleLength = title.length();
        int subtitleLength = subTitle.length();
        Spannable span = new SpannableString(title + "\n" + subTitle);
        span.setSpan(new RelativeSizeSpan(0.8f), titleLength, (titleLength + subtitleLength + 1), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        btn.setText(span);
        btn.setMinimumHeight(200);
        btn.setOnClickListener(v -> {
            Toast.makeText(getContext(), R.string.enable_overlay_toast_main_screen,
                    Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getContext().getPackageName()));
            requireActivity().startActivityForResult(intent, Tools.REQUEST_OVERLAY_PERMISSION);
        });
        btn.setOnLongClickListener(v -> {
            new AlertDialog.Builder(MainFragment.this.requireContext())
                    .setTitle(R.string.stream_dismiss_title)
                    .setPositiveButton(R.string.stream_dismiss_ok, (dialog, which) -> {
                        PreferencesAccessor.setDrawOnTopDismissed(MainFragment.this.requireContext(), true);
                        MainFragment.this.populateStream();
                    })
                    .setNegativeButton(R.string.stream_dismiss_cancel, null)
                    .show();
            return true;
        });
        this.streamViewGroup.addView(btn);
    }

    /**
     * Wait for the subscription details to load, then display the stream.
     */
    private void doInitialAsyncTasks() {
        this.subLoaded = false;
        this.populateStream();
        SubscriptionUtils.onInit(result -> {
            this.subLoaded = true;
            this.populateStream();
        });
    }

    /**
     * @return Whether the device supports always on VPN.
     */
    private boolean supportsAlwaysOnVPN() {
        return Build.VERSION.SDK_INT >= 24;
    }

    /**
     * @return Whether we need to prompt for ignoring battery optimization and drawing on top.
     *         This is only true for Android 6, before this starting background activities
     *         wasn't restricted, and after this we can prompt for always on VPN instead.
     */
    private boolean needsBatteryAndOverlayPermissions() {
        return Build.VERSION.SDK_INT == 23;
    }

    /**
     * @return Whether the VPN is in always-on mode.
     */
    private boolean isVPNAlwaysOn() {
        String alwaysOn = Settings.Secure.getString(getContext().getContentResolver(), "always_on_vpn_app");
        return Objects.equals(alwaysOn, "com.safesurfer");
    }

    /**
     * Clear the stream view group and populate it with the items that should be there.
     * Some items in here are loaded asynchronously - calling this method will place
     * a loading spinner in the stream if they are not loaded yet. Call again once loaded
     * to replace the spinner with real content.
     */
    private void populateStream() {
        if (!this.subLoaded) {
            // Not loaded yet
            this.streamSpinnerContainer.setVisibility(View.VISIBLE);
            this.streamViewGroup.setVisibility(View.INVISIBLE);
            return;
        }
        MainFragment.this.streamViewGroup.removeAllViews();
        int numContent = 0;
        // Finish setup section
        boolean showBatteryOptimization = false;
        boolean showDrawOnTop = false;
        boolean showFixPrivateDNS = false;
        if (protectionState == ProtectionState.PRIVATE_DNS_RUNNING_NON_SS) {
            showFixPrivateDNS = true;
            numContent++;
        }
        // Options for always-on VPN and extra permissions are mutually exclusive. We only need
        // one of each.
        if (protectionState != ProtectionState.PRIVATE_DNS_RUNNING_SS && !showFixPrivateDNS) {
            if (this.needsBatteryAndOverlayPermissions()) {
                if (MainFragment.this.isBatteryOptimized() && !PreferencesAccessor.isBatteryOptimizationDismissed(requireContext())) {
                    showBatteryOptimization = true;
                    numContent++;
                }
                if (!showBatteryOptimization && Tools.getInstance().needScreencastPermission() && !PreferencesAccessor.isDrawOnTopDismissed(requireContext())) {
                    showDrawOnTop = true;
                    numContent++;
                }
            }
        }
        if (showBatteryOptimization || showFixPrivateDNS || showDrawOnTop) {
            this.addFinishSetupHeader();
        }
        if (showFixPrivateDNS) {
            this.addFixPrivateDNSButton();
        }
        if (showBatteryOptimization) {
            this.addBatteryOptimizationButton();
        }
        if (showDrawOnTop) {
            this.addDrawOnTopButton();
        }
        this.streamSpinnerContainer.setVisibility(View.GONE);
        if (numContent == 0) {
            this.streamViewGroup.setVisibility(View.INVISIBLE);
        } else {
            this.streamViewGroup.setVisibility(View.VISIBLE);
        }
    }
}
