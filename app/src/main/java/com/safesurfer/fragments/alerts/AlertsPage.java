package com.safesurfer.fragments.alerts;

import com.safesurfer.R;
import com.safesurfer.fragments.PWAPage;

public class AlertsPage implements PWAPage {
    @Override
    public String getBaseURL() {
        return "https://my.safesurfer.io/alerts";
    }

    @Override
    public String getURLPattern() {
        return "^https://my.safesurfer.io/alerts.*";
    }

    @Override
    public String getTitle() {
        return "Alerts";
    }

    @Override
    public int getNavDrawerID() {
        return R.id.menu_alerts;
    }

    @Override
    public int getMenu() {
        return -1;
    }
}
