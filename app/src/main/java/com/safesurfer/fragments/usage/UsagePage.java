package com.safesurfer.fragments.usage;

import com.safesurfer.R;
import com.safesurfer.fragments.PWAPage;
import com.safesurfer.util.Tools;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class UsagePage implements PWAPage {
    @Override
    public String getBaseURL() {
        try {
            return "https://my.safesurfer.io/usage?device-id=" + URLEncoder.encode(Tools.getInstance().getDeviceId(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "https://my.safesurfer.io/usage";
        }
    }

    @Override
    public String getURLPattern() {
        return "^https://my.safesurfer.io/usage.*";
    }

    @Override
    public String getTitle() {
        return "Usage History";
    }

    @Override
    public int getNavDrawerID() {
        return R.id.menu_usage;
    }

    @Override
    public int getMenu() {
        return -1;
    }
}
