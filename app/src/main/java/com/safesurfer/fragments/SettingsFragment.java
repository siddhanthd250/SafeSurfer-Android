package com.safesurfer.fragments;

import android.Manifest;
import android.app.KeyguardManager;
import android.app.SearchManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.preference.EditTextPreference;
import androidx.preference.SeekBarPreference;
import androidx.preference.SwitchPreference;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.CheckBoxPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.appcompat.widget.SearchView;

import com.safesurfer.screens.EnablePrivateDNSActivity;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.screens.RevokeAccessActivity;
import com.safesurfer.services.VirtualTunnelService;

import android.os.IBinder;
import android.os.PowerManager;
import android.text.InputType;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.EditText;

import com.safesurfer.LogFactory;
import com.safesurfer.R;
import com.safesurfer.screens.AppSelectionActivity;
import com.safesurfer.screens.MainActivity;
import com.safesurfer.receivers.AdminReceiver;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.Tools;
import com.safesurfer.util.Util;
import com.safesurfer.util.Preferences;
import com.frostnerd.general.IntentUtil;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerActivity;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import java9.util.function.Consumer;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class SettingsFragment extends PreferenceFragmentCompat {
    private DevicePolicyManager devicePolicyManager;
    private ComponentName deviceAdmin;
    private static final int REQUEST_CODE_ENABLE_ADMIN = 1,
            REQUEST_EXCLUDE_APPS = 3, REQUEST_FINGERPRINT_PERMISSION = 4, REQUEST_ADVANCED_SETTINGS = 5;
    private final static String LOG_TAG = "[SettingsActivity]";
    public static final String ARGUMENT_SCROLL_TO_SETTING = "scroll_to_setting";
    private boolean pinDialogConfirmed = false;
    private String newPinEditValue = null;
    private ActionBar supportActionBar;
    private Consumer<Integer> setMenuItem;
    private boolean bounded = false;
    ServiceConnection mSc;
    VirtualTunnelService mVPN;

    public SettingsFragment(ActionBar actionBar, Consumer<Integer> setMenuItem) {
        super();
        this.supportActionBar = actionBar;
        this.setMenuItem = setMenuItem;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
        if (getArguments() != null && getArguments().containsKey(ARGUMENT_SCROLL_TO_SETTING)) {
            String key = getArguments().getString(ARGUMENT_SCROLL_TO_SETTING, null);
            if (key != null && !key.equals("")) {
                scrollToPreference(key);
            }
        }

        Log.d(getClass().getName(), "public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {}" );
    }

    /**
     * Set the action bar and background for this fragment.
     */
    private void setupActivity() {
        getActivity().findViewById(R.id.drawer).setBackground(getResources().getDrawable(R.color.off_white));
        supportActionBar.setBackgroundDrawable(getResources().getDrawable(R.color.colorPrimary));
        supportActionBar.setTitle(R.string.menu_settings);
        this.setMenuItem.accept(R.id.menu_settings);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setupActivity();
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Created Activity");
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Added preferences from resources");
        devicePolicyManager = (DevicePolicyManager) requireContext().getSystemService(Context.DEVICE_POLICY_SERVICE);
        deviceAdmin = new ComponentName(requireContext(), AdminReceiver.class);
        findPreference("setting_start_boot").setOnPreferenceChangeListener(changeListener);

        final Preferences preferences = Preferences.getInstance(requireContext());

        if (devicePolicyManager.isAdminActive(deviceAdmin))
            ((SwitchPreference) findPreference("device_admin")).setChecked(true);
        else {
            ((SwitchPreference) findPreference("device_admin")).setChecked(false);
            preferences.put("device_admin", false);
        }
        findPreference("device_admin").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                LogFactory.writeMessage(requireContext(), LOG_TAG, "Preference " + preference.getKey() + " was changed to " +
                        newValue + ", Type: " + Preferences.guessType(newValue));
                boolean value = (Boolean) newValue;
                if (value && !devicePolicyManager.isAdminActive(deviceAdmin)) {
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "User wants app to function as DeviceAdmin but access isn't granted yet. Showing dialog explaining Device Admin");
                    new AlertDialog.Builder(requireContext()).setTitle(R.string.information).setMessage(R.string.set_device_admin_info).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, deviceAdmin);
                            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                                    getString(R.string.device_admin_description));
                            LogFactory.writeMessage(requireContext(), LOG_TAG, "User clicked OK in dialog explaining DeviceAdmin. Going to settings", intent);
                            startActivityForResult(intent, REQUEST_CODE_ENABLE_ADMIN);
                            dialog.cancel();
                        }
                    }).setNegativeButton(R.string.cancel, (dialog, which) -> {
                        LogFactory.writeMessage(requireContext(), LOG_TAG, "User chose to cancel the dialog explaining DeviceAdmin");
                        dialog.cancel();
                    }).show();
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "Dialog is now being shown");
                    return false;
                } else if (!value) {
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "User disabled Admin access. Removing as Deviceadmin");
                    preferences.put("device_admin", false);
                    devicePolicyManager.removeActiveAdmin(deviceAdmin);
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "App was removed as DeviceAdmin");
                } else {
                    LogFactory.writeMessage(requireContext(), LOG_TAG, "User wants app to function as DeviceAdmin and Access was granted. Showing state as true.");
                    preferences.put("device_admin", true);
                }
                return true;
            }
        });
        findPreference("exclude_apps").setOnPreferenceClickListener(preference -> {
            Set<String> apps = preferences.getStringSet("excluded_apps");
            startActivityForResult(new Intent(requireContext(), AppSelectionActivity.class)
                    .putExtra("apps", Collections.list(Collections.enumeration(apps))).
                    putExtra("infoTextWhitelist", getString(R.string.excluded_apps_info_text_whitelist)).putExtra("infoTextBlacklist", getString(R.string.excluded_apps_info_text_blacklist))
                    .putExtra("whitelist", preferences.getBoolean("excluded_whitelist", false)).putExtra("onlyInternet", true)
                    .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true), REQUEST_EXCLUDE_APPS);
            return true;
        });
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            ((PreferenceCategory) findPreference("general_category")).removePreference(findPreference("exclude_apps"));
        if (preferences.getBoolean("excluded_whitelist", false)) {
            findPreference("excluded_whitelist").setSummary(R.string.excluded_apps_info_text_whitelist);
        } else {
            findPreference("excluded_whitelist").setTitle(R.string.blacklist);
        }
        findPreference("excluded_whitelist").setOnPreferenceChangeListener((preference, o) -> {
            boolean newValue = (boolean) o;
            preferences.put("app_whitelist_configured", true);
            preferences.put("excluded_whitelist", o);
            preference.setSummary(newValue ? R.string.excluded_apps_info_text_whitelist : R.string.excluded_apps_info_text_blacklist);
            preference.setTitle(newValue ? R.string.whitelist : R.string.blacklist);
            Set<String> selected = preferences.getStringSet("excluded_apps");
            Set<String> flipped = new HashSet<>();
            List<ApplicationInfo> packages = requireContext().getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
            for (ApplicationInfo packageInfo : packages) {
                if (selected.contains(packageInfo.packageName)) continue;
                flipped.add(packageInfo.packageName);
            }
            preferences.put("excluded_apps", flipped);
            return true;
        });

        findPreference("setting_pin_enabled").setOnPreferenceChangeListener((preference, newValue) -> {
            if ((boolean) newValue) {
                if (preferences.getString("pin_value", "1234").equals("1234")) {
                    this.pinDialogConfirmed = false;
                    AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                    builder.setTitle(R.string.set_pin);
                    final EditText input = new EditText(requireContext());
                    Runnable onConfirm = () -> {
                        String newPIN = input.getText().toString();
                        if (newPIN.length() == 0) {
                            return;
                        }
                        PreferencesAccessor.setPin(requireContext(), newPIN);
                        ((CheckBoxPreference) findPreference("setting_pin_enabled")).setChecked(true);
                        this.pinDialogConfirmed = true;
                    };
                    input.setInputType(InputType.TYPE_CLASS_NUMBER);
                    input.setOnKeyListener((view1, keyCode, keyEvent) -> {
                        if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                            onConfirm.run();
                            return true;
                        }
                        return false;
                    });
                    builder.setView(input);
                    builder.setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                        onConfirm.run();
                    });
                    builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                        PreferencesAccessor.setPinEnabled(requireContext(), false);
                        ((CheckBoxPreference) findPreference("setting_pin_enabled")).setChecked(false);
                    });
                    builder.setOnDismissListener(dialogInterface -> {
                        if (this.pinDialogConfirmed) {
                            return;
                        }
                        PreferencesAccessor.setPinEnabled(requireContext(), false);
                        ((CheckBoxPreference) findPreference("setting_pin_enabled")).setChecked(false);
                    });
                    builder.show();
                }
            }
            return true;
        });


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((MainActivity)requireContext(), new String[]{Manifest.permission.USE_FINGERPRINT}, REQUEST_FINGERPRINT_PERMISSION);
            }else{
                FingerprintManager fingerprintManager = (FingerprintManager) requireContext().getSystemService(Context.FINGERPRINT_SERVICE);
                KeyguardManager keyguardManager = requireContext().getSystemService(KeyguardManager.class);
                if (fingerprintManager != null && !fingerprintManager.isHardwareDetected()) {
                    ((PreferenceCategory)findPreference("pin_category")).removePreference(findPreference("pin_fingerprint"));
                }else if(fingerprintManager == null || keyguardManager == null || !fingerprintManager.hasEnrolledFingerprints() || !keyguardManager.isKeyguardSecure()){
                    findPreference("pin_fingerprint").setDependency("");
                    findPreference("pin_fingerprint").setEnabled(false);
                }
            }
        }else{
            ((PreferenceCategory)findPreference("pin_category")).removePreference(findPreference("pin_fingerprint"));
        }
        final EditTextPreference pinValue =  (EditTextPreference) findPreference("pin_value");

        findPreference("pin_value").setOnPreferenceChangeListener((preference, newValue) -> {
            if(newValue.toString().equals("")){
                ((CheckBoxPreference)findPreference("setting_pin_enabled")).setChecked(false);
            }else {
                preferences.put("pin_value",String.valueOf(newValue));
                newPinEditValue = String.valueOf(newValue);
                getPreferenceManager().showDialog(findPreference("pin_value"));
            }
            return false;
        });

        findPreference("pin_value").setOnPreferenceClickListener(preference -> {
            if (newPinEditValue != null) {
                pinValue.setText(newPinEditValue);
            }
            return false;
        });
        findPreference("setting_auto_mobile").setOnPreferenceChangeListener(autoSettingsChanged);
        findPreference("setting_auto_wifi").setOnPreferenceChangeListener(autoSettingsChanged);

        findPreference("screencast_use_blackout").setOnPreferenceChangeListener((preference, newValue) -> {
            Tools.getInstance().setUseBlackout( (Boolean) newValue );
            this.restartScreencastIfNecessary();
            getPreferenceScreen().findPreference("screencast_blackout_timeout").setEnabled((Boolean) newValue);
            return true;
        });

        findPreference("screencast_wholescreen").setOnPreferenceChangeListener((preference, newValue) -> {
            Tools.getInstance().setUseWholeScreen( (Boolean) newValue );
            this.restartScreencastIfNecessary();
            return true;
        });

        findPreference("screencast_debugmode").setOnPreferenceChangeListener((preference, newValue) -> {
            Tools.getInstance().setScreencastDebugMode( (Boolean) newValue );
            restartScreencastIfNecessary();
            return true;
        });

        boolean isscreencast_use_blackoutChecked = ((CheckBoxPreference)findPreference("screencast_use_blackout")).isChecked();
        getPreferenceScreen().findPreference("screencast_blackout_timeout").setEnabled( isscreencast_use_blackoutChecked);

        SeekBarPreference screencast_blackout_timeout = (SeekBarPreference)getPreferenceScreen().findPreference("screencast_blackout_timeout");
        screencast_blackout_timeout.setMin(3);
        screencast_blackout_timeout.setMax(20);

        screencast_blackout_timeout.setSummary(String.format(getString(R.string.screencast_blackout_timeout_scheme), String.valueOf( screencast_blackout_timeout.getValue() ) ));

        screencast_blackout_timeout.setOnPreferenceChangeListener((preference, newValue) -> {

            preference.setSummary(String.format( getString(R.string.screencast_blackout_timeout_scheme), String.valueOf( newValue) ));
            return true;
        });

        // Setup the slider for screenshot interval
        SeekBarPreference intervalSlider = (SeekBarPreference) getPreferenceScreen().findPreference("screencast_interval");
        intervalSlider.setMin(1);
        intervalSlider.setMax(5);
        intervalSlider.setSummary(String.format( getString(R.string.screencast_interval_format), String.valueOf(intervalSlider.getValue())));
        intervalSlider.setOnPreferenceChangeListener((preference, newValue) -> {
            preference.setSummary(String.format( getString(R.string.screencast_interval_format), String.valueOf(newValue) ));
            return true;
        });

        // Automation options only relevant for <= 6
        if (Build.VERSION.SDK_INT <= 23) {
            findPreference("automation").setVisible(true);
        }

        // Load the VPN service
        mSc = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mVPN = ((VirtualTunnelService.VirtualTunnelServiceBinder) service).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
            }
        };
        if (Util.isServiceRunning()) {
            Intent service = new Intent(requireContext(), VirtualTunnelService.class);
            requireActivity().bindService(service, mSc, Context.BIND_AUTO_CREATE);
            bounded = true;
        }
    }

    private void restartScreencastIfNecessary() {
        if( Tools.getInstance().isScreencastDetectionActive()){
            getActivity().stopService(new Intent(Tools.getInstance().getContext(), ScreenCapturerService.class));
            Tools.getInstance().cancelSheduleJob();
            // Wait until we stop running
            new Thread(() -> {
                boolean running = false;
                do {
                    running = Tools.getInstance().isScreencastDetectionActive();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        return;
                    }
                } while (running);
                Tools.getInstance().startScreencast(requireActivity(), () -> {});
            }).start();
        }
    }

    private void setupPrivateDNS() {
        Pair<Boolean, Boolean> privateDNSStatus = Tools.getInstance().getPrivateDNSStatus();
        if (privateDNSStatus.first || android.os.Build.VERSION.SDK_INT < 28) {
            findPreference("private_dns").setVisible(false);
        } else {
            findPreference("private_dns").setOnPreferenceClickListener(preference -> {
                Intent intent = new Intent(requireActivity(), EnablePrivateDNSActivity.class)
                        .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
                startActivity(intent);
                return true;
            });
        }
    }

    /**
     * Setup the button to revoke settings access.
     */
    private void setupSettingsAccess() {
        boolean accountAccessRevoked = PreferencesAccessor.getAccountAccessRevoked(requireContext());
        if (!accountAccessRevoked && Tools.getInstance().getDeviceAuthToken() != null) {
            findPreference("settings_access").setEnabled(true);
            findPreference("settings_access").setOnPreferenceClickListener(preference -> {
                Intent intent = new Intent(requireActivity(), RevokeAccessActivity.class)
                        .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
                startActivity(intent);
                return true;
            });
        } else {
            findPreference("settings_access").setEnabled(false);
        }
        if (accountAccessRevoked) {
            findPreference("private_dns").setEnabled(false);
            findPreference("automation").setEnabled(false);
            findPreference("pin_category").setEnabled(false);
            findPreference("vpn").setEnabled(false);
            findPreference("general_category").setEnabled(false);
            findPreference("machine_learning_model").setEnabled(false);
        }
    }

    private final Preference.OnPreferenceChangeListener autoSettingsChanged = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            Preferences.getInstance(requireContext()).put(preference.getKey(), newValue);
            boolean running = Util.isBackgroundConnectivityCheckRunning(requireContext());
            Preferences pref = Preferences.getInstance(requireContext());
            boolean run = pref.getBoolean("setting_auto_wifi", false) ||
                    pref.getBoolean("setting_auto_mobile", false) ||
                    pref.getBoolean("setting_disable_netchange", false);

            if(run && !running){
                Util.runBackgroundConnectivityCheck(requireContext(), false);
            } else if(!run && running) {
                Util.stopBackgroundConnectivityCheck(requireContext());
            }
            return true;
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_FINGERPRINT_PERMISSION){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                findPreference("pin_fingerprint").setDependency("setting_pin_enabled");
                findPreference("pin_fingerprint").setEnabled(((CheckBoxPreference)findPreference("setting_pin_enabled")).isChecked());
            }
        }
    }

    private final Preference.OnPreferenceChangeListener changeListener = (preference, newValue) -> {
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Preference " + preference.getKey() + " was changed to " +
                newValue + ", Type: " + Preferences.guessType(newValue));
        Preferences.getInstance(requireContext()).put(preference.getKey(), newValue);
        String key = preference.getKey();
        if (key.equals("setting_start_boot") && newValue.equals(true) && SettingsFragment.this.isBatteryOptimized()) {
            new AlertDialog.Builder(requireContext())
                    .setTitle(R.string.battery_optimization_title)
                    .setMessage(R.string.battery_optimization_body)
                    .setPositiveButton(R.string.battery_optimization_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final Intent intent = Util.getBatteryOptimizationIntent(requireContext());
                            if (intent.resolveActivity(requireContext().getPackageManager()) != null) {
                                try {
                                    startActivity(intent);
                                } catch (Exception e) {
                                    new AlertDialog.Builder(requireContext())
                                            .setTitle(R.string.no_battery_optimization_title)
                                            .setMessage(R.string.no_battery_optimization_body)
                                            .setPositiveButton(R.string.no_battery_optimization_ok, null)
                                            .show();
                                }
                            }
                        }
                    })
                    .setNegativeButton(R.string.battery_optimization_cancel, null)
                    .show();
        }
        return true;
    };

    /**
     * If the VPN service is running and bound, restart it.
     */
    private void restartVPNIfNecessary() {
        if (this.bounded) {
            this.mVPN.restartVPN();
        }
    }

    /**
     * @return Whether the app has battery optimizations applied to it.
     */
    private boolean isBatteryOptimized() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return false;
        }
        final Context context = requireContext();
        final PowerManager pwrm = (PowerManager) context.getSystemService(context.POWER_SERVICE);
        return !pwrm.isIgnoringBatteryOptimizations(context.getPackageName());
    }

    @Override
    public void onResume() {
        super.onResume();
        this.setupActivity();
        this.setupSettingsAccess();
        this.setupPrivateDNS();
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Resuming Activity");
        if(devicePolicyManager.isAdminActive(deviceAdmin)){
            ((SwitchPreference)findPreference("device_admin")).setChecked(true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Preferences preferences = Preferences.getInstance(requireContext());
        LogFactory.writeMessage(requireContext(), LOG_TAG, "Received onActivityResult", data);
        if (requestCode == REQUEST_CODE_ENABLE_ADMIN && resultCode == AppCompatActivity.RESULT_OK && devicePolicyManager.isAdminActive(deviceAdmin)){
            LogFactory.writeMessage(requireContext(), LOG_TAG, "Deviceadmin was activated");
            ((SwitchPreference)findPreference("device_admin")).setChecked(true);
        } else if(requestCode == REQUEST_EXCLUDE_APPS && resultCode == AppCompatActivity.RESULT_OK){
            ArrayList<String> apps = data.getStringArrayListExtra("apps");
            preferences.putStringSet("excluded_apps", new HashSet<>(apps));
            preferences.put("excluded_whitelist", data.getBooleanExtra("whitelist",false));
            this.restartVPNIfNecessary();
        } else if(requestCode == REQUEST_ADVANCED_SETTINGS && resultCode == AppCompatActivity.RESULT_FIRST_USER){
            IntentUtil.restartActivity(((MainActivity)requireContext()));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_settings, menu);
    }
}
