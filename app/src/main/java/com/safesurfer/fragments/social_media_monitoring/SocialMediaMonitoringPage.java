package com.safesurfer.fragments.social_media_monitoring;

import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.safesurfer.R;
import com.safesurfer.fragments.PWAPage;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.screens.chat_guard.KeywordsActivity;
import com.safesurfer.util.Tools;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class SocialMediaMonitoringPage implements PWAPage {
    @Override
    public String getBaseURL() {
        try {
            return "https://my.safesurfer.io/notification-log?device-id=" + URLEncoder.encode(Tools.getInstance().getDeviceId(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "https://my.safesurfer.io/notification-log";
        }
    }

    @Override
    public String getURLPattern() {
        return "^https://my.safesurfer.io/notification-log.*";
    }

    @Override
    public String getTitle() {
        return "Social Media Monitoring";
    }

    @Override
    public int getNavDrawerID() {
        return R.id.menu_chat_guard;
    }

    @Override
    public int getMenu() {
        return R.menu.menu_chat_guard;
    }

    @Override
    public boolean onMenuItem(Fragment fragment, int menuID) {
        if(menuID == R.id.menu_keywords){
            fragment.getActivity().startActivity(new Intent(fragment.requireContext(), KeywordsActivity.class)
                .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true));
            return true;
        }
        return false;
    }
}
