package com.safesurfer.fragments.social_media_monitoring;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;

import com.safesurfer.R;
import com.safesurfer.util.KeywordsManager;
import com.safesurfer.screens.MainActivity;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.screens.SSFragmentManager;
import com.safesurfer.screens.chat_guard.KeywordsActivity;
import com.safesurfer.screens.registration.RegisterActivity;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.SubscriptionUtils;
import com.safesurfer.util.Tools;
import com.safesurfer.util.Util;

import java9.util.function.Consumer;

public class SocialMediaMonitoringFragment extends Fragment {

    private View contentView;
    private View buttonPro;
    private View buttonSettings;
    private View buttonSignin;
    private TextView textView;

    private boolean wasBasic = false;
    private boolean wasSignedOut = false;
    private boolean switchToReal = false;

    private SSFragmentManager setFragment;

    private ActionBar supportActionBar;
    private Consumer<Integer> setMenuItem;

    public SocialMediaMonitoringFragment(ActionBar actionBar, Consumer<Integer> setMenuItem, SSFragmentManager setFragment) {
        this.supportActionBar = actionBar;
        this.setMenuItem = setMenuItem;
        this.setFragment = setFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setupActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (switchToReal && wasSignedOut && !wasBasic && Tools.getInstance().getDeviceAuthToken() != null) {
            this.setFragment.setFragment(MainActivity.SSFragment.CHAT_GUARD, false, false, new Bundle());
        } else if (switchToReal && wasBasic &&
                SubscriptionUtils.getSubscriptionBadgeMode(SubscriptionUtils.parseDeviceJWTClaims()) != SubscriptionUtils.SubscriptionBadgeMode.BASIC) {
            this.setFragment.setFragment(MainActivity.SSFragment.CHAT_GUARD, false, false, new Bundle());
        } else if (!wasBasic && Tools.getInstance().isNotificationServiceEnabled()) {
            this.setFragment.setFragment(MainActivity.SSFragment.CHAT_GUARD, false, false, new Bundle());
        }
        this.updateButtonState();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (wasBasic || wasSignedOut) {
            switchToReal = true;
        }
    }

    /**
     * Set the action bar and background for this fragment.
     */
    private void setupActivity() {
        getActivity().findViewById(R.id.drawer).setBackground(getResources().getDrawable(R.color.off_white));
        supportActionBar.setBackgroundDrawable(getResources().getDrawable(R.color.colorPrimary));
        supportActionBar.setTitle(R.string.menu_chat_guard);
        this.setMenuItem.accept(R.id.menu_chat_guard);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.fragment_chat_guard, container, false);
        KeywordsManager.getInstance().init(requireContext());
        buttonSettings = contentView.findViewById(R.id.button_settings);
        buttonSettings.setOnClickListener(view -> startActivity(new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS)));
        buttonPro = contentView.findViewById(R.id.button_pro);
        buttonPro.setOnClickListener(view -> {
            startActivity(Util.getProIntent(requireContext()));
        });
        buttonSignin = contentView.findViewById(R.id.button_signin);
        buttonSignin.setOnClickListener(v -> {
            Intent intent = new Intent(requireContext(), RegisterActivity.class)
                    .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true);
            startActivity(intent);
        });
        textView = contentView.findViewById(R.id.chat_guard_textview);
        this.updateButtonState();
        return contentView;
    }

    /**
     * Show either the signin, pro, or notification buttons.
     */
    private void updateButtonState() {
        wasSignedOut = Tools.getInstance().getDeviceAuthToken() == null;
        wasBasic = SubscriptionUtils.getSubscriptionBadgeMode(SubscriptionUtils.parseDeviceJWTClaims()) == SubscriptionUtils.SubscriptionBadgeMode.BASIC;
        if (wasSignedOut) {
            buttonSettings.setVisibility(View.GONE);
            buttonPro.setVisibility(View.GONE);
            buttonSignin.setVisibility(View.VISIBLE);
            textView.setText(R.string.chat_guard_sign_in);
        } else if (!Tools.getInstance().isNotificationServiceEnabled()) {
            buttonSettings.setVisibility(View.VISIBLE);
            buttonPro.setVisibility(View.GONE);
            buttonSignin.setVisibility(View.GONE);
            textView.setText(R.string.chat_guard_access_notifications);
        } else if (PreferencesAccessor.getAccountAccessRevoked(requireContext())) {
            buttonSettings.setVisibility(View.GONE);
            buttonPro.setVisibility(View.GONE);
            buttonSignin.setVisibility(View.GONE);
            textView.setText(R.string.chat_guard_running);
        } else if (wasBasic) {
            buttonSettings.setVisibility(View.GONE);
            buttonPro.setVisibility(View.VISIBLE);
            buttonSignin.setVisibility(View.GONE);
            textView.setText(R.string.chat_guard_get_pro);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_chat_guard, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_keywords){
            startActivity(new Intent(requireContext(), KeywordsActivity.class)
                .putExtra(PINProtectedActivity.INTENT_EXTRA_SKIP_PIN, true));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
