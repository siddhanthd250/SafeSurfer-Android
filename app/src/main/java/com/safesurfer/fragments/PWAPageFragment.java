package com.safesurfer.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.safesurfer.R;
import com.safesurfer.util.SubscriptionUtils;
import com.safesurfer.util.Tools;

import java.util.Set;

import java9.util.function.Consumer;

public class PWAPageFragment extends Fragment {

    private WebView webView;
    private LinearLayout loadingView;

    private final ActionBar supportActionBar;
    private final Consumer<Integer> setMenuItem;

    private PWAPage currPage;
    private Set<PWAPage> allPages;

    public PWAPageFragment(ActionBar supportActionBar, Consumer<Integer> setMenuItem) {
        super();
        this.supportActionBar = supportActionBar;
        this.setMenuItem = setMenuItem;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setupActivity();
    }

    /**
     * Set the action bar and background for this fragment.
     */
    private void setupActivity() {
        getActivity().findViewById(R.id.drawer).setBackground(getResources().getDrawable(R.color.off_white));
        supportActionBar.setBackgroundDrawable(getResources().getDrawable(R.color.colorPrimary));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        View contentView = inflater.inflate(R.layout.fragment_pwa_page, container, false);
        webView = contentView.findViewById(R.id.webview);
        loadingView = contentView.findViewById(R.id.loading_layout);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                String token = Tools.getInstance().getClearedDeviceAuthToken();
                setLocalStorage("my-ss-token", token);
                setLocalStorage("my-ss-immersive", "1");
                webView.setVisibility(View.INVISIBLE);
                loadingView.setVisibility(View.VISIBLE);
                // Find the page that this page references.
                for (PWAPage page : allPages) {
                    if (url.matches(page.getURLPattern())) {
                        PWAPageFragment.this.setCurrPage(page);
                        break;
                    }
                }
            }

            @Override
            public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
                for (PWAPage page : allPages) {
                    if (url.matches(page.getURLPattern())) {
                        PWAPageFragment.this.setCurrPage(page);
                        break;
                    }
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webView.setVisibility(View.VISIBLE);
                loadingView.setVisibility(View.INVISIBLE);
            }
        });
        if (currPage != null) {
            this.setCurrPage(currPage);
        }
        return contentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (currPage != null) {
            int menuID = currPage.getMenu();
            if (menuID == -1) {
                return;
            }
            inflater.inflate(menuID, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Set a local storage item.
     */
    protected void setLocalStorage(String key, String val) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            webView.evaluateJavascript("localStorage.setItem('"+ key +"','"+ val +"');", null);
        } else {
            webView.loadUrl("javascript:localStorage.setItem('"+ key +"','"+ val +"');");
        }
    }

    /**
     * Navigate back in the web view.
     * @return Whether we went back.
     */
    public boolean goBack() {
        if (webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (currPage != null) {
            this.currPage.onMenuItem(this, item.getItemId());
            return true;
        }
        return false;
    }

    public void setCurrPage(PWAPage currPage) {
        this.currPage = currPage;
        if (webView == null) {
            return;
        }
        if (webView.getUrl() == null || !webView.getUrl().matches(currPage.getURLPattern())) {
            this.webView.loadUrl(currPage.getBaseURL());
        }
        if (getActivity() != null) {
            setHasOptionsMenu(currPage.getMenu() != -1);
            getActivity().invalidateOptionsMenu();
            this.setupActivity();
        }
        this.setMenuItem.accept(currPage.getNavDrawerID());
        supportActionBar.setTitle(currPage.getTitle());
    }

    public void setAllPages(Set<PWAPage> allPages) {
        this.allPages = allPages;
    }
}
