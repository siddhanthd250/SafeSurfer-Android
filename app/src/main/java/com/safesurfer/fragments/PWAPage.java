package com.safesurfer.fragments;

import androidx.fragment.app.Fragment;

public interface PWAPage {
    /**
     * @return The initial URL to load for this page.
     */
    String getBaseURL();
    /**
     * @return A regex string of the URLs that should be considered as this page.
     */
    String getURLPattern();
    /**
     * @return The title to show on the navigation bar.
     */
    String getTitle();
    /**
     * @return The nav drawer item ID.
     */
    int getNavDrawerID();
    /**
     * @return The menu resource to put in the navigation bar, or -1 for none.
     */
    int getMenu();
    /**
     * Callback for when a menu item is clicked.
     * @return Whether action was taken.
     */
    default boolean onMenuItem(Fragment fragment, int menuID) {
        return false;
    }
}
