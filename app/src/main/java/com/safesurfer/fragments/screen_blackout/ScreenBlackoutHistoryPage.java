package com.safesurfer.fragments.screen_blackout;

import com.safesurfer.R;
import com.safesurfer.fragments.PWAPage;
import com.safesurfer.util.Tools;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ScreenBlackoutHistoryPage implements PWAPage {

    @Override
    public String getBaseURL() {
        try {
            return "https://my.safesurfer.io/screencasts?device=" + URLEncoder.encode(Tools.getInstance().getDeviceId(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "https://my.safesurfer.io/screencasts";
        }
    }

    @Override
    public String getURLPattern() {
        return "^https://my.safesurfer.io/screencasts.*";
    }

    @Override
    public String getTitle() {
        return "Screen Blackout History";
    }

    @Override
    public int getNavDrawerID() {
        return R.id.menu_screencast_detection;
    }

    @Override
    public int getMenu() {
        return -1;
    }
}
