package com.safesurfer.fragments.screen_blackout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.safesurfer.R;
import com.safesurfer.fragments.SettingsFragment;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.screens.MainActivity;
import com.safesurfer.screens.SSFragmentManager;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.SubscriptionUtils;
import com.safesurfer.util.Tools;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerService;

import java9.util.function.Consumer;

public class ScreenBlackoutFragment extends Fragment {

    private ActionBar supportActionBar;
    private Consumer<Integer> setMenuItem;
    private SSFragmentManager setFragment;

    private MaterialButton startStop;
    private MaterialButton history;

    private Boolean localStartedState;

    public ScreenBlackoutFragment(ActionBar supportActionBar, Consumer<Integer> setMenuItem, SSFragmentManager setFragment) {
        super();
        this.supportActionBar = supportActionBar;
        this.setMenuItem = setMenuItem;
        this.setFragment = setFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View contentView = inflater.inflate(R.layout.fragment_screen_blackout, container, false);
        this.startStop = contentView.findViewById(R.id.screen_blackout_start_stop);
        this.history = contentView.findViewById(R.id.screen_blackout_history);
        this.updateStartStopText();
        this.startStop.setOnClickListener(view -> {
            this.createScreencastDetectionDialog();
        });
        // Set whether history is available. If account access revoked, then no. Otherwise, depends
        // on subscription status.
        if (PreferencesAccessor.getAccountAccessRevoked(getContext())) {
            this.history.setEnabled(false);
        } else {
            SubscriptionUtils.onInit(result -> {
                this.history.setEnabled(SubscriptionUtils.areProFeaturesEnabled(SubscriptionUtils.parseDeviceJWTClaims()));
            });
        }
        this.history.setOnClickListener(view -> {
            setFragment.setFragment(MainActivity.SSFragment.SCREEN_BLACKOUT_HISTORY, true, false, null);
        });
        return contentView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.screen_blackout_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Bundle fragArgs = new Bundle();
        fragArgs.putCharSequence(SettingsFragment.ARGUMENT_SCROLL_TO_SETTING, "machine_learning_model");
        setFragment.setFragment(MainActivity.SSFragment.SETTINGS, true, false, fragArgs);
        return true;
    }

    /**
     * Update the text of the start/stop button.
     */
    private void updateStartStopText() {
        if (localStartedState != null) {
            if (localStartedState) {
                this.startStop.setText(R.string.screen_blackout_stop);
            } else {
                this.startStop.setText(R.string.screen_blackout_start);
            }
            return;
        }
        if (Tools.getInstance().isScreencastDetectionActive()) {
            this.startStop.setText(R.string.screen_blackout_stop);
        } else {
            this.startStop.setText(R.string.screen_blackout_start);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.setupActivity();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setupActivity();
    }

    /**
     * Set the action bar and background for this fragment.
     */
    private void setupActivity() {
        getActivity().findViewById(R.id.drawer).setBackground(getResources().getDrawable(R.color.off_white));
        supportActionBar.setBackgroundDrawable(getResources().getDrawable(R.color.colorPrimary));
        supportActionBar.setTitle(R.string.menu_screen_blackout);
        this.setMenuItem.accept(R.id.menu_screencast_detection);
    }

    private void stopScreencast() {
        stopService();
        localStartedState = false;
        updateStartStopText();
        String jwtToken = Tools.getInstance().getDeviceAuthToken();
        SafeSurferNetworkApi.getInstance().getSRUseCase()
                .screencastStop(jwtToken)
                .toObservable()
                .subscribe(success -> {
                    Tools.getInstance().setCurrentScreencastId(null);
                }, fail -> {
                    fail.printStackTrace();
                });
    }

    private void createScreencastDetectionDialog() {
        if (Tools.getInstance().isScreencastDetectionActive()) {
            stopScreencast();
        } else {
            Tools.getInstance().startScreencast(getActivity(), () -> {
                localStartedState = true;
                updateStartStopText();
            });
        }
    }

    private void stopService() {
        Tools.getInstance().setScreencastDetectionActive(false);
        requireContext().stopService(new Intent(Tools.getInstance().getContext(), ScreenCapturerService.class));
        Tools.getInstance().cancelSheduleJob();
    }
}
