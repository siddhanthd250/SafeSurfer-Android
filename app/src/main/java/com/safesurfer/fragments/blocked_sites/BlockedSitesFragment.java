package com.safesurfer.fragments.blocked_sites;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.safesurfer.R;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.network_api.entities.MetaResp;
import com.safesurfer.network_api.entities.categories.CategoriesRules;
import com.safesurfer.network_api.entities.categories.Category;
import com.safesurfer.network_api.entities.categories.Restriction;
import com.safesurfer.network_api.entities.devices.Device;
import com.safesurfer.network_api.entities.devices.Devices;
import com.safesurfer.screens.PINProtectedActivity;
import com.safesurfer.screens.subscription.SubscriptionActivity;
import com.safesurfer.util.SubscriptionUtils;
import com.safesurfer.util.Tools;
import com.safesurfer.util.Util;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import java9.util.function.Consumer;
import java9.util.stream.Collectors;
import java9.util.stream.StreamSupport;

public class BlockedSitesFragment extends Fragment {

    private static final String DOMAIN_NAME_PATTERN = "^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$";
    private static final Pattern DOMAIN_NAME_ONLY = Pattern.compile(DOMAIN_NAME_PATTERN);

    private final ActionBar supportActionBar;
    private final Consumer<Integer> setMenuItem;

    private LinearLayout loadingLayout;
    private LinearLayout loadedLayout;
    private LinearLayout failedLayout;
    private Spinner devicesSpinner;
    private MaterialButton saveButton;
    private MaterialButton retryButton;
    private LinearLayout bottomBar;

    private SitesCategoriesFragment sitesCategoriesFragment;
    private DomainListFragment blocklistFragment;
    private DomainListFragment allowlistFragment;

    private List<Device> devices;
    private CategoriesRules rules;
    private Map<Integer, List<Integer>> parentToChild; // Map from parent ID to list of child indices
    private String selectedDeviceID = Tools.getInstance().getDeviceId();
    private String selectedDeviceMAC = "";
    private boolean hasChanges = false; // If there are changes waiting to be saved, prompt before exiting
    private final boolean basicMode; // Used on the setup flow, changes some behavior

    public BlockedSitesFragment(ActionBar supportActionBar, Consumer<Integer> setMenuItem, boolean basicMode) {
        super();
        this.supportActionBar = supportActionBar;
        this.setMenuItem = setMenuItem;
        this.basicMode = basicMode;
    }

    public BlockedSitesFragment(ActionBar supportActionBar, Consumer<Integer> setMenuItem) {
        this(supportActionBar, setMenuItem, false);
    }

    public boolean getHasChanges() {
        return hasChanges;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View contentView = inflater.inflate(R.layout.fragment_blocked_sites, container, false);
        this.loadingLayout = contentView.findViewById(R.id.loading_layout);
        this.loadedLayout = contentView.findViewById(R.id.loaded_layout);
        this.failedLayout = contentView.findViewById(R.id.failed_layout);
        this.retryButton = contentView.findViewById(R.id.blocked_sites_retry);
        this.bottomBar = contentView.findViewById(R.id.bottom_bar);
        if (basicMode) {
            this.bottomBar.setVisibility(View.GONE);
        }
        this.retryButton.setOnClickListener(view -> {
            this.loadData();
        });
        TabLayout tabLayout = contentView.findViewById(R.id.blocked_site_tabs);
        if (basicMode) {
            tabLayout.setVisibility(View.GONE);
        }
        ViewPager2 pager = contentView.findViewById(R.id.pager);
        BlockedSitesFragmentStateAdapter pagerAdapter = new BlockedSitesFragmentStateAdapter(this, basicMode);
        pager.setAdapter(pagerAdapter);
        new TabLayoutMediator(tabLayout, pager, (tab, position) -> {
            switch (position) {
                case 0: tab.setText(R.string.blocked_sites_tab_sites_categories); return;
                case 1: tab.setText(R.string.blocked_sites_tab_blocklist); return;
                case 2: tab.setText(R.string.blocked_sites_tab_allowlist); return;
            }
        }).attach();
        pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                if (blocklistFragment != null) {
                    blocklistFragment.stopSelection();
                }
                super.onPageSelected(position);
            }
        });
        this.devicesSpinner = contentView.findViewById(R.id.devices_spinner);
        SubscriptionUtils.onInit(result -> {
            this.loadData();
        });
        return contentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.setupActivity();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setupActivity();
    }

    /**
     * Show the failed UI.
     */
    private void onFail(Throwable e) {
        if (e != null) {
            e.printStackTrace();
        }
        this.loadedLayout.setVisibility(View.GONE);
        this.loadingLayout.setVisibility(View.GONE);
        this.failedLayout.setVisibility(View.VISIBLE);
    }

    private void onFail() { this.onFail(null); }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (basicMode) {
            inflater.inflate(
                    (sitesCategoriesFragment != null && sitesCategoriesFragment.showDescriptions) ?
                            R.menu.blocked_sites_basic_menu_hide_descriptions :
                            R.menu.blocked_sites_basic_menu, menu);
        } else {
            inflater.inflate(
                    (sitesCategoriesFragment != null && sitesCategoriesFragment.showDescriptions) ?
                            R.menu.blocked_sites_menu_hide_descriptions :
                            R.menu.blocked_sites_menu, menu);
            saveButton = (MaterialButton) menu.findItem(R.id.blocked_sites_menu_save).getActionView();
            saveButton.setOnClickListener(view -> {
                this.save(this::loadData);
            });
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (failedLayout.getVisibility() == View.VISIBLE || loadingLayout.getVisibility() == View.VISIBLE) {
            return false;
        }
        switch (item.getItemId()) {
            case R.id.blocked_sites_menu_save:
                this.save(this::loadData);
                return true;
            case R.id.blocked_sites_menu_show_descriptions:
                sitesCategoriesFragment.setShowDescriptions(true);
                requireActivity().invalidateOptionsMenu();
                return true;
            case R.id.blocked_sites_menu_hide_descriptions:
                sitesCategoriesFragment.setShowDescriptions(false);
                requireActivity().invalidateOptionsMenu();
                return true;
            case R.id.blocked_sites_menu_reset:
                new AlertDialog.Builder(requireContext())
                        .setMessage(R.string.blocked_sites_reset_message)
                        .setNegativeButton(R.string.blocked_sites_leave_warning_cancel, null)
                        .setPositiveButton(R.string.blocked_sites_menu_reset, (dialogInterface, i) -> {
                            rules.hasRules = false;
                            save(this::loadData);
                        })
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Set the action bar and background for this fragment.
     */
    private void setupActivity() {
        getActivity().findViewById(R.id.drawer).setBackground(getResources().getDrawable(R.color.white));
        supportActionBar.setBackgroundDrawable(getResources().getDrawable(R.color.colorPrimary));
        if (basicMode) {
            supportActionBar.setTitle(R.string.setup_blocked_sites);
        } else {
            supportActionBar.setTitle(R.string.menu_blocked_sites);
        }
        this.setMenuItem.accept(R.id.menu_blocked_sites);
    }

    /**
     * Load the devices list and categories rules, showing the main layout when finished.
     */
    @SuppressLint({"CheckResult", "ClickableViewAccessibility"})
    private void loadData() {
        this.loadedLayout.setVisibility(View.GONE);
        this.failedLayout.setVisibility(View.GONE);
        this.loadingLayout.setVisibility(View.VISIBLE);
        String jwt = Tools.getInstance().getDeviceAuthToken();
        String thisDeviceID = Tools.getInstance().getDeviceId();
        hasChanges = false;
        SafeSurferNetworkApi.getInstance()
                .getSRUseCase()
                .getDevices(jwt)
                .zipWith(
                        SafeSurferNetworkApi.getInstance().getSRUseCase().getCategoriesRules(jwt, selectedDeviceID, selectedDeviceMAC),
                        (first, second) -> new Pair<>(first, second)
                ).
                subscribe(result -> {
                    if (result.first.code() != 200 || result.second.code() != 200) {
                        this.onFail();
                        return;
                    }
                    // Decode both
                    Gson gson = new Gson();
                    MetaResp<CategoriesRules> rulesResp = gson.fromJson(result.second.body().string(), new TypeToken<MetaResp<CategoriesRules>>(){}.getType());
                    // Reorder the categories so that parents are above their children, and store this
                    // info for later as we'll need it.
                    this.parentToChild = new HashMap<>();
                    for (int i = 0; i < rulesResp.spec.categories.length; i++) {
                        Category category = rulesResp.spec.categories[i];
                        if (category.parent > 0) {
                            if (!this.parentToChild.containsKey(category.parent)) {
                                this.parentToChild.put(category.parent, new ArrayList<>());
                            }
                            this.parentToChild.get(category.parent).add(i);
                        }
                    }
                    List<Category> newCategories = new ArrayList<>();
                    for (Category category : rulesResp.spec.categories) {
                        if (category.parent > 0) {
                            // Has parent - these are added out of order, ignore
                            continue;
                        }
                        newCategories.add(category);
                        if (this.parentToChild.containsKey(category.id)) {
                            List<Integer> newChildIndices = new ArrayList<>();
                            for (Integer childIdx : this.parentToChild.get(category.id)) {
                                newChildIndices.add(newCategories.size());
                                newCategories.add(rulesResp.spec.categories[childIdx]);
                            }
                            this.parentToChild.put(category.id, newChildIndices);
                        }
                    }
                    rulesResp.spec.categories = newCategories.toArray(new Category[]{});
                    // Also ensure that device specific rules is enabled, or our new selection
                    // will just be ignored.
                    rulesResp.spec.hasRules = true;
                    this.rules = rulesResp.spec;
                    // Replace null arrays with empty ones
                    if (rulesResp.spec.blacklist == null) {
                        rulesResp.spec.blacklist = new String[]{};
                    }
                    if (rulesResp.spec.whitelist == null) {
                        rulesResp.spec.whitelist = new String[]{};
                    }
                    if (sitesCategoriesFragment != null) {
                        sitesCategoriesFragment.rules = this.rules;
                        sitesCategoriesFragment.sitesCategoriesAdapter.notifyDataSetChanged();
                    }
                    if (blocklistFragment != null) {
                        blocklistFragment.rules = this.rules;
                        if (blocklistFragment.domainlistAdapter != null) {
                            blocklistFragment.domainlistAdapter.notifyDataSetChanged();
                        }
                    }
                    if (allowlistFragment != null) {
                        allowlistFragment.rules = this.rules;
                        if (allowlistFragment.domainlistAdapter != null) {
                            allowlistFragment.domainlistAdapter.notifyDataSetChanged();
                        }
                    }
                    // Flatten devices list
                    Devices nonFlatDevices;
                    nonFlatDevices = gson.fromJson(result.first.body().string(), Devices.class);
                    devices = new ArrayList<>();
                    // Add a pseudo-device representing the account-level rules
                    Device accountLevel = new Device();
                    accountLevel.id = "";
                    accountLevel.mac = "";
                    accountLevel.name = getResources().getString(R.string.account_level_selection);
                    devices.add(accountLevel);
                    // Add rest of devices
                    if (nonFlatDevices.dns != null) {
                        Collections.addAll(devices, nonFlatDevices.dns);
                    }
                    if (nonFlatDevices.lifeguards != null) {
                        for (Device device : nonFlatDevices.lifeguards) {
                            if (device.devices != null) {
                                for (Device innerDevice : device.devices) {
                                    innerDevice.name = device.name + "/" + innerDevice.name;
                                    devices.add(innerDevice);
                                }
                            }
                        }
                    }
                    if (nonFlatDevices.mobile != null) {
                        for (Device device : nonFlatDevices.mobile) {
                            devices.add(device);
                            if (device.id.equals(thisDeviceID)) {
                                device.name = device.name + " " + getResources().getString(R.string.this_device);
                            }
                        }
                    }
                    // Order devices so that newer devices are at the bottom
                    devices = StreamSupport.stream(devices)
                            .sorted((device1, device2) -> (int) (device1.registeredTime - device2.registeredTime))
                            .collect(Collectors.toList());
                    getActivity().runOnUiThread(() -> {
                        // Stop showing loading view
                        this.loadingLayout.setVisibility(View.GONE);
                        this.loadedLayout.setVisibility(View.VISIBLE);
                        // Fill spinner with choices, most recently registered first
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                requireContext(),
                                android.R.layout.simple_spinner_item,
                                StreamSupport
                                        .stream(devices)
                                        .map(device -> device.name)
                                        .collect(Collectors.toList())
                                        .toArray(new String[]{})
                        );
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        this.devicesSpinner.setAdapter(adapter);
                        int selectedDeviceIdx = 0;
                        for (int i = 0; i < devices.size(); i++) {
                            Device device = devices.get(i);
                            if (device.id.equals(selectedDeviceID) && device.mac.equals(selectedDeviceMAC)) {
                                selectedDeviceIdx = i;
                                break;
                            }
                        }
                        this.devicesSpinner.setSelection(selectedDeviceIdx);
                        if (SubscriptionUtils.areProFeaturesEnabled(SubscriptionUtils.parseDeviceJWTClaims())) {
                            this.devicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    final Device selected = BlockedSitesFragment.this.devices.get(i);
                                    if (selected.id.equals(BlockedSitesFragment.this.selectedDeviceID) && selected.mac.equals(BlockedSitesFragment.this.selectedDeviceMAC)) {
                                        return; // Don't do anything for duplicate selection
                                    }
                                    // If there are changes, prompt before leaving
                                    if (BlockedSitesFragment.this.hasChanges) {
                                        new AlertDialog.Builder(BlockedSitesFragment.this.requireContext())
                                                .setMessage(R.string.blocked_sites_leave_warning)
                                                .setPositiveButton(R.string.blocked_sites_leave_warning_positive_button, (dialogInterface, i1) -> {
                                                    BlockedSitesFragment.this.selectedDeviceID = selected.id;
                                                    BlockedSitesFragment.this.selectedDeviceMAC = selected.mac;
                                                    BlockedSitesFragment.this.loadData();
                                                })
                                                .setNegativeButton(R.string.blocked_sites_leave_warning_cancel, (dialogInterface, i12) -> {
                                                    devicesSpinner.setSelection(
                                                            devices.indexOf(StreamSupport.stream(devices)
                                                                    .filter(device -> device.id == selectedDeviceID && device.mac == selectedDeviceMAC)
                                                                    .findFirst()
                                                                    .get())
                                                    );
                                                })
                                                .show();
                                    } else {
                                        BlockedSitesFragment.this.selectedDeviceID = selected.id;
                                        BlockedSitesFragment.this.selectedDeviceMAC = selected.mac;
                                        BlockedSitesFragment.this.loadData();
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {}
                            });
                        } else {
                            this.devicesSpinner.setOnTouchListener((view, motionEvent) -> {
                                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                                    Snackbar.make(this.devicesSpinner, R.string.blocked_sites_get_pro, Snackbar.LENGTH_SHORT)
                                            .setAction(R.string.get_pro_surfer_short, view1 -> {
                                                startActivity(Util.getProIntent(requireContext()));
                                            })
                                            .show();
                                }
                                return true;
                            });
                        }
                    });
                }, this::onFail);
    }

    /**
     * Save the current state.
     */
    @SuppressLint("CheckResult")
    public void save(Runnable onDone) {
        CategoriesRules fromSitesCategories = sitesCategoriesFragment.rules;
        this.rules.categories = fromSitesCategories.categories;
        this.rules.restrictions = fromSitesCategories.restrictions;
        this.loadedLayout.setVisibility(View.GONE);
        this.loadingLayout.setVisibility(View.VISIBLE);
        SafeSurferNetworkApi.getInstance().getSRUseCase().patchCategoriesRules(
                Tools.getInstance().getDeviceAuthToken(),
                this.selectedDeviceID,
                this.selectedDeviceMAC,
                this.rules
        ).subscribe(result -> {
            if (onDone != null) {
                onDone.run();
            }
        });
    }

    /**
     * Should be called when any part of the model changes.
     */
    private void onChange() {
        hasChanges = true;
        if (basicMode) {
            return;
        }
        AnimatorSet set = new AnimatorSet(); //this is your animation set.
        //add as many Value animator to it as you like
        ValueAnimator scaleUp = ValueAnimator.ofFloat(1,(float)1.5);
        scaleUp.setDuration(500);
        scaleUp.setInterpolator(new BounceInterpolator()); //remove this if you prefer default interpolator
        scaleUp.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Float newValue = (Float) valueAnimator.getAnimatedValue();
                saveButton.setScaleY(newValue);
                saveButton.setScaleX(newValue);
            }
        });
        ValueAnimator scaleDown = ValueAnimator.ofFloat((float)1.5,1);
        scaleDown.setDuration(500);
        scaleDown.setInterpolator(new BounceInterpolator());
        scaleDown.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Float newValue = (Float) valueAnimator.getAnimatedValue();
                saveButton.setScaleY(newValue);
                saveButton.setScaleX(newValue);
            }
        });
        set.play(scaleUp);
        set.play(scaleDown).after(scaleUp);
        set.start();
    }

    /**
     * Set whether the support action bar is visible.
     */
    private void setSupportABVisible(boolean visible) {
        if (visible) {
            supportActionBar.show();
        } else {
            supportActionBar.hide();
        }
    }

    /**
     * Fragment state adapter to load the correct fragments into each tab.
     */
    private class BlockedSitesFragmentStateAdapter extends FragmentStateAdapter {

        private final boolean basicMode;

        public BlockedSitesFragmentStateAdapter(Fragment fragment, boolean basicMode) {
            super(fragment);
            this.basicMode = basicMode;
        }

        @NotNull
        @Override
        public Fragment createFragment(int position) {
            switch (position) {
                case 0:
                    sitesCategoriesFragment = new SitesCategoriesFragment(rules, parentToChild, BlockedSitesFragment.this::onChange);
                    return sitesCategoriesFragment;
                case 1:
                    blocklistFragment = new DomainListFragment(
                            rules,
                            BlockedSitesFragment.this::onChange,
                            BlockedSitesFragment.this::setSupportABVisible,
                            true
                    );
                    return blocklistFragment;
                case 2:
                    allowlistFragment = new DomainListFragment(
                            rules,
                            BlockedSitesFragment.this::onChange,
                            BlockedSitesFragment.this::setSupportABVisible,
                            false
                    );
                    return allowlistFragment;
            }
            return null;
        }

        @Override
        public int getItemCount() {
            return basicMode ? 1 : 3;
        }
    }

    /**
     * Fragment to hold the sites/categories tab.
     */
    public static class SitesCategoriesFragment extends Fragment {

        private CategoriesRules rules;
        private final Map<Integer, List<Integer>> parentToChild;
        private SitesCategoriesAdapter sitesCategoriesAdapter;
        private final Runnable onChange;
        private boolean showDescriptions = false;

        public SitesCategoriesFragment(CategoriesRules rules, Map<Integer, List<Integer>> parentToChild, Runnable onChange) {
            this.parentToChild = parentToChild;
            this.rules = rules;
            this.onChange = onChange;
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                                 @Nullable Bundle savedInstanceState) {
            View contentView = inflater.inflate(R.layout.fragment_blocked_sites_sites_categories, container, false);
            RecyclerView recyclerView = contentView.findViewById(R.id.recycler_view);
            sitesCategoriesAdapter = new SitesCategoriesAdapter();
            recyclerView.setAdapter(sitesCategoriesAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            return contentView;
        }

        /**
         * Set whether to show category descriptions.
         */
        private void setShowDescriptions(boolean show) {
            this.showDescriptions = show;
            this.sitesCategoriesAdapter.notifyDataSetChanged();
        }

        private class SitesCategoriesAdapter extends RecyclerView.Adapter<SitesCategoriesFragment.ViewHolder> {

            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_danger_toggle_item, parent, false);
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
                holder.position = position;
                holder.checkbox.setOnCheckedChangeListener(null);
                holder.cardView.setOnClickListener(null);
                holder.leftSpace.setVisibility(View.GONE);
                holder.bottomText.setVisibility(View.VISIBLE);
                if (position < rules.categories.length) {
                    // Is a category
                    holder.textView.setText(rules.categories[position].displayName);
                    if (showDescriptions) {
                        holder.bottomText.setText(rules.categories[position].description);
                    } else {
                        holder.bottomText.setVisibility(View.GONE);
                    }
                    holder.checkbox.setVisibility(View.VISIBLE);
                    holder.checkbox.setChecked(rules.categories[position].action == 0);
                    holder.checkbox.setClickable(!rules.categories[position].frozen);
                    holder.cardView.setClickable(!rules.categories[position].frozen);
                    if (rules.categories[position].frozen) {
                        holder.lockImg.setVisibility(View.VISIBLE);
                    } else {
                        holder.lockImg.setVisibility(View.GONE);
                    }
                    // When the whole card is clicked, toggle the switch and optionally children
                    holder.cardView.setOnClickListener(view -> {
                        if (rules.categories[position].frozen) {
                            return;
                        }
                        onChange.run();
                        boolean newChecked = !holder.checkbox.isChecked();
                        rules.categories[position].action = newChecked ? 0 : 1;
                        holder.checkbox.setChecked(newChecked);
                        if (parentToChild.containsKey(rules.categories[position].id)) {
                            // Set all the children too
                            for (Integer i : parentToChild.get(rules.categories[position].id)) {
                                rules.categories[i].action = newChecked ? 0 : 1;
                            }
                            notifyDataSetChanged();
                        }
                    });
                    // When the switch is clicked, update state
                    holder.checkbox.setOnCheckedChangeListener((compoundButton, b) -> {
                        if (rules.categories[position].frozen) {
                            return;
                        }
                        onChange.run();
                        rules.categories[position].action = compoundButton.isChecked() ? 0 : 1;
                        if (parentToChild.containsKey(rules.categories[position].id)) {
                            // Set all the children too
                            for (Integer i : parentToChild.get(rules.categories[position].id)) {
                                rules.categories[i].action = compoundButton.isChecked() ? 0 : 1;
                            }
                            notifyDataSetChanged();
                        }
                    });
                    if (rules.categories[position].parent > 0) {
                        holder.leftSpace.setVisibility(View.VISIBLE);
                    }
                } else {
                    // Is a restriction
                    int restrictionIdx = position - rules.categories.length;
                    Restriction restriction = rules.restrictions[restrictionIdx];
                    holder.textView.setText(restriction.displayName);
                    holder.bottomText.setText(restriction.actions[restriction.action]);
                    holder.checkbox.setVisibility(View.GONE);
                    holder.cardView.setOnClickListener(view -> {
                        new AlertDialog.Builder(requireContext())
                                .setTitle(restriction.displayName)
                                .setSingleChoiceItems(restriction.actions, restriction.action, (dialogInterface, i) -> {
                                    onChange.run();
                                    rules.restrictions[restrictionIdx].action = i;
                                    holder.bottomText.setText(restriction.actions[restriction.action]);
                                    dialogInterface.dismiss();
                                })
                                .show();
                    });
                }
            }

            @Override
            public int getItemCount() {
                return rules.categories.length + rules.restrictions.length;
            }
        }

        private static class ViewHolder extends RecyclerView.ViewHolder {

            public final MaterialCardView cardView;
            public final TextView textView;
            public final SwitchMaterial checkbox;
            public final ImageView lockImg;
            public final TextView bottomText;
            public final Space leftSpace;
            public int position;
            public Restriction restriction;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                checkbox = itemView.findViewById(R.id.switch_material);
                cardView = itemView.findViewById(R.id.root);
                lockImg = itemView.findViewById(R.id.lock);
                textView = itemView.findViewById(R.id.name);
                bottomText = itemView.findViewById(R.id.bottom_text);
                leftSpace = itemView.findViewById(R.id.left_space);
            }
        }
    }

    /**
     * Fragment to hold the blocklist tab.
     */
    public static class DomainListFragment extends Fragment {

        private CategoriesRules rules;
        private MaterialButton addButton;
        private RecyclerView recyclerView;
        private DomainListAdapter domainlistAdapter;
        private Runnable onChange;
        private Consumer<Boolean> setSupportABVisible;
        private Set<Integer> selectedIndices = new HashSet<>();
        private ActionMode actionMode;
        private boolean isBlacklist;
        // Action mode for toolbar if selecting items
        private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Inflate a menu resource providing context menu items
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.context_menu_delete, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false; // Return false if nothing is done
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (item.getItemId() == R.id.action_delete) {
                    new AlertDialog.Builder(requireContext())
                            .setMessage(R.string.delete_blocklist_confirm)
                            .setNegativeButton(R.string.cancel, null)
                            .setPositiveButton(R.string.delete, (dialogInterface, j) -> {
                                List<String> newList = new ArrayList<>();
                                if (isBlacklist) {
                                    for (int i = 0; i < rules.blacklist.length; i++) {
                                        if (selectedIndices.contains(i)) {
                                            continue;
                                        }
                                        newList.add(rules.blacklist[i]);
                                    }
                                    rules.blacklist = newList.toArray(new String[]{});
                                } else {
                                    for (int i = 0; i < rules.whitelist.length; i++) {
                                        if (selectedIndices.contains(i)) {
                                            continue;
                                        }
                                        newList.add(rules.whitelist[i]);
                                    }
                                    rules.whitelist = newList.toArray(new String[]{});
                                }
                                onChange.run();
                                domainlistAdapter.notifyDataSetChanged();
                                stopSelection();
                            })
                            .show();
                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                selectedIndices.clear();
                domainlistAdapter.notifyDataSetChanged();
                setSupportABVisible.accept(true);
            }
        };

        public DomainListFragment(CategoriesRules rules, Runnable onChange, Consumer<Boolean> setSupportABVisible, boolean isBlacklist) {
            super();
            this.rules = rules;
            this.onChange = onChange;
            this.setSupportABVisible = setSupportABVisible;
            this.isBlacklist = isBlacklist;
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                                 @Nullable Bundle savedInstanceState) {
            View contentView = inflater.inflate(R.layout.fragment_blocked_sites_blocklist, container, false);
            addButton = contentView.findViewById(R.id.add_button);
            recyclerView = contentView.findViewById(R.id.recycler_view);
            domainlistAdapter = new DomainListAdapter();
            recyclerView.setAdapter(domainlistAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            return contentView;
        }

        private void showInput(String initialValue, boolean isAdd, int position) {
            // Get the view to show
            View view = LayoutInflater.from(requireContext()).inflate(R.layout.view_domain_dialog, (ViewGroup) getView(), false);
            final EditText editText = view.findViewById(R.id.domain_input);
            editText.setText(initialValue);
            final TextView errorText = view.findViewById(R.id.error_text);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (!DOMAIN_NAME_ONLY.matcher(charSequence).find()) {
                        // Invalid domain name
                        errorText.setText(R.string.domain_dialog_error);
                        errorText.setVisibility(View.VISIBLE);
                        return;
                    }
                    // Valid domain name, but check not one of our domains
                    String str = charSequence.toString();
                    if (isBlacklist && (str.endsWith("safesurfer.co.nz") || str.endsWith("safesurfer.io"))) {
                        errorText.setText(R.string.domain_dialog_ss_error);
                        errorText.setVisibility(View.VISIBLE);
                        return;
                    }
                    errorText.setVisibility(View.GONE);
                }

                @Override
                public void afterTextChanged(Editable editable) {}
            });
            // Run when user presses add via keyboard or button.
            Consumer<DialogInterface> onDone = dialog -> {
                if (errorText.getVisibility() != View.VISIBLE) {
                    // Is valid
                    if (isAdd) {
                        if (isBlacklist) {
                            List<String> newBlacklist = new ArrayList<>(Arrays.asList(rules.blacklist));
                            newBlacklist.add(0, editText.getText().toString());
                            rules.blacklist = newBlacklist.toArray(new String[]{});
                        } else {
                            List<String> newWhitelist = new ArrayList<>(Arrays.asList(rules.whitelist));
                            newWhitelist.add(0, editText.getText().toString());
                            rules.whitelist = newWhitelist.toArray(new String[]{});
                        }
                    } else {
                        if (isBlacklist) {
                            rules.blacklist[position] = editText.getText().toString();
                        } else {
                            rules.whitelist[position] = editText.getText().toString();
                        }
                    }
                    domainlistAdapter.notifyDataSetChanged();
                    onChange.run();
                    dialog.dismiss();
                }
            };
            AlertDialog dialog = new AlertDialog.Builder(requireContext())
                    .setView(view)
                    .setNegativeButton(R.string.cancel, null)
                    .setPositiveButton(isAdd ? R.string.add : R.string.update, null)
                    .show();
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view12 -> {
                onDone.accept(dialog);
            });
            editText.setOnKeyListener((view1, keyCode, keyEvent) -> {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    onDone.accept(dialog);
                    return true;
                }
                return false;
            });
        }

        /**
         * Deselect all items and hide the action mode bar.
         */
        private void stopSelection() {
            selectedIndices.clear();
            if (actionMode != null) {
                actionMode.finish();
            }
            setSupportABVisible.accept(true);
        }

        /**
         * Show the context menu to delete items.
         */
        private void showContextMenu() {
            actionMode = getActivity().startActionMode(mActionModeCallback);
            setSupportABVisible.accept(false);
        }

        private class DomainListAdapter extends RecyclerView.Adapter<DomainListFragment.ViewHolder> {

            private static final int STATIC_CARD = 0;
            private static final int DYNAMIC_CARD = 1;

            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_checkable_text_list_item, parent, false);
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
                if (getItemViewType(position) == STATIC_CARD) {
                    holder.materialButton.setVisibility(View.VISIBLE);
                    holder.cardView.setVisibility(View.GONE);
                    holder.materialButton.setOnClickListener(view -> {
                        showInput("", true, 0);
                    });
                } else {
                    holder.textView.setText(isBlacklist ? rules.blacklist[position-1] : rules.whitelist[position-1]);
                    holder.cardView.setOnClickListener(view -> {
                        if (selectedIndices.size() > 0) {
                            // Select/unselect
                            if (selectedIndices.contains(position-1)) {
                                selectedIndices.remove(position-1);
                                holder.cardView.setChecked(false);
                                if (selectedIndices.size() == 0) {
                                    stopSelection();
                                }
                            } else {
                                selectedIndices.add(position-1);
                                holder.cardView.setChecked(true);
                            }
                        } else {
                            showInput(isBlacklist ? rules.blacklist[position-1] : rules.whitelist[position-1], false, position-1);
                        }
                    });
                    holder.cardView.setChecked(selectedIndices.contains(position-1));
                    holder.cardView.setOnLongClickListener(view -> {
                        holder.cardView.setChecked(true);
                        selectedIndices.add(position-1);
                        showContextMenu();
                        return true;
                    });
                }
            }

            @Override
            public int getItemCount() {
                return (isBlacklist ? rules.blacklist.length : rules.whitelist.length) + 1; // Static card at beginning
            }

            @Override
            public int getItemViewType(int position) {
                if(position == 0) {
                    return STATIC_CARD;
                } else {
                    return DYNAMIC_CARD;
                }
            }
        }

        private static class ViewHolder extends RecyclerView.ViewHolder {

            private final TextView textView;
            private final MaterialButton materialButton;
            private final MaterialCardView cardView;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                textView = itemView.findViewById(R.id.list_text);
                materialButton = itemView.findViewById(R.id.add_button);
                cardView = itemView.findViewById(R.id.root);
            }
        }
    }
}
