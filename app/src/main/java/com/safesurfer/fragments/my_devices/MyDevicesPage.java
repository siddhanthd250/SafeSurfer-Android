package com.safesurfer.fragments.my_devices;

import com.safesurfer.R;
import com.safesurfer.fragments.PWAPage;

public class MyDevicesPage implements PWAPage {
    @Override
    public String getBaseURL() {
        return "https://my.safesurfer.io/devices";
    }

    @Override
    public String getURLPattern() {
        return "^https://my.safesurfer.io/devices.*";
    }

    @Override
    public String getTitle() {
        return "My Devices";
    }

    @Override
    public int getNavDrawerID() {
        return R.id.menu_devices;
    }

    @Override
    public int getMenu() {
        return -1;
    }
}
